<!DOCTYPE html>
<!--
Application Name: CloudOnex Business Suite
Version: 1.0.2
Website: https://www.cloudonex.com/
License: You must have a valid license purchased only from cloudonex.com in order to legally use this software.
-->
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title></title>

    <link rel="apple-touch-icon" sizes="180x180" href="{$app_url}apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{$app_url}favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{$app_url}favicon-16x16.png">

    <link href="{$theme}default/css/app.css?v={$file_build}" rel="stylesheet">

    <link href="{$theme}default/css/{$config['nstyle']}.css" rel="stylesheet">

    {foreach $plugin_ui_header_client as $plugin_ui_header_add}
        {$plugin_ui_header_add}
    {/foreach}

    {if $config['rtl'] eq '1'}
        <link href="{$_theme}/css/bootstrap-rtl.min.css" rel="stylesheet">
        <link href="{$_theme}/css/style-rtl.min.css" rel="stylesheet">
    {/if}

    {if isset($xheader)}
        {$xheader}
    {/if}

    {block name=style}{/block}

    {$config['header_scripts']}
    <style type="text/css">
        body {

            background-color: #e9ebee;
            overflow-x: visible;
        }
        .paper {
            margin: auto;
            width: 1000px;
            /*border: 2px solid #DDD;*/
            background-color: #FFF;
            position: relative;

        }

        .fancybox-slide--iframe .fancybox-content {
            width  : 600px;
            max-width  : 80%;
            max-height : 80%;
            margin: 0;
        }

        .panel {

            /*box-shadow: none;*/

            -webkit-box-shadow: 0 10px 40px 0 rgba(18,106,211,.07), 0 2px 9px 0 rgba(18,106,211,.06);
            box-shadow: 0 10px 40px 0 rgba(18,106,211,.07), 0 2px 9px 0 rgba(18,106,211,.06);

        }

        .panel-body {
            padding: 25px;
        }

        {if isset($payment_gateways_by_processor['stripe'])}

        .StripeElement {
            background-color: white;
            height: 40px;
            padding: 10px 12px;
            border-radius: 4px;
            border: 1px solid transparent;
            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }

        {/if}
        /* Extra CSS for Print Button*/
        .button {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            overflow: hidden;
            margin-top: 20px;
            padding: 12px 12px;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-transition: all 60ms ease-in-out;
            transition: all 60ms ease-in-out;
            text-align: center;
            white-space: nowrap;
            text-decoration: none !important;

            color: #fff;
            border: 0 none;
            border-radius: 4px;
            font-size: 14px;
            font-weight: 500;
            line-height: 1.3;
            -webkit-appearance: none;
            -moz-appearance: none;

            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-flex: 0;
            -webkit-flex: 0 0 160px;
            -ms-flex: 0 0 160px;
            flex: 0 0 160px;
        }
        .button:hover {
            -webkit-transition: all 60ms ease;
            transition: all 60ms ease;
            opacity: .85;
        }
        .button:active {
            -webkit-transition: all 60ms ease;
            transition: all 60ms ease;
            opacity: .75;
        }
        .button:focus {
            outline: 1px dotted #959595;
            outline-offset: -4px;
        }

        .button.-regular {
            color: #202129;
            background-color: #edeeee;
        }
        .button.-regular:hover {
            color: #202129;
            background-color: #e1e2e2;
            opacity: 1;
        }
        .button.-regular:active {
            background-color: #d5d6d6;
            opacity: 1;
        }

        .button.-dark {
            color: #FFFFFF;
            background: #333030;
        }
        .button.-dark:focus {
            outline: 1px dotted white;
            outline-offset: -4px;
        }

        @media print
        {
            .no-print, .no-print *
            {
                display: none !important;
            }
        }


    </style>

    {if isset($payment_gateways_by_processor['stripe'])}
        <script src="https://js.stripe.com/v3/"></script>
    {/if}

</head>
<body class="fixed-nav">

<div class="paper">
    <div class="row">
        <div class="col-lg-12"  id="application_ajaxrender">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="invoice">
                        <header class="clearfix">
                            <div class="row">
                                <div class="col-sm-6 mt-md">
                                    <b class="mt-none mb-sm text-dark text-bold">INVOICE  # {$order_id}</b><br>
                                    <b class="mt-none mb-sm text-dark text-bold">ORDER  # {$order_number}</b><br>
                                    <b class="mt-none mb-sm text-dark text-bold">DATE {date( $config['df'], strtotime($date))}</b>
                                </div>
                                <div class="col-sm-6 text-right">

                                    <div class="ib">
                                        <img src="{$app_url}storage/system/{$config['logo_default']}" alt="Logo" style="margin-bottom: 15px;">
                                    </div>

                                    <address class="ib mr-xlg">
                                        <strong>{$config['CompanyName']}</strong>
                                        <br>
                                        {$config['caddress']}
                                    </address>

                                </div>
                            </div> <hr  noshade color="red"  align="center" />
                        </header>

                        <div class="bill-info" style="margin-top: 50px; margin-bottom: 100px;">
                            <div class="row">
                                <div class="col-md-6" style="width: 50%;float: left" >
                                    <div class="bill-to" style="float: left;">
                                        <p class="h5 mb-xs text-dark text-semibold"><strong>Invoiced To</strong></p>
                                        <address>
                                            {if isset($shipping_address['company']) }
                                                <strong>Company :</strong> {$shipping_address['company']}
                                                <br>
                                            {/if}

                                            <strong>ATTN :</strong>
                                            {if isset($shipping_address['first_name']) }
                                                {$shipping_address['first_name']}
                                            {/if}
                                            {if isset($shipping_address['last_name']) }
                                                {$shipping_address['last_name']}
                                            {/if}
                                            <br>
                                            {if isset($shipping_address['address1']) }
                                                {$shipping_address['address1']}
                                                <br>
                                            {/if}
                                            {if isset($shipping_address['province']) }
                                                {$shipping_address['province']}
                                            {/if}
                                            {if isset($shipping_address['city']) }
                                                {$shipping_address['city']}
                                            {/if}
                                            {if isset($shipping_address['zip']) }
                                                {$shipping_address['zip']}

                                            {/if}
                                            <br>
                                            {if isset($shipping_address['country']) }
                                                {$shipping_address['country']}
                                                <br>
                                            {/if}
                                            {if isset($shipping_address['phone']) }
                                                <strong>{$_L['Phone']}:</strong> {$shipping_address['phone']}
                                                <br>
                                            {/if}
                                            {if isset($customer['email'])  }
                                                <strong>Email :</strong> {$customer['email']}
                                            {else}
                                                {if isset($shipping_address['email']) }
                                                    <strong>{$_L['Email']}:</strong>  {$shipping_address['email']}
                                                {/if}
                                            {/if}

                                        </address>
                                    </div>
                                </div>
                                <div class="col-md-6" style="width: 50%;float: right">
                                    <div class="bill-to" style="float: right">
                                        <p class="h5 mb-xs text-dark text-semibold"><strong>Billing Address:</strong></p>
                                        <address>
                                            {if isset($address_billing['company']) }
                                                <strong>Company :</strong> {$address_billing['company']}
                                            {/if}
                                            {if isset($address_billing['first_name']) }
                                                {$address_billing['first_name']}
                                            {/if}
                                            {if isset($address_billing['last_name']) }
                                                {$address_billing['last_name']}
                                            {/if}
                                            <br>
                                            {if isset($address_billing['address1']) }
                                                <strong>Address :</strong> {$address_billing['address1']}
                                                <br>
                                            {/if}
                                            {if isset($address_billing['province']) }
                                                <strong>Province :</strong> {$address_billing['province']}
                                            {/if}
                                            {if isset($address_billing['city']) }
                                               {$address_billing['city']}
                                            {/if}
                                            {if isset($address_billing['zip']) }
                                                {$address_billing['zip']}
                                                <br>
                                            {/if}

                                            {if isset($address_billing['country']) }
                                                <strong>Country :</strong> {$address_billing['country']}
                                                <br>
                                            {/if}
                                            {if isset($address_billing['phone']) }
                                                <strong>{$_L['Phone']}:</strong> {$address_billing['phone']}
                                                <br>
                                            {/if}

                                        </address>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <form method="post" id="post_warehouse">
                            <div class="table-responsive">
                                <table class="table invoice-items" >
                                    <thead>
                                    <tr class="h5 text-dark">
                                        <th id="cell-id">#</th>
                                        <th id="cell-item">SELLER SKU</th>
                                        <th id="cell-item">NAME </th>
                                        <th id="cell-qty">QTY</th>
                                        <th id="cell-qty">UNIT PRICE</th>
                                        <th id="cell-qty">TOTAL PRICE</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    {foreach $items as $item}
                                        <tr>
                                            {if isset($item['order_item_id']) }
                                                <th id="cell-id">{$item['order_item_id']}</th>
                                            {else}
                                                <th id="cell-id"></th>
                                            {/if}
                                            {if isset($item['sku']) }
                                                <th id="cell-id">{$item['sku']}</th>
                                            {else}
                                                <th id="cell-id"></th>
                                            {/if}
                                            {if isset($item['name']) }
                                                <th id="cell-id">{$item['name']}</th>
                                            {else}
                                                <th id="cell-id"></th>
                                            {/if}
                                            {if isset($item['quantity']) }
                                                <th id="cell-id">{$item['quantity']}</th>
                                            {else}
                                                <th id="cell-id">1</th>
                                            {/if}
                                            {if isset($item['item_price']) }
                                                <th id="cell-id">{$item['item_price']}</th>
                                            {else}
                                                <th id="cell-id"></th>
                                            {/if}
                                            {if isset($item['item_price']) }
                                                <th id="cell-id">{$item['item_price']} {$currency}</th>
                                            {else}
                                                <th id="cell-id"></th>
                                            {/if}
                                        </tr>
                                    {/foreach}

                                    </tbody>
                                </table>
                            </div>
                        </form>
                        <div class="invoice-summary">
                            <div class="row" >
                                <div class="col-sm-4 col-sm-offset-8" style="margin-left: 66.66666667%;">
                                    <table class="table h5 text-dark">
                                        <tbody>
                                        <tr class="b-top-none">
                                            <td>{$_L['Subtotal']}</td>
                                            <td style="text-align: right;" class="text-left amount">{$subtotal_price} {$currency}</td>
                                        </tr>
                                            <tr>
                                                <td>Shipping fee:</td>
                                                <td style="text-align: right;" class="text-left amount">{$shipping_fee}</td>
                                            </tr>
                                            <tr>
                                                <td>{$type} Discount Total:</td>
                                                <td style="text-align: right;" class="text-left amount">{$total_discounts}</td>
                                            </tr>
                                            <tr>
                                                <td>Seller Discount Total:</td>
                                                <td style="text-align: right;" class="text-left amount">{$total_discounts}</td>
                                            </tr>
                                            {if {$total_tax}!= 0 }
                                                <tr>
                                                    <td>{$_L['TAX']}</td>
                                                    <td style="text-align: right;" class="text-left amount">{$total_tax}</td>
                                                </tr>
                                            {/if}
                                        <tr>
                                            <td>{$_L['Grand Total']}</td>
                                            <td style="text-align: right;" class="text-left amount">{$total} {$currency}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <button class="button -dark center no-print" onclick="window.print();">Click Here to Print</button>
</div>
</body>

</html>