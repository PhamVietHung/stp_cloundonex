{extends file="$layouts_admin"}

{block name="content"}
    <div class="row">
        <div class="col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Add New </h5>

                </div>
                <div class="ibox-content">

                    <form role="form" name="marketplace-add" method="post" action="{$_url}marketplace/add-post">

                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" name="title">
                        </div>

                        <div class="form-group">
                            <label for="marketplace">Marketplace</label>
                            <select id="marketplace" name="options" class="form-control">
                                <option value="Shopify" selected="selected">
                                    Shopify
                                </option>
                                <option value="Lazada">
                                    Lazada
                                </option>
                            </select>
                        </div>
                        <div class="form-group Lazada">
                            <label for="country">Country</label>
                            <select id="country" name="country" class="form-control">
                                <option value="sg">
                                    Singapore
                                </option>
                                <option value="my">
                                    Malaysia
                                </option>
                                <option value="id">
                                    Indonesia
                                </option>
                                <option value="th">
                                    Thailand
                                </option>
                                <option value="ph">
                                    Philippines
                                </option>
                                <option value="vn">
                                    Vietnam
                                </option>
                            </select>
                        </div>
                        <div class="form-group url">
                            <label for="url">Url</label>
                            <input type="text" class="form-control" id="url" name="url">
                        </div>

                        <div class="form-group">
                            <label for="api_key">Api Key</label>
                            <input type="text" class="form-control" id="api_key" name="api_key">
                        </div>

                        <div class="form-group">
                            <label for="api_secret">Api Secret</label>
                            <input type="text" class="form-control" id="api_secret" name="api_secret">
                        </div>

                        <div class="form-group date-picker">
                            <label for="orders_created_after">Orders Created After</label>
                            <input type="text" class="form-control" id="created_after" name="orders_created_after" datepicker
                                   data-date-format="yyyy-mm-dd" data-auto-close="true" value="{$idate}">
                        </div>

                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> {$_L['Submit']}
                        </button>
                    </form>

                </div>
            </div>


        </div>


    </div>
{/block}
{block name="script"}
    <script>
        $(document).ready(function () {
            var formCountry = $('.Lazada'),
                labelUrl = $('.url label'),
                datePicker = $('.date-picker');

            formCountry.hide();
            datePicker.hide();

            $('#marketplace').change(function () {
                var options = $("#marketplace option:selected").val();
                formCountry.hide();
                if (options) {
                    if (options == 'Lazada') {
                        datePicker.show();
                        labelUrl.text('Callback URL');
                    } else {
                        datePicker.hide();
                        labelUrl.text('URL');
                    }
                }

                $('.' + $(this).val()).show();
            });
        });
    </script>
{/block}
