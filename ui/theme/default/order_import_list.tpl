{extends file="$layouts_admin"}

{block name="content"}
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="{$_url}order-import/import_csv/" class="md-btn md-btn-primary">
                        <i class="fa fa-plus"></i> Import
                    </a>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="fa fa-search"></span>
                                    </div>
                                    <input type="text" name="name" id="foo_filter"
                                           class="form-control" placeholder="{$_L['Search']}..."/>
                                </div>
                            </div>
                        </div>
                    </form>
                    <table class="table table-bordered table-hover sys_table footable"
                           data-filter="#foo_filter" data-page-size="10">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Date Created</th>
                            <th>Financial Status</th>
                            <th>Fulfillment Status</th>
                            <th>Total</th>
                            <th>Notes</th>
                            <th class="text-right" data-sort-ignore="true">{$_L['Manage']}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach $orders as $o}
                            <tr>
                                <td>{$o['id']}</td>
                                <td>{$o['name']}</td>
                                <td>{$o['email']}</td>
                                <td>
                                    {date( $config['df'], strtotime({$o['created_at']}))}
                                </td>
                                <td>{$o['financial_status']}</td>
                                <td>{$o['fulfillment_status']}</td>
                                <td>{$o['total']}</td>
                                <td>{$o['notes']}</td>
                                <td class="text-right">
                                    <a title="delete" href="#"
                                       class="btn btn-danger btn-xs cdelete"
                                       id="{$o['id']}">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="9">
                                <ul class="pagination">
                                </ul>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
{/block}
