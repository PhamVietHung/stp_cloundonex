{extends file="$layouts_admin"}

{block name="content"}
    <div class="alert alert-danger" id="emsg">
        <span id="emsgbody"></span>
    </div>
    <div class="ibox-title">
        <h5>
            Edit Manufacturer
        </h5>
        <div class="ibox-tools">
            <a href="{$_url}ps/p-list/bundled" class="btn btn-primary btn-xs">List Product</a>
        </div>
    </div>
    <input id="is_bundle" type="hidden" value="{$is_bundle}" name="is_bundle">
    <form class="form-horizontal" id="rform">
        <input type="hidden" value="{$id_manufacturer_product}" name="id_manufacturer_product">
        <input id="currency" type="hidden" value="{$currency}" name="currency">
        <div class="table-responsive m-t">

            <table class="table manufacturer-table" id="manufacturer_items">
                <thead>
                <tr>
                    <th width="70%">{$_L['Name']}</th>
                    <th width="30%">{$_L['Cost Price']}</th>
                </tr>
                </thead>
                <tbody>

                {foreach $items as $item}
                    <tr>
                        <input type="hidden" value="{$item['id_manufacturer']}" name="id_manufacturer[]">
                        <input type="hidden" value="{$item['name']}" name="item_name_manufacturer[]">
                        <td class="item_name">{$item['name']}</td>
                        <td>
                            <input type="text" class="form-control amount" name="cost_price_manufacturer[]" data-a-sign="{$config['currency_code']} "  data-a-dec="{$config['dec_point']}" data-a-sep="{$config['thousands_sep']}" value="{{$item['cost_price']}}">
                        </td>
                    </tr>
                {/foreach}

                </tbody>
            </table>

        </div>

        <button type="button" class="btn btn-primary" id="item-add"><i
                    class="fa fa-search"></i> Add Manufacturer</button>
        <button style="display: none" type="button" class="btn btn-danger" id="item-remove"><i
                    class="fa fa-minus-circle"></i> {$_L['Delete']}</button>
        <button style="float: right;padding: 8px 31px;margin-right: 8px;" class="btn btn-sm btn-primary" type="submit" id="submit">{$_L['Save']}</button>
    </form>
{/block}
