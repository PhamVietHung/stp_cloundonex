{extends file="$layouts_admin"}

{block name="content"}
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="{$_url}customers/list/">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="fa fa-search"></span>
                                    </div>
                                    <input type="text" name="name" id="foo_filter"
                                           class="form-control" placeholder="{$_L['Search']}..."/>
                                </div>
                            </div>
                        </div>
                    </form>
                    <table class="table table-bordered table-hover sys_table footable"
                           data-filter="#foo_filter" data-page-size="10">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Date Created</th>
                            <th>Marketplace</th>
                            <th>Country</th>
                            <th class="text-right" data-sort-ignore="true">{$_L['Manage']}</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach $mar as $m}
                            <tr>
                                <td>{$m['id']}</td>
                                <td>{$m['title']}</td>
                                <td>
                                    {date( $config['df'], strtotime({$m['date_added']}))}
                                </td>
                                <td>
                                    {if $m['options'] eq 'Shopify'}
                                        <span class="label label-success">{ib_lan_get_line($m['options'])}</span>
                                    {else}
                                        <span class="label label-danger">{ib_lan_get_line($m['options'])}</span>
                                    {/if}
                                </td>
                                <td>
                                    <span class="label label-primary">{ib_lan_get_line($m['country'])}</span>
                                </td>
                                <td class="text-right">
                                    {if $m['options'] eq 'Lazada'}
                                        <a title="get token"
                                           href="{$_url}marketplace/get-token/{$m['id']}/{$type}"
                                           class="btn btn-primary btn-xs">
                                            <i class="fa fa-save"></i>
                                        </a>
                                    {/if}
                                    <a title="sync"
                                       href="{$_url}marketplace/sync/{$m['id']}/{$m['options']}/{$type}"
                                       class="btn btn-primary btn-xs">
                                        <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="6">
                                <ul class="pagination">
                                </ul>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
{/block}
