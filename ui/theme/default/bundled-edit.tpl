{extends file="$layouts_admin"}

{block name="content"}
    <div class="alert alert-danger" id="emsg">
        <span id="emsgbody"></span>
    </div>
    <div class="ibox-title">
        <h5>
            Edit Bundle Product
        </h5>
        <div class="ibox-tools">
            <a href="{$_url}ps/p-list/bundled" class="btn btn-primary btn-xs">List Bundle</a>
        </div>
    </div>
    <form class="form-horizontal" id="rform">
        <input type="hidden" value="{$id_bundled_product}" name="id_bundled_product">
        <div class="table-responsive m-t">

            <table class="table bundled-table" id="bundled_items">
                <thead>
                <tr>
                    <th width="10%">{$_L['Item Code']}</th>
                    <th width="50%">{$_L['Item Name']}</th>
                    <th width="10%">{$_L['Qty']}</th>
                    <th width="10%">{$_L['Price']}</th>
                    <th width="10%">{$_L['Total']}</th>
                    <th width="10%">Tax</th>

                </tr>
                </thead>
                <tbody>

                {foreach $items as $item}
                    <tr>
                        <input type="hidden" value="{$item['item_code']}" name="item_code[]">
                        <td>{$item['item_code']}</td>
                        <td>
                            <textarea class="form-control item_name" name="desc[]" rows="3">{$item['description']}</textarea>
                        </td>
                        <td>
                            <input type="number" data-item-code-qty="{$item['item_code']}" min="0" max="99" class="form-control qty" value="{if ($config['dec_point']) eq ','}{$item['qty']|replace:'.':','}{else}{$item['qty']}{/if}" name="qty[]">
                        </td>
                        <td>
                            <input type="text" data-item-code-price="{$item['item_code']}" data-item-price="{$item['price']}" class="form-control item_price" name="amount[]" value="{if ($config['dec_point']) eq ','}{$item['amount']|replace:'.':','}{else}{$item['price']}{/if}">
                        </td>
                        <td class="ltotal">
                            <input type="text" data-item-code-total="{$item['item_code']}" class="form-control lvtotal" readonly="" value="{if ($config['dec_point']) eq ','}{{$item['total']}|replace:'.':','}{else}{{$item['total']}}{/if}" name="total[]">
                        </td>
                        <td>
                            <select class="form-control taxed" name="taxed[]">
                                <option value="Yes" {if $item['taxable'] eq '1'}selected=""{/if}>Yes</option>
                                <option value="No" {if $item['taxable'] eq '0'}selected=""{/if}>No</option>
                            </select>
                        </td>
                    </tr>
                {/foreach}

                </tbody>
            </table>

        </div>

        <button type="button" class="btn btn-primary" id="item-add"><i
                    class="fa fa-search"></i> Add Products/Services</button>
        <button style="display: none" type="button" class="btn btn-danger" id="item-remove"><i
                    class="fa fa-minus-circle"></i> {$_L['Delete']}</button>
        <button style="float: right;padding: 8px 31px;margin-right: 8px;" class="btn btn-sm btn-primary" type="submit" id="submit">{$_L['Save']}</button>
    </form>

{/block}
