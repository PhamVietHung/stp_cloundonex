{extends file="$layouts_admin"}

{block name="content"}
    <div class="row">


        <div class="col-md-12">

            <div class="panel panel-default">

                <div class="panel-body">

                    <form class="form-horizontal" method="post" action="{$_url}customers/list/">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="fa fa-search"></span>
                                    </div>
                                    <input type="text" name="name" id="foo_filter" class="form-control"
                                           placeholder="{$_L['Search']}..."/>

                                </div>
                            </div>

                        </div>
                    </form>
                    <div id="application_ajaxrender">
                        <table class="table table-bordered table-hover sys_table footable" data-filter="#foo_filter"
                               data-page-size="25">
                            <thead>
                            <tr>
                                <th>Order Date</th>
                                <th>Order Number</th>
                                <th>Marketplace</th>
                                <th>Status</th>
                                <th>Fulfillment Status</th>
                                <th class="text-right" data-sort-ignore="true">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach $orders as $order}
                                <tr>
                                    <td>
                                        {date( $config['df'], strtotime({$order['created_at']}))}
                                    </td>
                                    <td>{$order['order_number']}</td>
                                    <td>
                                        {if $order['marketplace'] eq 'Shopify'}
                                            <span class="label label-success">{ib_lan_get_line($order['marketplace'])}</span>
                                        {else}
                                            <span class="label label-danger">{ib_lan_get_line($order['marketplace'])}</span>
                                        {/if}
                                    </td>
                                    <td>
                                        {if $order['marketplace'] eq 'Shopify'}
                                            {$order['financial_status']}
                                        {else}
                                            {$order['statuses']}
                                        {/if}
                                    </td>
                                    <td>{$order['fulfillment_status']}</td>
                                    <td class="text-right">
                                        <a title="invoice"
                                           href="{$_url}marketplace/view-invoice/{$order['id']}/{$order['marketplace']}/{$status}"
                                           class="btn btn-primary btn-xs"><i class="fa fa-cubes" aria-hidden="true"></i>
                                        </a>
                                        <a title="waybill" href="#"
                                           class="btn md-btn-warning btn-xs popup-create-waybill"
                                           data-marketplace="{$order['marketplace']}"
                                           data-setting-id="{$order['marketplace_settings_id']}"
                                           data-order-id="{$order['id']}"><i class="fa fa-history"
                                                                             aria-hidden="true"></i> </a>
                                        {if $list_type == 'Shopify'}
                                            <a title="fulfilment" href="#"
                                               class="btn btn-success btn-xs view-fulfilment"
                                               data-marketplace="{$order['marketplace']}"
                                               data-setting-id="{$order['marketplace_settings_id']}"
                                               data-order-id="{$order['id']}"><i class="fa fa-check"
                                                                                 aria-hidden="true"></i> </a>
                                        {/if}
                                        {if $list_type == 'Lazada'}
                                            <a title="print-waybill"
                                               href="{$_url}marketplace/print-waybill/{$order['id']}/{$order['marketplace_settings_id']}"
                                               target="_blank" class="btn md-btn-warning btn-xs print-waybill"
                                               data-marketplace="{$order['marketplace']}"
                                               data-setting-id="{$order['marketplace_settings_id']}"
                                               data-order-id="{$order['id']}"><i class="fa fa-cart-arrow-down"
                                                                                 aria-hidden="true"></i> </a>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}

                            </tbody>

                            <tfoot>
                            <tr>
                                <td colspan="7">
                                    <ul class="pagination">
                                    </ul>
                                </td>
                            </tr>
                            </tfoot>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}
{block name="script"}
    <script type="text/javascript" src="{$app_url}ui/lib/sms/sms_counter.js"></script>
    <script>

        $(document).ready(function () {

            var $modal = $('#ajax-modal');

            var sysrender = $('#application_ajaxrender');

            sysrender.on('click', '.popup-create-waybill', function (e) {
                e.preventDefault();
                var orderId = $(this).attr('data-order-id');
                var settingId = $(this).attr('data-setting-id');
                var marketplace = $(this).attr('data-marketplace');
                $('body').modalmanager('loading');
                var _url = $("#_url").val();
                $modal.load(_url + 'marketplace/tracking-number-form/' + marketplace + '/' + orderId + '/' + settingId, '', function () {
                    $modal.modal();
                    $('.amount').autoNumeric('init', {
                        vMax: '9999999999999999.00',
                        vMin: '-9999999999999999.00'
                    });

                });
            });

            $modal.on('change', '#shipment_providers', function (e) {
                e.preventDefault();
                var shipmentProvider = $(this).val();
                if (shipmentProvider) {
                    $(".custom-error").hide()
                }
                if (shipmentProvider === 'Seller-Own') {
                    $("#tracking_number_content").show();
                } else {
                    $("#tracking_number_content").hide();
                }
            });

            $modal.on('click', '#confirm-create-waybill', function (e) {
                e.preventDefault();
                var _url = $("#_url").val();
                var orderId = $('#order_id').val();
                var marketplace = $('#marketplace').val();
                var settingId = $('#marketplace_setting_id').val();
                var shipment_provider = $('#shipment_providers').val();
                var tracking_number = $('#tracking_number').val();
                if (marketplace === 'Lazada') {
                    if (!shipment_provider) {
                        $(".custom-error").show().html('Please select shipment.');
                        return false;
                    }
                    if (shipment_provider === 'Seller-Own' && !tracking_number) {
                        $(".custom-error").show().html('Please enter tracking number.');
                        return false;
                    }
                    window.location = _url + 'marketplace/set-status-packed-marketplace-lazada/' + orderId + '/' + settingId + '/' + shipment_provider + '/' + tracking_number;
                    return true;
                }
                if (marketplace === 'Shopify') {
                    window.location = _url + 'marketplace/create-waybill/' + orderId + '/' + settingId + '/' + tracking_number;
                    return true;
                }
            });

            sysrender.on('click', '.view-fulfilment', function (e) {
                e.preventDefault();
                var orderId = $(this).attr('data-order-id');
                var settingId = $(this).attr('data-setting-id');

                $('body').modalmanager('loading');
                var _url = $("#_url").val();
                $modal.load(_url + 'marketplace/view-fulfilment/' + orderId + '/' + settingId, '', function () {
                    $modal.modal();
                    $('.amount').autoNumeric('init', {
                        vMax: '9999999999999999.00',
                        vMin: '-9999999999999999.00'
                    });

                });
            });

        });


    </script>
{/block}