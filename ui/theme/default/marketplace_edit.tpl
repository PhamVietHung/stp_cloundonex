{extends file="$layouts_admin"}

{block name="content"}
    <div class="row">
        <div class="col-md-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edit </h5>

                </div>
                <div class="ibox-content">

                    <form role="form" name="marketplace-add" method="post" action="{$_url}marketplace/edit-post">
                        <input type="hidden" name="id" value="{$mar['id']}"/>
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" name="title" value="{$mar['title']}">
                        </div>

                        <div class="form-group">
                            <label for="marketplace">Marketplace</label>
                            <select id="marketplace" name="options" class="form-control">
                                <option value="Shopify"
                                        {if {$mar['options']} eq 'Shopify'}
                                            selected="selected"
                                        {/if}
                                >Shopify
                                </option>
                                <option value="Lazada"
                                        {if {$mar['options']} eq 'Lazada'}
                                            selected="selected"
                                        {/if}
                                >Lazada
                                </option>
                            </select>
                        </div>

                        <div class="form-group Lazada"
                                {if {$mar['options']} eq 'Shopify'}
                                {/if}
                        >
                            <label for="country">Country</label>
                            <select id="country" name="country" class="form-control">
                                <option value="sg"
                                        {if {$mar['country']} eq 'sg'}
                                            selected="selected"
                                        {/if}
                                >Singapore
                                </option>
                                <option value="my"
                                        {if {$mar['country']} eq 'my'}
                                            selected="selected"
                                        {/if}
                                >Malaysia
                                </option>
                                <option value="id"
                                        {if {$mar['country']} eq 'id'}
                                            selected="selected"
                                        {/if}
                                >Indonesia
                                </option>
                                <option value="th"
                                        {if {$mar['country']} eq 'th'}
                                            selected="selected"
                                        {/if}
                                >Thailand
                                </option>
                                <option value="ph"
                                        {if {$mar['country']} eq 'ph'}
                                            selected="selected"
                                        {/if}
                                >Philippines
                                </option>
                                <option value="vn"
                                        {if {$mar['country']} eq 'vn'}
                                            selected="selected"
                                        {/if}
                                >Vietnam
                                </option>
                            </select>
                        </div>
                        <div class="form-group url">
                            <label for="url">Url</label>
                            <input type="text" class="form-control" id="url" name="url" value="{$mar['url']}">
                        </div>

                        <div class="form-group">
                            <label for="api_key">Api Key</label>
                            <input type="text" class="form-control" id="api_key" name="api_key" value="{$mar['api_key']}">
                        </div>

                        <div class="form-group">
                            <label for="api_secret">Api Secret</label>
                            <input type="text" class="form-control" id="api_secret" name="api_secret" value="{$mar['api_secret']}">
                        </div>

                        <div class="form-group date-picker">
                            <label for="orders_created_after">Orders Created After</label>
                            <input type="text" class="form-control" id="created_after" name="orders_created_after" datepicker
                                   data-date-format="yyyy-mm-dd" data-auto-close="true"
                                   value="{if {$mar['orders_created_after']} neq ''} {date('Y-m-d', {strtotime({$mar['orders_created_after']})})} {else} {$idate} {/if}">
                        </div>

                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> {$_L['Submit']}</button>

                    </form>

                </div>
            </div>



        </div>



    </div>
{/block}
{block name="script"}
    <script>
        $(document).ready(function () {
            var options = $("#marketplace option:selected").val(),
                formCountry = $('.Lazada'),
                labelUrl = $('.url label'),
                datePicker = $('.date-picker');

            if(options == 'Lazada') {
                labelUrl.text('Callback URL');
            } else {
                formCountry.hide();
                datePicker.hide();
            }

            $('#marketplace').change(function () {
                var options = $("#marketplace option:selected").val();
                if(options){
                    if(options == 'Lazada'){
                        formCountry.show();
                        datePicker.show();
                        labelUrl.text('Callback URL');
                    } else {
                        formCountry.hide();
                        datePicker.hide();
                        labelUrl.text('URL');
                    }
                }
            });
        });
    </script>
{/block}