{extends file="$layouts_admin"}

{block name="content"}
    <div class="row">
        <div class="col-lg-12"  id="application_ajaxrender">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                </div>
                <div class="ibox-content">

                    {if $d['stage'] eq 'Lost'}
                        <div id="ribbon-alert-container">
                            <a href="javascript:void(0)" id="ribbon">{$_L['Lost']}</a>
                        </div>
                    {elseif $d['stage'] eq 'Accepted'}
                        <div id="ribbon-container">
                            <a href="javascript:void(0)" id="ribbon">{$_L['Paid']}</a>
                        </div>
                    {elseif $d['stage'] eq 'Delivered'}
                        <div id="ribbon-container">
                            <a href="javascript:void(0)" id="ribbon">{$_L['Delivered']}</a>
                        </div>
                    {elseif $d['stage'] eq 'Draft'}
                        <div id="ribbon-container">
                            <a href="javascript:void(0)" id="ribbon">{$_L['Draft']}</a>
                        </div>
                    {elseif $d['stage'] eq 'Dead'}
                        <div id="ribbon-alert-container">
                            <a href="javascript:void(0)" id="ribbon">{$_L['Dead']}</a>
                        </div>
                    {elseif $d['stage'] eq 'On Hold'}
                        <div id="ribbon-alert-container">
                            <a href="javascript:void(0)" id="ribbon">{$_L['On Hold']}</a>
                        </div>
                    {elseif $d['stage'] eq 'Received - Complete'}
                        <div id="ribbon-container">
                            <a href="javascript:void(0)" id="ribbon">Received - Complete</a>
                        </div>
                    {elseif $d['stage'] eq 'Received - Incomplete'}
                        <div id="ribbon-container">
                            <a href="javascript:void(0)" id="ribbon">Received - Incomplete</a>
                        </div>
                    {elseif $d['stage'] eq 'Void'}
                        <div id="ribbon-container">
                            <a href="javascript:void(0)" id="ribbon">Void</a>
                        </div>

                    {else}
                        <div id="ribbon-container">

                            <a href="javascript:void(0)" id="ribbon">{$d['stage']}</a>
                        </div>
                    {/if}

                    <div class="invoice">
                        <header class="clearfix">
                            <div class="row">
                                <div class="col-sm-6 mt-md">
                                    <h2 class="h2 mt-none mb-sm text-dark text-bold">Warehouse Inbound</h2>
                                    <h4 class="h4 m-none text-dark text-bold">#{$d['invoicenum']}{if $d['cn'] neq ''} {$d['cn']} {else} {$d['id']} {/if}</h4>

                                    {if ($config['purchase_invoice_payment_status']) eq '1'}

                                        {if $d['status'] eq 'Unpaid'}
                                            <h3 class="alert alert-danger">{$_L['Unpaid']}</h3>
                                        {elseif $d['status'] eq 'Paid'}
                                            <h3 class="alert alert-success">{$_L['Paid']}</h3>
                                        {elseif $d['status'] eq 'Partially Paid'}
                                            <h3 class="alert alert-info">{$_L['Partially Paid']}</h3>
                                        {else}
                                            <h3 class="alert alert-info">{$d['status']}</h3>
                                        {/if}

                                    {/if}



                                    {if $config['invoice_receipt_number'] eq '1' && $d['receipt_number'] neq ''}
                                        <h4>{$_L['Receipt Number']}: {$d['receipt_number']}</h4>
                                        <hr>
                                    {/if}

                                </div>
                                <div class="col-sm-6 text-right" style="margin-top: 45px;">

                                    <div class="ib">
                                        <img src="{$app_url}storage/system/{$config['logo_default']}" alt="Logo" style="margin-bottom: 15px;">
                                    </div>

                                    <address class="ib mr-xlg">
                                        <strong>{$config['CompanyName']}</strong>
                                        <br>
                                        {$config['caddress']}
                                    </address>

                                </div>
                            </div>
                        </header>
                        <div class="bill-info">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="bill-to">
                                        <p class="h5 mb-xs text-dark text-semibold"><strong>{$_L['Supplier']}</strong></p>
                                        <address>
                                            {if $a['company'] neq ''}
                                                {$a['company']}
                                            {else}
                                                {$d['account']}
                                            {/if}
                                        </address>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="bill-data text-right">
                                        <p class="mb-none">
                                            <span class="text-dark">{$_L['Issued at']}:</span>
                                            <span class="value">{date( $config['df'], strtotime($d['date']))}</span>
                                        </p>
                                        {*<p class="mb-none">*}
                                        {*<span class="text-dark">{$_L['Due Date']}:</span>*}
                                        {*<span class="value">{date( $config['df'], strtotime($d['duedate']))}</span>*}
                                        {*</p>*}
                                        {if ($d['credit']) neq '0.00'}
                                            <h2> {$_L['Total Paid']}:  <span class="amount" data-a-sign="{if $d['currency_symbol'] eq ''} {$config['currency_code']} {else} {$d['currency_symbol']}{/if} ">{$d['credit']}</span> </h2>
                                            {*<h2> {$_L['Amount Due']}: {$config['currency_code']} {number_format($i_due,2,$config['dec_point'],$config['thousands_sep'])} </h2>*}
                                            <h2> {$_L['Amount Due']}: <span class="amount" data-a-sign="{if $d['currency_symbol'] eq ''} {$config['currency_code']} {else} {$d['currency_symbol']}{/if} ">{$i_due}</span> </h2>
                                        {/if}
                                    </div>
                                </div>
                            </div>

                            {if $d['subject'] neq ''}
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr>

                                        <strong>{$d['subject']}</strong>

                                        <hr>

                                    </div>
                                </div>
                            {/if}

                        </div>
                        <form method="post" id="post_warehouse">
                            <input type="hidden" name="iid" value="{$d['id']}" id="iid">
                            <div class="table-responsive">
                                <table class="table post_warehouse">
                                    <thead>
                                    <tr class="h5 text-dark">
                                        <th id="cell-id">#</th>
                                        <th id="cell-item">{$_L['Item']}</th>

                                        {*                                    <th id="cell-price" class="text-center text-semibold">{$_L['Price']}</th>*}
                                        <th id="cell-qty">{$_L['Qty']}</th>
                                        {*                                    <th id="cell-total" class="text-center text-semibold">{$_L['Total']}</th>*}
                                        <th>Qty Received</th>
                                        <th>Internal Memo</th>
                                        <th>Status</th>
                                        <th>Date/Time</th>
                                        <th>Username</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    {foreach $items as $item}
                                        <tr>
                                            <input type="hidden" value="{$item['id']}" name="item_ids[]"/>
                                            <input type="hidden" value="{$item['itemcode']}" name="item_code[]"/>
                                            <input type="hidden" value="{$item['description']}" name="item_description[]"/>
                                            <td>{$item['itemcode']}</td>
                                            <td>{$item['description']}</td>

                                            {*                                        <td class="text-center amount" data-a-sign="{if $d['currency_symbol'] eq ''} {$config['currency_code']} {else} {$d['currency_symbol']}{/if} ">{$item['amount']}</td>*}
                                            <td>{$item['qty']}</td>
                                            {*                                        <td class="text-center amount" data-a-sign="{if $d['currency_symbol'] eq ''} {$config['currency_code']} {else} {$d['currency_symbol']}{/if} ">{$item['total']}</td>*}
                                            <td><input aria-label="Qty Received" type="number" value="{$item['qty_received']}" name="qty_received[]" style="width: 100px"/></td>
                                            <td><textarea aria-label="Internal memo" type="text" name="internal_memo[]" >{$item['internal_memo']}</textarea></td>
                                            <td>
                                                <select name="status[]" >
                                                    <option value="Missing Items"
                                                            {if $item['status'] eq 'Missing Items'}
                                                                selected="selected"
                                                            {/if}
                                                    >Missing Items</option>
                                                    <option value="Damaged Items"
                                                            {if $item['status'] eq 'Damaged Items'}
                                                                selected="selected"
                                                            {/if}
                                                    >Damaged Items</option>
                                                    <option value="Different Items"
                                                            {if $item['status'] eq 'Different Items'}
                                                                selected="selected"
                                                            {/if}
                                                    >Different Items</option>r
                                                    <option value="Different Specs"
                                                            {if $item['status'] eq 'Different Specs'}
                                                                selected="selected"
                                                            {/if}
                                                    >Different Specs</option>
                                                    <option value="All Good"
                                                            {if $item['status'] eq 'All Good'}
                                                                selected="selected"
                                                            {/if}
                                                    >All Good</option>
                                                </select>
                                            </td>
                                            <td>{$item['date_time']}</td>
                                            <td>{$item['username']}</td>
                                            <td class="project-actions">
                                                <a href="{$U}warehouse/barcode/{$item['itemcode']}/{base64_encode($item['description'])}" target="_blank" class="btn btn-inverse btn-sm"><i class="fa fa-barcode"></i>{$_L['Barcode']}</a>
                                            </td>
                                        </tr>
                                    {/foreach}

                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>

                </div>

                <div class='row'>
                    <div class='col-md-11'>
                        <div class="text-right">
                            <button class="btn btn-primary" id="submit"> {$_L['Save']}</button>
                            <button class="btn btn-info" id="save_n_close"> {$_L['Save n Close']}</button>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>


{/block}

{block name="script"}
    <script>


        $("#submit").click(function (e) {
            e.preventDefault();
            var _url = $("#_url").val();
            bootbox.confirm("Are You Sure?", function(result) {
                if(result){
                    $.post(_url + 'warehouse/save/', $('#post_warehouse').serialize(), function(data){

                        if ($.isNumeric(data)) {

                            window.location = _url + 'warehouse/view/' + data + '/';
                        }
                        else {
                            $('#ibox_form').unblock();
                            var body = $("html, body");
                            body.animate({ scrollTop:0 }, '1000', 'swing');
                            $("#emsgbody").html(data);
                            $("#emsg").show("slow");
                        }
                    });
                }
            });

        });

        $("#save_n_close").click(function (e) {
            e.preventDefault();
            $('#ibox_form').block({ message: null });
            var _url = $("#_url").val();
            bootbox.confirm("Are You Sure?", function(result) {
                if(result){
                    $.post(_url + 'warehouse/save/', $('#post_warehouse').serialize(), function(data){

                        var _url = $("#_url").val();
                        if ($.isNumeric(data)) {

                            window.location = _url + 'warehouse/list/';
                        }
                        else {
                            $('#ibox_form').unblock();
                            var body = $("html, body");
                            body.animate({ scrollTop:0 }, '1000', 'swing');
                            $("#emsgbody").html(data);
                            $("#emsg").show("slow");
                        }
                    });
                }
            });
        });


    </script>
{/block}