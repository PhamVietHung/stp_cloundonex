{extends file="$layouts_admin"}

{block name="content"}


    <div class="row">
        <div class="col-md-12">
            <h3 class="ibilling-page-header">Re-order</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a class="nav-link active show" href="{$_url}reports/reorder"> Re-order</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active show">
                        <div class="panel-body panel-body-with-border" style="background-color: #fff;">

{*                            <div class="row">*}
{*                                <div class="col-md-12">*}
{*                                    <form role="form" class="form-inline">*}
{*                                        <div class="form-group">*}
{*                                            <input style="min-width: 250px;" type="text" name="reportrange" class="form-control" id="reportrange">*}
{*                                        </div>*}

{*                                        <button class="btn btn-primary" id="ib_filter" type="submit">{$_L['Filter']}</button>*}
{*                                    </form>*}

{*                                </div>*}
{*                            </div>*}

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive" id="ib_data_panel">


                                        <table class="table table-bordered table-hover display" id="ib_dt">
                                            <thead>
                                            <tr class="heading">
                                                <th>SKU #</th>
                                                <th>Name</th>
                                                <th class="text-right">Current Level</th>
                                                <th class="text-right">Re-order Level</th>
                                                <th class="text-right">{$_L['Due']}</th>


                                                <th>{$_L['Date']}</th>

                                                <th>{$_L['Manage']}</th>
                                            </tr>
                                            </thead>

                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>


{/block}


{block name="script"}



    <script>


        $(function () {


            var $ib_data_panel = $("#ib_data_panel");

            $ib_data_panel.block({ message:block_msg });

            var $cid = $('#cid');



            $cid.select2({
                theme: "bootstrap"
            });

            var ib_dt = $('#ib_dt').DataTable( {

                "serverSide": false,
                "ajax": {
                    "url": base_url + "reports/json_reorder/",
                    "type": "POST",
                    "data": function ( d ) {
                        d.cid = $cid.val();
                    }
                },
                "pageLength": 10,
                "responsive": true,
                dom: "<'row'<'col-sm-6'i><'col-sm-6'B>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'><'col-sm-7'p>>",
                fixedHeader: {
                    headerOffset: 50
                },
                lengthMenu: [
                    [ 10, 25, 50, -1 ],
                    [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                ],
                buttons: [
                    {
                        extend:    'excelHtml5',
                        text:      '<i class="fa fa-file-excel-o"></i>',
                        titleAttr: 'Excel',
                        exportOptions: {
                            columns: [0,1,2,3]
                        }
                    },
                    {
                        extend:    'csvHtml5',
                        text:      '<i class="fa fa-file-text-o"></i>',
                        titleAttr: 'CSV',
                        exportOptions: {
                            columns: [0,1,2,3]
                        }
                    },
                    {
                        extend:    'pdfHtml5',
                        text:      '<i class="fa fa-file-pdf-o"></i>',
                        titleAttr: 'PDF',
                        exportOptions: {
                            columns: [0,1,2,3]
                        }
                    },
                    {
                        extend:    'print',
                        text:      '<i class="fa fa-print"></i>',
                        titleAttr: 'Print',
                        exportOptions: {
                            columns: [0,1,2,3]
                        }
                    },
                    {
                        extend:    'pageLength',
                        text:      '<i class="fa fa-bars"></i>',
                        titleAttr: 'Entries'
                    }
                ],
                "columnDefs": [
                    {
                        "targets": [ 4 ],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [ 5 ],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [ 6 ],
                        "visible": false,
                        "searchable": false
                    }
                ],
                "order": [[ 0, 'desc' ]],
                "scrollX": false,
                "initComplete": function () {
                    $ib_data_panel.unblock();
                },
                "footerCallback": function ( row, data, start, end, display ) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                }
            } );

        });

    </script>
{/block}