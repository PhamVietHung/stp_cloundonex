{extends file="$layouts_admin"}

{block name="content"}

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <div class="ibox-tools">
                <a href="{$_url}quotes/new/" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Create New Quotes/Sales Order</a>
            </div>
        </div>
        {*start custom upload quotations*}
        {*<br>*}
        {*<div class="upload-btn-wrapper custom-upload">*}
            {*<button class="btn btn-custom-upload"><i class="fa fa-upload"></i> Upload Quotations</button>*}
            {*<input type="file" name="file" id="file"/>*}
            {*<br>*}
            {*<div id="uploaded_quotations_message"></div>*}
        {*</div>*}
        {*<a href="{$_url}quotes/viewPdf/">View Pdf</a>*}
        {*end custom*}

        <div class="ibox-title">
            <h5>{$_L['Total']} : {$total_quote}</h5>
        </div>
        <div class="ibox-content">

            <form class="form-horizontal" method="post" action="{$_url}customers/list/">
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span class="fa fa-search"></span>
                            </div>
                            <input type="text" name="name" id="foo_filter" class="form-control" placeholder="{$_L['Search']}..."/>

                        </div>
                    </div>

                </div>
            </form>

            <table class="table table-bordered table-hover sys_table footable" data-filter="#foo_filter" data-page-size="50">
                <thead>
                <tr>
                    <th>#</th>
                    <th>{$_L['Account']}</th>
                    <th>Type</th>
                    <th>{$_L['Subject']}</th>
                    <th>{$_L['Amount']}</th>
                    <th>{$_L['Date Created']}</th>
                    <th>{$_L['Expiry Date']}</th>
                    <th>{$_L['Stage']}</th>

                    <th class="text-right">{$_L['Manage']}</th>
                </tr>
                </thead>
                <tbody>

                {foreach $d as $ds}
                    <tr>
                        <td><a href="{$_url}quotes/view/{$ds['id']}/">{$ds['id']}</a> </td>
                        <td><a href="{$_url}contacts/view/{$ds['userid']}/">{$ds['account']}</a> </td>

                        {if $ds['type'] eq 'quotes'}
                            <td>Q</td>
                        {else}
                            <td>SO</td>
                        {/if}

                        <td><a href="{$_url}quotes/view/{$ds['id']}/">{$ds['subject']}</a> </td>
                        <td class="amount" data-a-sign="
                                {if !empty($ds['currency_symbol']) }
                                    {$ds['currency_symbol']}
                                {else}
                                    {$config['currency_code']}
                                {/if}
                                ">{$ds['total']}</td>
                        <td>{date( $config['df'], strtotime($ds['datecreated']))}</td>
                        <td>{date( $config['df'], strtotime($ds['validuntil']))}</td>
                        <td>
                            {if $ds['stage'] eq 'Dead'}
                                <span class="label label-default">{$_L['Dead']}</span>
                            {elseif $ds['stage'] eq 'Lost'}
                                <span class="label label-danger">{$_L['Lost']}</span>
                            {elseif $ds['stage'] eq 'Accepted'}
                                <span class="label label-success">{$_L['Accepted']}</span>
                            {elseif $ds['stage'] eq 'Draft'}
                                <span class="label label-info">{$_L['Draft']}</span>
                            {elseif $ds['stage'] eq 'Delivered'}
                                <span class="label label-info">{$_L['Delivered']}</span>
                            {else}
                                <span class="label label-info">{$ds['stage']}</span>
                            {/if}

                        </td>

                        <td class="text-right">
                            <a href="{$_url}quotes/view/{$ds['id']}/" class="btn btn-primary btn-xs"><i class="fa fa-check"></i> {$_L['View']}</a>
                            <a href="{$_url}quotes/edit/{$ds['id']}/" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> {$_L['Edit']}</a>
                            <a href="#" class="btn btn-danger btn-xs cdelete" id="iid{$ds['id']}"><i class="fa fa-trash"></i> {$_L['Delete']}</a>
                        </td>
                    </tr>
                {/foreach}

                </tbody>
                <tfoot>
                <tr>
                    <td colspan="9">
                        <ul class="pagination">
                        </ul>
                    </td>
                </tr>
                </tfoot>
            </table>
            {$paginator['contents']}
        </div>
    </div>
{/block}

{block name="script"}
    <script>
        $(document).ready(function(){
            $(document).on('change', '#file', function(){
                var name = document.getElementById("file").files[0].name;
                var form_data = new FormData();
                var ext = name.split('.').pop().toLowerCase();
                if(jQuery.inArray(ext, ['gif','png','jpg','jpeg','pdf']) == -1)
                {
                    alert("Invalid Image File");
                }
                var oFReader = new FileReader();
                oFReader.readAsDataURL(document.getElementById("file").files[0]);
                var f = document.getElementById("file").files[0];
                var fsize = f.size||f.fileSize;
                if(fsize > 2000000)
                {
                    alert("Image File Size is very big");
                }
                else
                {
                    form_data.append("file", document.getElementById('file').files[0]);
                    $.ajax({
                        url:"{$_url}quotes/upload/",
                        method:"POST",
                        data: form_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        beforeSend:function(){
                            $('#uploaded_quotations_message').show().html("<label class='text-success'>Image Uploading...</label>");
                        },
                        success:function(data)
                        {
                            $('#uploaded_quotations_message').html("<label class='text-success'> Upload successful.</label>");
                            setTimeout(function () {
                                $('#uploaded_quotations_message').hide('blind', 500);
                            }, 5 * 1000);
                        }
                    });
                }
            });
        });
    </script>
{/block}