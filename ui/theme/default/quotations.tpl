{extends file="$layouts_admin"}

{block name="content"}
    <div class="row">


        <div class="col-md-12">


            <div class="panel panel-default">
                <div class="panel-body">

                    <a data-toggle="modal" href="#modal_add_item"
                       class="btn btn-primary add_document waves-effect waves-light" id="add_document"><i
                                class="fa fa-plus"></i>
                        {if $actionType eq 'quotations'}
                            Upload Quote
                        {elseif $actionType eq 'invoices'}
                            Upload Invoice
                        {elseif $actionType eq 'payment'}
                            Upload Payment
                        {/if}
                    </a>


                </div>

            </div>
        </div>


    </div>
    <div class="row">


        <div class="col-md-12">


            <div class="panel panel-default">

                <div class="panel-body">

                    <form class="form-horizontal" method="post" action="">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="fa fa-search"></span>
                                    </div>
                                    <input type="text" name="name" id="foo_filter" class="form-control"
                                           placeholder="{$_L['Search']}..."/>

                                </div>
                            </div>

                        </div>
                    </form>

                    <table class="table table-bordered table-hover sys_table footable" id="footable_tbl"
                           data-filter="#foo_filter" data-page-size="50">
                        <thead>
                        <tr>

                            <th class="text-right" data-sort-ignore="true" width="20px;">{$_L['Type']}</th>

                            <th>{$_L['Title']}</th>

                            <th>Supplier Name</th>

                            <th>{ucwords($actionType)} Amount</th>

                            <th>Received Date</th>

                            <th>Upload Date</th>
                            {if $actionType eq 'quotations'}

                            {else}
                                <th>Status</th>
                            {/if}

                            <th class="text-right" data-sort-ignore="true" width="200px;">{$_L['Manage']}</th>

                        </tr>
                        </thead>
                        <tbody>

                        {foreach $d as $ds}
                            <tr>

                                <td>
                                    {if $ds['file_mime_type'] eq 'jpg' || $ds['file_mime_type'] eq 'png' || $ds['file_mime_type'] eq 'gif'}
                                        <i class="fa fa-file-image-o"></i>
                                    {elseif $ds['file_mime_type'] eq 'pdf'}
                                        <i class="fa fa-file-pdf-o"></i>
                                    {elseif $ds['file_mime_type'] eq 'zip'}
                                        <i class="fa fa-file-archive-o"></i>
                                    {else}
                                        <i class="fa fa-file"></i>
                                    {/if}
                                </td>


                                <td>

                                    <a href="{$_url}documents/view/{$ds['id']}/" target="_blank">{$ds['title']}</a>

                                </td>

                                <td>
                                    <a href="{$_url}documents/view/{$ds['id']}/"
                                       target="_blank">{$ds['supplier_name']}</a>
                                </td>

                                <td>{formatCurrency($ds['quote_amount'],$config['home_currency'])}</td>

                                <td>{$ds['created_at']}</td>

                                <td>{$ds['quote_received_date']}</td>

                                {if $actionType eq 'quotations'}

                                {else}
                                    <td>{$ds['status']}</td>
                                {/if}

                                <td class="text-right">

                                    {*<a href="#" class="btn btn-primary btn-xs c_link" data-token="{$ds['id']}_{$ds['file_dl_token']}"><i class="fa fa-link"></i> </a>*}

                                    <a href="{$_url}documents/view/{$ds['id']}/" class="btn btn-success btn-xs"
                                       target="_blank"><i class="fa fa-download"></i> </a>

                                    <a href="#" class="btn btn-danger btn-xs cdelete" id="did{$ds['id']}"><i
                                                class="fa fa-trash"></i> </a>
                                </td>
                            </tr>
                        {/foreach}

                        </tbody>

                        <tfoot>
                        <tr>
                            <td colspan="10">
                                <ul class="pagination">
                                </ul>
                            </td>
                        </tr>
                        </tfoot>

                    </table>

                </div>
            </div>
        </div>


    </div>
    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary waves-effect waves-light add_document" data-toggle="modal"
           href="#modal_add_item">
            <i class="fa fa-plus"></i>
        </a>
    </div>
    <div id="modal_add_item" class="modal fade" tabindex="-1" data-width="760" style="display: none;">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">
                {if $actionType eq 'quotations'}
                    Upload Quote
                {elseif $actionType eq 'invoices'}
                    Upload Invoice
                {elseif $actionType eq 'payment'}
                    Upload Payment
                {/if}
            </h4>
        </div>
        <div class="modal-body">
            <div class="row">

                <div class="col-md-12">

                    <form id="form_upload_document">
                        <div class="form-group">
                            <label for="doc_title_quotations">{$_L['Title']}</label>
                            <input type="text" class="form-control" id="doc_title_quotations"
                                   name="doc_title_quotations">

                        </div>
                        <div class="form-group quote_amount_quotations">
                            <label for="quote_amount_quotations">
                                {if $actionType eq 'quotations'}
                                    Quote Amount
                                {elseif $actionType eq 'invoices'}
                                    Invoice Amount
                                {elseif $actionType eq 'payment'}
                                    Amount Paid
                                {/if}

                            </label>
                            <span></span><input type="text" class="form-control" id="quote_amount_quotations" name="quote_amount_quotations">

                        </div>
                        <div class="form-group">
                            <label for="quote_received_date">
                                {if $actionType eq 'quotations'}
                                    Quote Date
                                {elseif $actionType eq 'invoices'}
                                    Invoice Date
                                {elseif $actionType eq 'payment'}
                                    Payment Date
                                {/if}
                            </label>
                            <input type="text" class="form-control" id="quote_received_date" name="quote_received_date"
                                   datepicker
                                   data-date-format="yyyy-mm-dd" data-auto-close="true" value="{$idate}">

                        </div>
                        {if $actionType eq 'quotations'}

                        {else}
                            <div class="form-group">
                                <label for="status_invoices">Status</label>
                                <select id="status_invoices" name="status_invoices" class="form-control">
                                    <option value="Full">Full</option>
                                    <option value="Partial">Partial</option>
                                </select>
                            </div>
                        {/if}

                        <div class="form-group">
                            <label for="supplier_quotations">Supplier</label>
                            <select id="supplier_quotations" name="supplier_quotations" class="form-control">
                                <option value="">{$_L['Select']}...</option>
                                {foreach $c as $cs}
                                    <option value="{$cs['id']}"
                                            {if $p_cid eq ($cs['id'])}selected="selected" {/if}>{$cs['account']} {if $cs['email'] neq ''}- {$cs['email']}{/if}</option>
                                {/foreach}

                            </select>

                        </div>
                        {if $actionType eq 'invoices'}
                            <div class="form-group">
                                <label for="purchase_order">Purchase Order</label>
                                <select id="purchase_order" name="purchase_order" class="form-control">
                                    <option value="">{$_L['Select']}...</option>
                                    {foreach $po as $p}
                                        <option value="{$p['id']}">{$p['subject']}</option>
                                    {/foreach}
                                </select>
                            </div>
                        {/if}
                        {if $actionType eq 'payment'}
                            <div class="form-group">
                                <label for="invoice">Invoice</label>
                                <select id="invoice" name="invoice" class="form-control">
                                    <option value="">{$_L['Select']}...</option>
                                    {foreach $invoice as $i}
                                        <option value="{$i['id']}">{$i['account']}</option>
                                    {/foreach}
                                </select>
                            </div>
                        {/if}

                        {*<div class="checkbox">*}
                        {*<label>*}
                        {*<input type="checkbox" id="is_global" name="is_global"> {$_L['Available for all Customers']}*}
                        {*</label>*}
                        {*</div>*}


                    </form>

                    <hr>

                    <form action="" class="dropzone" id="upload_container">

                        <div class="dz-message">
                            <h3><i class="fa fa-cloud-upload"></i> {$_L['Drop File Here']}</h3>
                            <br/>
                            <span class="note">{$_L['Click to Upload']}</span>
                        </div>

                    </form>
                    <hr>
                    <h4>{$_L['Server Config']}:</h4>
                    <p>{$_L['Upload Maximum Size']}: {$upload_max_size}</p>
                    <p>{$_L['POST Maximum Size']}: {$post_max_size}</p>

                </div>

            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="file_link_quotations" id="file_link_quotations" value="">
            <input type="hidden" name="action_type" id="action_type" value="{$actionType}">
            <button type="button" data-dismiss="modal" class="btn btn-danger">{$_L['Close']}</button>
            <button type="button" id="btn_add_file_quotations" class="btn btn-primary">{$_L['Submit']}</button>
        </div>

    </div>
{/block}
{block name="script"}
    <script>
        $(document).ready(function () {
            var _url = $("#_url").val();
            var file_link_quotations = $("#file_link_quotations");
            var btn_add_file_quotations = $("#btn_add_file_quotations");
            var doc_title_quotations = $("#doc_title_quotations");
            var quote_amount_quotations = $("#quote_amount_quotations");
            var supplier_quotations = $("#supplier_quotations");
            var action_type = $("#action_type");
            var status_invoices = $("#status_invoices");
            var quote_received_date = $("#quote_received_date");
            var po = $("#purchase_order");
            var invoice = $("#invoice");

            btn_add_file_quotations.on('click', function (e) {
                e.preventDefault();

                var dataPost;

                if (action_type.val() === 'invoices' || action_type.val() === 'payment') {
                    dataPost = {
                        title: doc_title_quotations.val(),
                        amount: quote_amount_quotations.val(),
                        quote_received_date: quote_received_date.val(),
                        supplier: supplier_quotations.val(),
                        file_link: file_link_quotations.val(),
                        action_type: action_type.val(),
                        status: status_invoices.val(),
                        po: po.val(),
                        invoice: invoice.val(),
                    };
                } else {
                    dataPost = {
                        title: doc_title_quotations.val(),
                        amount: quote_amount_quotations.val(),
                        quote_received_date: quote_received_date.val(),
                        supplier: supplier_quotations.val(),
                        file_link: file_link_quotations.val(),
                        action_type: action_type.val(),
                        status: null
                    };
                }

                $.post(_url + "upload/post/", dataPost)
                    .done(function (data) {

                        if ($.isNumeric(data)) {

                            location.reload();

                        } else {
                            toastr.error(data);
                        }

                    });


            });
            $("#supplier_quotations").on("change", function () {
                var actionType = $("#action_type").val();
                var supId = $(this).val();
                $.get(_url + "upload/getpo/" + actionType + '/' + supId, function (data) {
                    if(actionType === 'invoices') {
                        $('#purchase_order').html(data);
                    }else {
                        $('#invoice').html(data);
                    }
                });
            });
            $('#modal_add_item').on('focus', '#quote_amount_quotations', function(){
                var currencyInput = '{$config['currency_code']}';
                $(this).siblings('span').html(currencyInput);
            });
            $('#modal_add_item').on('focusout', '#quote_amount_quotations', function(){
                $(this).siblings('span').html('');
            });
        });
    </script>
{/block}