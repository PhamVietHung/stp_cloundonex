{extends file="$layouts_admin"}

{block name="content"}


    <div class="row">
        <div class="col-md-12">
            <h3>Warehouse Inbound</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">

                    {if $view_type == 'filter'}
                        <form class="form-horizontal" method="post" action="">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="fa fa-search"></span>
                                        </div>
                                        <input type="text" name="name" id="foo_filter" class="form-control" placeholder="{$_L['Search']}..."/>

                                    </div>
                                </div>

                            </div>
                        </form>
                    {/if}

                    <table class="table table-bordered table-hover sys_table footable" {if $view_type == 'filter'} data-filter="#foo_filter" data-page-size="50" {/if}>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Supplier Name</th>
                            <th>{$_L['Issued at']}</th>
                            <th>
                                {$_L['Status']}
                            </th>
                            <th>{$_L['Type']}</th>
                            <th class="text-right" width="120px;">{$_L['Manage']}</th>
                        </tr>
                        </thead>
                        <tbody>

                        {foreach $d as $ds}
                            <tr>
                                <td  data-value="{$ds['id']}">{$ds['invoicenum']}{if $ds['cn'] neq ''} {$ds['cn']} {else} {$ds['id']} {/if} </td>
                                <td>{$ds['account']}</td>
                                <td data-value="{strtotime($ds['date'])}">{date( $config['df'], strtotime($ds['date']))}</td>
                                <td>

                                    {if $ds['stage'] eq 'Dead'}
                                        <span class="label label-default">{$_L['Dead']}</span>
                                    {elseif $ds['stage'] eq 'Lost'}
                                        <span class="label label-danger">{$_L['Lost']}</span>
                                    {elseif $ds['stage'] eq 'Accepted'}
                                        <span class="label label-success">{$_L['Paid']}</span>
                                    {elseif $ds['stage'] eq 'Draft'}
                                        <span class="label label-info">{$_L['Draft']}</span>
                                    {elseif $ds['stage'] eq 'Delivered'}
                                        <span class="label label-info">{$_L['Delivered']}</span>
                                    {else}
                                        <span class="label label-info">{$ds['stage']}</span>
                                    {/if}

                                </td>
                                <td>
                                    {if $ds['r'] eq '0'}
                                        <span class="label label-default"><i class="fa fa-dot-circle-o"></i> {$_L['Onetime']}</span>
                                    {else}
                                        <span class="label label-default"><i class="fa fa-repeat"></i> {$_L['Recurring']}</span>
                                    {/if}
                                </td>
                                <td class="text-right">


                                    <a href="{$_url}warehouse/view/{$ds['id']}/" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="{$_L['Edit']}"><i class="fa fa-pencil"></i></a>


                                </td>
                            </tr>
                        {/foreach}

                        </tbody>

                        {if $view_type == 'filter'}
                            <tfoot>
                            <tr>
                                <td colspan="8">
                                    <ul class="pagination">
                                    </ul>
                                </td>
                            </tr>
                            </tfoot>
                        {/if}

                    </table>
                    {$paginator['contents']}
                </div>
            </div>
        </div>
    </div>
{/block}