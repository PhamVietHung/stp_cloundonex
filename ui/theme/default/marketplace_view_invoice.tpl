{extends file="$layouts_admin"}

{block name="content"}

    <div class="row">
        <div class="col-lg-12" id="application_ajaxrender">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="btn-group  pull-right" role="group" aria-label="...">
                        {if $type eq 'Shopify' }
                            <a href="{$_url}marketplace/shopify-order-list/" class="btn  btn-danger btn-sm"><i
                                        class="fa fa-window-close" aria-hidden="true"></i> {$_L['Close']}</a>
                        {else}
                            <a href="{$_url}marketplace/lazada-order-list/{$status}" class="btn  btn-danger btn-sm"><i
                                        class="fa fa-window-close" aria-hidden="true"></i> {$_L['Close']}</a>
                        {/if}
                        <a href="{$_url}marketplace/view-invoice/{$order_id}/{$type}/print" target="_blank"
                           class="btn btn-inverse  btn-sm"><i class="fa fa-print"></i> {$_L['Print']}</a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div id="ribbon-container">
                        <a href="javascript:void(0)" id="ribbon">{$statusOrder}</a>
                    </div>
                    <div class="invoice">
                        <header class="clearfix">
                            <div class="row">
                                <div class="col-sm-6 mt-md" style="top:130px">
                                    <b class="mt-none mb-sm text-dark text-bold">INVOICE  # {$order_id}</b><br>
                                    <b class="mt-none mb-sm text-dark text-bold">ORDER  # {$order_number}</b><br>
                                    <b class="mt-none mb-sm text-dark text-bold">DATE {date( $config['df'], strtotime($date))}</b>
                                </div>
                                <div class="col-sm-6 text-right" style="margin-top: 45px;">

                                    <div class="ib">
                                        <img src="{$app_url}storage/system/{$config['logo_default']}" alt="Logo" style="margin-bottom: 15px;">
                                    </div>

                                    <address class="ib mr-xlg">
                                        <strong>{$config['CompanyName']}</strong>
                                        <br>
                                        {$config['caddress']}
                                    </address>

                                </div>
                            </div> <hr  noshade color="red"  align="center" />
                        </header>

                        <div class="bill-info" style="margin-top: 50px; margin-bottom: 100px;">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="bill-to" style="float: left;">
                                        <p class="h5 mb-xs text-dark text-semibold"><strong>Invoiced To</strong></p>
                                        <address>
                                            {if isset($shipping_address['company']) }
                                                <strong>Company :</strong> {$shipping_address['company']}
                                                <br>
                                            {/if}

                                            <strong>ATTN :</strong>
                                            {if isset($shipping_address['first_name']) }
                                                {$shipping_address['first_name']}
                                            {/if}
                                            {if isset($shipping_address['last_name']) }
                                                {$shipping_address['last_name']}
                                            {/if}
                                            <br>
                                            {if isset($shipping_address['address1']) }
                                                {$shipping_address['address1']}
                                                <br>
                                            {/if}
                                            {if isset($shipping_address['province']) }
                                                {$shipping_address['province']}
                                            {/if}
                                            {if isset($shipping_address['city']) }
                                                {$shipping_address['city']}
                                            {/if}
                                            {if isset($shipping_address['zip']) }
                                                {$shipping_address['zip']}

                                            {/if}
                                            <br>
                                            {if isset($shipping_address['country']) }
                                                {$shipping_address['country']}
                                                <br>
                                            {/if}
                                            {if isset($shipping_address['phone']) }
                                                <strong>{$_L['Phone']}:</strong> {$shipping_address['phone']}
                                                <br>
                                            {/if}
                                            {if isset($customer['email'])  }
                                                <strong>Email :</strong> {$customer['email']}
                                            {else}
                                                {if isset($shipping_address['email']) }
                                                    <strong>{$_L['Email']}:</strong>  {$shipping_address['email']}
                                                {/if}
                                            {/if}

                                        </address>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="bill-to">
                                        <p class="h5 mb-xs text-dark text-semibold"><strong>Billing Address: :</strong></p>
                                        <address>
                                            {if isset($address_billing['company']) }
                                                <strong>Company :</strong> {$address_billing['company']}
                                            {/if}
                                            {if isset($address_billing['first_name']) }
                                                {$address_billing['first_name']}
                                            {/if}
                                            {if isset($address_billing['last_name']) }
                                                {$address_billing['last_name']}
                                            {/if}
                                            <br>
                                            {if isset($address_billing['address1']) }
                                                <strong>Address :</strong> {$address_billing['address1']}
                                                <br>
                                            {/if}
                                            {if isset($address_billing['province']) }
                                                <strong>Province :</strong> {$address_billing['province']}
                                            {/if}
                                            {if isset($address_billing['city']) }
                                                {$address_billing['city']}
                                            {/if}
                                            {if isset($address_billing['zip']) }
                                                {$address_billing['zip']}
                                                <br>
                                            {/if}

                                            {if isset($address_billing['country']) }
                                                <strong>Country :</strong> {$address_billing['country']}
                                                <br>
                                            {/if}
                                            {if isset($address_billing['phone']) }
                                                <strong>{$_L['Phone']}:</strong> {$address_billing['phone']}
                                                <br>
                                            {/if}

                                        </address>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <form method="post" id="post_warehouse">
                            <div class="table-responsive">
                                <table class="table invoice-items" >
                                    <thead>
                                    <tr class="h5 text-dark">
                                        <th id="cell-id">#</th>
                                        <th id="cell-item">SELLER SKU</th>
                                        <th id="cell-item">NAME </th>
                                        <th id="cell-qty">QTY</th>
                                        <th id="cell-qty">UNIT PRICE</th>
                                        <th id="cell-qty">TOTAL PRICE</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    {foreach $items as $item}
                                        <tr>
                                            {if isset($item['order_item_id']) }
                                                <th id="cell-id">{$item['order_item_id']}</th>
                                            {else}
                                                <th id="cell-id"></th>
                                            {/if}
                                            {if isset($item['sku']) }
                                                <th id="cell-id">{$item['sku']}</th>
                                            {else}
                                                <th id="cell-id"></th>
                                            {/if}
                                            {if isset($item['name']) }
                                                <th id="cell-id">{$item['name']}</th>
                                            {else}
                                                <th id="cell-id"></th>
                                            {/if}
                                            {if isset($item['quantity']) }
                                                <th id="cell-id">{$item['quantity']}</th>
                                            {else}
                                                <th id="cell-id">1</th>
                                            {/if}
                                            {if isset($item['item_price']) }
                                                <th id="cell-id">{$item['item_price']}</th>
                                            {else}
                                                <th id="cell-id"></th>
                                            {/if}
                                            {if isset($item['item_price']) }
                                                <th id="cell-id">{$item['item_price']} {$currency}</th>
                                            {else}
                                                <th id="cell-id"></th>
                                            {/if}
                                        </tr>
                                    {/foreach}

                                    </tbody>
                                </table>
                            </div>
                        </form>
                        <div class="invoice-summary">
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-8">
                                    <table class="table h5 text-dark">
                                        <tbody>
                                        <tr class="b-top-none">
                                            <td>{$_L['Subtotal']}</td>
                                            <td style="text-align: right;" class="text-left amount">{$subtotal_price} {$currency}</td>
                                        </tr>
                                        <tr>
                                            <td>Shipping fee:</td>
                                            <td style="text-align: right;" class="text-left amount">{$shipping_fee}</td>
                                        </tr>
                                        <tr>
                                            <td>{$type} Discount Total:</td>
                                            <td style="text-align: right;" class="text-left amount">{$total_discounts}</td>
                                        </tr>
                                        <tr>
                                            <td>Seller Discount Total:</td>
                                            <td style="text-align: right;" class="text-left amount">{$total_discounts}</td>
                                        </tr>
                                        {if {$total_tax}!= 0 }
                                            <tr>
                                                <td>{$_L['TAX']}</td>
                                                <td style="text-align: right;" class="text-left amount">{$total_tax}</td>
                                            </tr>
                                        {/if}
                                        <tr>
                                            <td>{$_L['Grand Total']}</td>
                                            <td style="text-align: right;" class="text-left amount">{$total} {$currency}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
{/block}