$(document).ready(function () {
    $(".progress").hide();
    $("#emsg").hide();

    var _url = $("#_url").val();
    var ib_submit = $("#submit");

    $('#description').redactor(
        {
            minHeight: 200 // pixels
        }
    );

    ib_submit.click(function (e) {
        e.preventDefault();
        $('#ibox_form').block({message: null});
        var _url = $("#_url").val();
        $.post(_url + 'ps/edit-bundle-post/', $("#rform").serialize())
            .done(function (data) {

                if ($.isNumeric(data)) {
                    window.location = _url + 'ps/p-list/bundled';
                } else {
                    $('#ibox_form').unblock();

                    $("#emsgbody").html(data);
                    $("#emsg").show("slow");
                }
            });
    });

    $("#is_bundled").change(function () {

        if (this.checked) {
            $("#button_add_bundled").css("display", "block");
        } else {
            $("#button_add_bundled").css("display", "none");
        }
    });

    var $modal = $('#ajax-modal');
    var item_remove = $('#item-remove');
    var item_list = $("#bundled_items");
    $('#item-add').on('click', function () {
        // create the backdrop and wait for next modal to be triggered
        $('body').modalmanager('loading');
        $modal.load(_url + 'ps/modal-list/exBundled', '', function () {
            $modal.modal();
        });
    });

    $modal.on('click', '.update', function () {
        var tableControl = document.getElementById('items_table');
        $modal.modal('loading');

        $('input:checkbox:checked', tableControl).each(function () {

            var item_code = $(this).closest('tr').find('td:eq(1)').text();
            var item_name = $(this).closest('tr').find('td:eq(2)').text();

            var item_price = $(this).closest('tr').find('td:eq(3)').text();

            var qtyInput = $('input[data-item-code-qty="' + item_code + '"]');
            var priceInput = $('input[data-item-code-price="' + item_code + '"]');
            var totalInput = $('input[data-item-code-total="' + item_code + '"]');

            if (qtyInput.length > 0) {
                qtyInput.val(parseInt(qtyInput.val()) + 1);
                totalInput.val((parseFloat(totalInput.val()) + parseFloat(item_price)).toString() + '.00');
            } else {
                item_list.css('display', 'block');
                item_list.find('tbody')
                    .append(
                        '<tr> ' +
                        '<td>' + item_code + '</td>' +
                        ' <td><textarea class="form-control item_name" name="desc[]" rows="3">' + item_name + '</textarea></td>' +
                        '<input type="hidden" value="' + item_code + '" name="item_code[]">' +
                        '<td><input type="number" data-item-code-qty="' + item_code + '" min="0" max="99" class="form-control qty" value="1" name="qty[]"></td> ' +
                        '<td><input type="text" data-item-code-price="' + item_code + '" data-item-price="' + item_price + '" class="form-control item_price" name="amount[]" value="' + item_price + '"></td> ' +
                        '<td class="ltotal"><input data-item-code-total="' + item_code + '" type="text" class="form-control lvtotal" readonly name="total[]" value="' + item_price + '"></td> ' +
                        '<td> <select class="form-control taxed" name="taxed[]"> <option value="Yes">Yes</option> <option value="No" selected>No</option></select></td>' +
                        '</tr>'
                    );
            }
        });
        $modal.modal('hide');
    });

    item_list.on('click', '.item_name', function () {
        $("tr").removeClass("info");
        $(this).closest('tr').addClass("info");
        item_remove.show();
    });

    item_list.on('change', '.qty', function () {
        var qty = $(this).val();
        var item_code = $(this).attr('data-item-code-qty');
        var qtyInput = $('input[data-item-code-price="' + item_code + '"]');
        var oriPrice = qtyInput.attr('data-item-price');
        $('input[data-item-code-total="' + item_code + '"]').val(qty * parseFloat(oriPrice));
    });

    item_remove.on('click', function () {
        $("#bundled_items tr.info").fadeOut(300, function () {
            $(this).remove();
        });

    });
});

function numberWithCommas(price) {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}