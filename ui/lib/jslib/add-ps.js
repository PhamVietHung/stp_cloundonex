Dropzone.autoDiscover = false;
$(document).ready(function () {
    $(".progress").hide();
    $("#emsg").hide();

    var _url = $("#_url").val();

    var ib_submit = $("#submit");

    var $file_link = $("#file_link");

    var upload_resp;

    $('#description').redactor(
        {
            minHeight: 200 // pixels
        }
    );

    $('#package_type').redactor(
        {
            minHeight: 200 // pixels
        }
    );

    var ib_file = new Dropzone("#upload_container",
        {
            url: _url + "ps/upload/",
            maxFiles: 1
        }
    );


    ib_file.on("sending", function () {

        ib_submit.prop('disabled', true);

    });

    ib_file.on("success", function (file, response) {

        ib_submit.prop('disabled', false);

        upload_resp = response;

        if (upload_resp.success == 'Yes') {

            toastr.success(upload_resp.msg);
            $file_link.val(upload_resp.file);


        } else {
            toastr.error(upload_resp.msg);
        }


    });


    ib_submit.click(function (e) {
        e.preventDefault();
        $('#ibox_form').block({message: null});
        var _url = $("#_url").val();
        $.post(_url + 'ps/add-post/', $("#rform").serialize())
            .done(function (data) {

                if ($.isNumeric(data)) {

                    location.reload();
                } else {
                    $('#ibox_form').unblock();

                    $("#emsgbody").html(data);
                    $("#emsg").show("slow");
                }
            });
    });

    $("#is_bundled").change(function () {

        if (this.checked) {
            $("#button_add_bundled").css("display", "block");
            $("#cost_price").attr("readonly", "true");
            var total = $("#cost_price").attr("data-cost-price-bundled");
            $("#cost_price").val(customFormatCurrency(total));
        } else {
            $("#button_add_bundled").css("display", "none");
            $("#cost_price").removeAttr("readonly");
            $("#cost_price").val("");
        }
    });

    var $modal = $('#ajax-modal');
    var item_remove = $('#item-remove');

    $('#item-add').on('click', function () {

        // create the backdrop and wait for next modal to be triggered
        $('body').modalmanager('loading');
        $modal.load(_url + 'ps/modal-list/exBundled', '', function () {
            $modal.modal();
        });
    });

    $modal.on('click', '.update', function () {
        var tableControl = document.getElementById('items_table');
        $modal.modal('loading');
        var totalPrice = parseFloat(($("#cost_price").attr("data-cost-price-bundled")));

        $('input:checkbox:checked', tableControl).each(function () {

            var item_code = $(this).closest('tr').find('td.product:eq(1)').text();
            var item_name = $(this).closest('tr').find('td.product:eq(2)').text();

            var item_price = $(this).closest('tr').find('td.product:eq(3)').text();
            totalPrice += parseFloat(item_price);

            var qtyInput = $('input[data-item-code-qty="' + item_code + '"]');
            var priceInput = $('input[data-item-code-price="' + item_code + '"]');
            var totalInput = $('input[data-item-code-total="' + item_code + '"]');

            if (qtyInput.length > 0) {
                qtyInput.val(parseInt(qtyInput.val()) + 1);
                totalInput.val((parseFloat(totalInput.val()) + parseFloat(item_price)).toString() + '.00');
            } else {
                //  obj.push(innertext);
                $("#invoice_items").css('display', 'block');
                $("#invoice_items").find('tbody')
                    .append(
                        '<tr class="item_product"> ' +
                        '<td>' + item_code + '</td>' +
                        ' <td><textarea class="form-control" data-item-code="' + item_code + '" name="desc[]" rows="3">' + item_name + '</textarea></td>' +
                        '<input type="hidden" value="' + item_code + '" name="item_code[]">' +
                        ' <td><input type="number" data-item-code-qty="' + item_code + '" min="0" max="99" class="form-control qty" value="1" name="qty[]"></td> ' +
                        '<td><input type="text" data-qty-current="1" data-item-code-price="' + item_code + '" data-item-price="' + item_price + '" class="form-control item_price" name="amount[]" value="' + item_price + '"></td> ' +
                        '<td class="ltotal"><input id="total-item" data-item-code-total="' + item_code + '" type="text" class="form-control lvtotal" readonly name="total[]" data-a-sign="$"  data-a-dec="." data-a-sep="," value="' + item_price + '"></td> ' +
                        '<td> <select class="form-control taxed" name="taxed[]"> <option value="Yes">Yes</option> <option value="No" selected>No</option></select></td>' +
                        '</tr>'
                    );
            }
        });
        //  console.debug(obj); // Write it to the console
        //  calculateTotal();
        $("#cost_price").val(customFormatCurrency(totalPrice));
        $("#cost_price").attr("data-cost-price-bundled", totalPrice);
        $modal.modal('hide');
    });

    $('#invoice_items').on('click', '.item_product', function () {
        $("tr").removeClass("info");
        $("tr").removeAttr("data-item-total-item");
        $(this).closest('tr').addClass("info");

        var item_code = $(this).attr("data-item-code");
        var item_total = $('input[data-item-code-total="' + item_code + '"]').val();
        $(this).closest('tr').attr("data-item-total-item", item_total);

        item_remove.show();
    });

    $('#invoice_items').on('change', '.qty', function () {
        var qty = $(this).val();
        var item_code = $(this).attr('data-item-code-qty');
        var qtyInput = $('input[data-item-code-price="' + item_code + '"]');
        var oriPrice = qtyInput.attr('data-item-price').replace(',', '');
        $('input[data-item-code-total="' + item_code + '"]').val(qty * parseFloat(oriPrice));

        //update total cost price
        var qtyCurrent = qtyInput.attr("data-qty-current");
        var priceCurrent = qtyCurrent * parseFloat(oriPrice);
        var totalCurrent = $("#cost_price").attr("data-cost-price-bundled");
        var totalNew = parseFloat(totalCurrent) - parseFloat(priceCurrent) + parseFloat(qty * parseFloat(oriPrice));
        $("#cost_price").val(customFormatCurrency(totalNew));
        $("#cost_price").attr("data-cost-price-bundled", totalNew);
        qtyInput.attr("data-qty-current", qty);
    });

    item_remove.on('click', function () {
        var totalRemove = $("#invoice_items tr.info").attr("data-item-total-item");
        $("#invoice_items tr.info").fadeOut(300, function () {
            $(this).remove();
            // calculateTotal();
        });
        var total = (parseFloat($("#cost_price").attr("data-cost-price-bundled")) - parseFloat(totalRemove));

        $("#cost_price").val(customFormatCurrency(total));
        $("#cost_price").attr("data-cost-price-bundled", total);
    });

    function customFormatCurrency(price) {
        var currency = $("#cost_price").attr('data-a-sign');
        if (!isNaN(price) && parseFloat(price) === 0) {
            return currency + '0.00';
        }
        if (!isNaN(price) && price > 0) {
            return currency + price.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
        }
        return price;
    }

    var $modalManufacturer = $('#ajax-modal-manufacturer');
    var item_manufacturer_remove = $('#item_remove_manufacturer');

    $('#button_add_manufacturer').on('click', function () {

        // create the backdrop and wait for next modal to be triggered
        $('body').modalmanager('loading');
        $modalManufacturer.load(_url + 'ps/modal-list-manufacturer', '', function () {
            $modalManufacturer.modal();
        });
    });

    $modalManufacturer.on('click', '.update', function () {
        var tableControl = document.getElementById('items_table_manufacturer');
        $modalManufacturer.modal('loading');
        $('input:checkbox:checked', tableControl).each(function () {

            var name = $(this).closest('tr.manufacturer').find('td.manufacturer:eq(1)').text();
            var email = $(this).closest('tr.manufacturer').find('td.manufacturer:eq(2)').text();
            var currency = $(this).closest('tr.manufacturer').find('td.manufacturer:eq(2)').find('input[name="currency"]').val();
            var phone = $(this).closest('tr.manufacturer').find('td.manufacturer:eq(3)').text();
            var id = $(this).closest('tr.manufacturer').find('td.manufacturer:eq(3)').find('input').val();

            var manufacturerItems = $("#manufacturer_items");
            manufacturerItems.css('display', 'block');
            manufacturerItems.find('tbody')
                .append(
                    '<tr class="item_manufacturer"> ' +
                    '<input type="hidden" value="' + id + '" name="idManufacturer[]">' +
                    '<td><input type="text" readonly name="item_name_manufacturer[]" class="form-control" value="' + name + '"></td>' +
                    ' <td class="item_cost_price_manufacturer"><span></span><input class="form-control amount_manufacturer" type="number" name="cost_price_manufacturer[]"><input type="hidden" name="currency" value="' + currency + '"></td> ' +
                    '</tr>'
                );
        });

        $modalManufacturer.modal('hide');
    });
    $('#manufacturer_items').on('focus', '.amount_manufacturer', function () {
        var currencyInput = $(this).siblings('input[name="currency"]').val();
        $(this).siblings('span').html(currencyInput);
    });

    $('#manufacturer_items').on('click', '.item_manufacturer', function () {
        $("tr").removeClass("info");
        $(this).closest('tr').addClass("info");
        item_manufacturer_remove.show();
    });

    item_manufacturer_remove.on('click', function () {
        $("#manufacturer_items tr.info").fadeOut(300, function () {
            $(this).remove();
        });
    });
});
