Dropzone.autoDiscover = false;
$(document).ready(function () {
    $(".progress").hide();
    $("#emsg").hide();

    var _url = $("#_url").val();
    var $modal = $('#ajax-modal');
    var sysrender = $('#application_ajaxrender');

    sysrender.on('click', '#order-view-item', function(e){
        e.preventDefault();
        var orderId = $(this).attr("data-order-id");

        $('body').modalmanager('loading');
        var _url = $("#_url").val();
        $modal.load(_url + 'orders/view-items/' + orderId, '', function(){
            $modal.modal();
            $('.amount').autoNumeric('init', {
                vMax: '9999999999999999.00',
                vMin: '-9999999999999999.00'
            });

        });
    });

});
