$(document).ready(function () {
    $(".progress").hide();
    $("#emsg").hide();

    var _url = $("#_url").val();
    var ib_submit = $("#submit");

    $('#description').redactor(
        {
            minHeight: 200 // pixels
        }
    );

    ib_submit.click(function (e) {
        e.preventDefault();
        $('#ibox_form').block({message: null});
        var is_bundle = $("#is_bundle").val();
        var _url = $("#_url").val();
        $.post(_url + 'ps/edit-manufacturer-post/', $("#rform").serialize())
            .done(function (data) {

                if ($.isNumeric(data)) {
                    if (is_bundle) {
                        window.location = _url + 'ps/p-list/bundled';
                    } else {
                        window.location = _url + 'ps/p-list';
                    }
                } else {
                    $('#ibox_form').unblock();

                    $("#emsgbody").html(data);
                    $("#emsg").show("slow");
                }
            });
    });

    $("#is_bundled").change(function () {

        if (this.checked) {
            $("#button_add_bundled").css("display", "block");
        } else {
            $("#button_add_bundled").css("display", "none");
        }
    });

    var $modal = $('#ajax-modal');
    var item_remove = $('#item-remove');
    var item_list = $("#manufacturer_items");

    $('#item-add').on('click', function () {
        // create the backdrop and wait for next modal to be triggered
        $('body').modalmanager('loading');
        $modal.load(_url + 'ps/modal-list-manufacturer', '', function () {
            $modal.modal();
        });
    });
    $modal.on('click', '.update', function () {
        var tableControl = document.getElementById('items_table');
        $modal.modal('loading');

        $('input:checkbox:checked', tableControl).each(function () {

            var name = $(this).closest('tr.manufacturer').find('td.manufacturer:eq(1)').text();
            var email = $(this).closest('tr.manufacturer').find('td.manufacturer:eq(2)').text();
            var phone = $(this).closest('tr.manufacturer').find('td.manufacturer:eq(3)').text();
            var id = $(this).closest('tr.manufacturer').find('td.manufacturer:eq(3)').find('input').val();

            item_list.find('tbody')
                .append(
                    '<tr class="item_manufacturer"> ' +
                    '<input type="hidden" value="' + id + '" name="id_manufacturer[]">' +
                    '<input type="hidden" readonly name="item_name_manufacturer[]" class="form-control" value="' + name + '">' +
                    '<td class="item_name">' + name + '</td>' +
                    ' <td class="item_cost_price_manufacturer"><span></span><input class="form-control amount_manufacturer" type="number" name="cost_price_manufacturer[]"></td> ' +
                    '</tr>'
                );
        });
        $modal.modal('hide');
    });

    item_list.on('focus', '.amount_manufacturer', function () {
        var currencyInput = $("#currency").val();
        $(this).siblings('span').html(currencyInput);
    });

    item_list.on('click', '.item_name', function () {
        $("tr").removeClass("info");
        $(this).closest('tr').addClass("info");
        item_remove.show();
    });

    item_list.on('change', '.qty', function () {
        var qty = $(this).val();
        var item_code = $(this).attr('data-item-code-qty');
        var qtyInput = $('input[data-item-code-price="' + item_code + '"]');
        var oriPrice = qtyInput.attr('data-item-price');
        $('input[data-item-code-total="' + item_code + '"]').val(qty * parseFloat(oriPrice) + '.00');
    });

    item_remove.on('click', function () {
        $("#manufacturer_items tr.info").fadeOut(300, function () {
            $(this).remove();
        });

    });
});

function numberWithCommas(price) {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}