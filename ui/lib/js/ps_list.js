Dropzone.autoDiscover = false;

$(document).ready(function () {




    var upload_resp;



    function updateDiv(){
        //   $("#application_ajaxrender").html('Loading...');
        $('#ibox_form').block({ message: null });




        var btnsearch = $("#search");

        //btnsearch.html(_L['Searching']);
        //btnsearch.addClass("btn-danger");
        var _url = $("#_url").val();
        var typeProduct = $("#type_product").val();
        var request = 'search/ps/' + typeProduct;


        $.post(_url + request, {

            txtsearch: $('#txtsearch').val(),
            stype: $('#stype').val()

        })
            .done(function (data) {
                var sbutton = $("#search");
                var result =  $("#application_ajaxrender");
                //sbutton.html('Search');
                //sbutton.removeClass("btn-danger");

                $('#ibox_form').unblock();
                result.html(data);
                result.show();
            });

    }

    updateDiv();

    $("#search").click(function (e) {
        e.preventDefault();
        updateDiv();
    });
    var $modal = $('#ajax-modal');
    var sysrender = $('#application_ajaxrender');
    sysrender.on('click', '.cdelete', function(e){
        e.preventDefault();
        var id = this.id;
        var isBundle = $(this).attr('data-is-bundle');
        var lan_msg = $("#_lan_are_you_sure").val();
        bootbox.confirm(lan_msg, function(result) {
            if(result){
                var _url = $("#_url").val();
                window.location.href = _url + "delete/ps/" + id + '/isBundle/' + isBundle;
            }
        });
    });

    sysrender.on('click', '.cedit', function(e){
        e.preventDefault();
        var vid = this.id;
        var id = vid.replace("e", "");
        id = id.replace("t", "");

        // var id = $(this).closest('tr').attr('id');
        // create the backdrop and wait for next modal to be triggered
        $('body').modalmanager('loading');
        var _url = $("#_url").val();
        $modal.load(_url + 'ps/edit-form/' + id, '', function(){
            $modal.modal();
            $('.amount').autoNumeric('init', {
                vMax: '9999999999999999.00',
                vMin: '-9999999999999999.00'
            });

            $('#description').redactor(
                {
                    minHeight: 200 // pixels
                }
            );

            $('#package_type').redactor(
                {
                    minHeight: 200 // pixels
                }
            );

            new Clipboard('.ib_btn_copy');

            var $file_link = $("#file_link");
            var ib_submit = $("#update");

            var ib_file = new Dropzone("#upload_container",
                {
                    url: _url + "ps/upload/",
                    maxFiles: 1
                }
            );


            ib_file.on("sending", function() {

                ib_submit.prop('disabled', true);

            });

            ib_file.on("success", function(file,response) {

                ib_submit.prop('disabled', false);

                upload_resp = response;

                if(upload_resp.success == 'Yes'){

                    toastr.success(upload_resp.msg);
                    $file_link.val(upload_resp.file);


                }
                else{
                    toastr.error(upload_resp.msg);
                }


            });

        });
    });

    sysrender.on('click', '.history', function(e){
        e.preventDefault();
        var vid = this.id;
        var id = vid.replace("e", "");
        id = id.replace("t", "");

        // var id = $(this).closest('tr').attr('id');
        // create the backdrop and wait for next modal to be triggered
        $('body').modalmanager('loading');
        var _url = $("#_url").val();
        $modal.load(_url + 'ps/history-form/' + id, '', function(){
            $modal.modal();
            $('.amount').autoNumeric('init', {
                vMax: '9999999999999999.00',
                vMin: '-9999999999999999.00'
            });

        });
    });

    sysrender.on('click', '.update-inventory', function(e){
        e.preventDefault();
        var vid = this.id;
        var id = vid.replace("e", "");
        id = id.replace("t", "");

        // var id = $(this).closest('tr').attr('id');
        // create the backdrop and wait for next modal to be triggered
        $('body').modalmanager('loading');
        var _url = $("#_url").val();
        $modal.load(_url + 'ps/update-inventory-form/' + id, '', function(){
            $modal.modal();
            $('.amount').autoNumeric('init', {
                vMax: '9999999999999999.00',
                vMin: '-9999999999999999.00'
            });

        });
    });

    $modal.on('click', '#update', function(){
        $modal.modal('loading');

        var _url = $("#_url").val();
        $.post(_url + 'ps/edit-post/', $('#edit_form').serialize(), function(data){

            var _url = $("#_url").val();
            if ($.isNumeric(data)) {

                location.reload();
            }
            else {

                $modal
                    .modal('loading')
                    .find('.modal-body')
                    .prepend('<div class="alert alert-danger fade in">' + data +

                        '</div>');

            }

        });

    });

    $modal.on('click', '.btn-update-inventory', function(){

        var currentInventory = $("#current_inventory").val().replace(',','');
        var valChange = $("#update-inventory").val();

        var type = $(this).attr('id');
        if(type === 'btn-increase'){
            $valUpdate = parseInt(currentInventory) + parseInt(valChange);
        }else {
            $valUpdate = parseInt(currentInventory) - parseInt(valChange);
        }
        if(parseInt($valUpdate) <= 0){
            $valUpdate = 0;
        }
        $("#new_inventory").val($valUpdate);

    });

    $modal.on('click', '#save-inventory', function(){

        $modal.modal('loading');
        var type = $(this).attr('id');
        var _url = $("#_url").val();
        $.post(_url + 'ps/edit-post-inventory/' + type, $('#edit_form_inventory').serialize(), function(data){

            var _url = $("#_url").val();
            if ($.isNumeric(data)) {

                location.reload();
            }
            else {

                $modal
                    .modal('loading')
                    .find('.modal-body')
                    .prepend('<div class="alert alert-danger fade in">' + data +

                        '</div>');

            }

        });

    });





});