<?php
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

// $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
$sql1 = 'CREATE TABLE sys_orders_marketplace_settings (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
date_added DATETIME NULL DEFAULT NULL,
url VARCHAR(200) NOT NULL,
api_key VARCHAR(200) NOT NULL,
api_secret VARCHAR(200) NOT NULL,
token VARCHAR(200) NOT NULL,
options VARCHAR(200) NOT NULL,
title VARCHAR(200) NOT NULL,
country VARCHAR(200) NULL DEFAULT NULL,
orders_created_after DATETIME NULL DEFAULT NULL
)';

$mysqli->query($sql1);

$sql2 = 'CREATE TABLE webhook_logs (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
date DATETIME NULL DEFAULT NULL,
logs VARCHAR(200) NULL DEFAULT NULL
)';

$mysqli->query($sql2);


$sql3 = 'CREATE TABLE sys_orders_marketplace (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
id_order_marketplace VARCHAR(200) NULL DEFAULT NULL,
email VARCHAR(200) NULL DEFAULT NULL,
number INT(6) NULL DEFAULT NULL,
note VARCHAR(200) NULL DEFAULT NULL,
created_at DATETIME NULL DEFAULT NULL,
updated_at DATETIME NULL DEFAULT NULL,
total_price decimal(18,2) NULL DEFAULT NULL,
subtotal_price decimal(18,2) NULL DEFAULT NULL,
total_weight INT(6) NULL DEFAULT NULL,
total_tax VARCHAR(200) NULL DEFAULT NULL,
currency VARCHAR(200) NULL DEFAULT NULL,
financial_status VARCHAR(200) NULL DEFAULT NULL,
confirmed boolean null default 0,
total_discounts decimal(18,2) NULL DEFAULT NULL,
order_number VARCHAR(200) NULL DEFAULT NULL,
fulfillment_status VARCHAR(200) NULL DEFAULT NULL,
line_items LONGTEXT NULL DEFAULT NULL,
billing_address VARCHAR(5000) NULL DEFAULT NULL,
shipping_address VARCHAR(5000) NULL DEFAULT NULL,
customer VARCHAR(5000) NULL DEFAULT NULL,
shipping_lines VARCHAR(5000) NULL DEFAULT NULL,
marketplace VARCHAR(200) NULL DEFAULT NULL,
marketplace_settings_id INT(6) UNSIGNED NOT NULL,
FOREIGN KEY (marketplace_settings_id) REFERENCES sys_orders_marketplace_settings(id) on delete cascade
)';

$mysqli->query($sql3);

$sql4 = 'CREATE TABLE `webhook_lazada_token` (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
marketplace_settings_id INT(6) UNSIGNED NOT NULL,
access_token varchar(200) NOT NULL,
refresh_token varchar(200) NOT NULL,
expires_in DATETIME NOT NULL,
refresh_expires_in DATETIME NOT NULL,
FOREIGN KEY (marketplace_settings_id) REFERENCES sys_orders_marketplace_settings(id) on delete cascade
)';

$mysqli->query($sql4);

$sql6 = 'CREATE TABLE `sys_orders_marketplace_lazada` (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
id_order_marketplace VARCHAR(200) NULL DEFAULT NULL,
order_number VARCHAR(200) NULL DEFAULT NULL,
voucher varchar(200) NULL DEFAULT NULL,
voucher_platform varchar(200) NULL DEFAULT NULL,
voucher_seller varchar(200) NULL DEFAULT NULL,
voucher_code varchar(200) NULL DEFAULT NULL,
gift_option bool NULL DEFAULT NULL,
customer_last_name varchar(200) NULL DEFAULT NULL,
promised_shipping_times varchar(200) NULL DEFAULT NULL,
delivery_info varchar(200) NULL DEFAULT NULL,
gift_message varchar(200) NULL DEFAULT NULL,
shipping_fee int(10) UNSIGNED NULL DEFAULT NULL,
payment_method varchar(200) NULL DEFAULT NULL,
customer_first_name varchar(200) NULL DEFAULT NULL,
items_count int(10) UNSIGNED NULL DEFAULt NULL,
statuses varchar(200) NULL DEFAULT NULL,
address_billing VARCHAR(5000) NULL DEFAULT NULL,
address_shipping VARCHAR(5000) NULL DEFAULT NULL,
shipping_lines VARCHAR(5000) NULL DEFAULT NULL,
created_at DATETIME NULL DEFAULT NULL,
updated_at DATETIME NULL DEFAULT NULL,
price decimal(18,2) NULL DEFAULT NULL,
national_registration_number varchar(200) NULL DEFAULT NULL,
remarks varchar(200) NULL DEFAULT NULL,
extra_attributes varchar(200) NULL DEFAULT NULL,
items text varchar(200) NULL DEFAULT NULL,
fulfillment_status VARCHAR(200) NULL DEFAULT NULL,
marketplace VARCHAR(200) NULL DEFAULT NULL,
marketplace_settings_id INT(6) UNSIGNED NOT NULL,
FOREIGN KEY (marketplace_settings_id) REFERENCES sys_orders_marketplace_settings(id) on delete cascade
)';

$mysqli->query($sql6);

$sql7 = 'ALTER TABLE sys_orders_marketplace_lazada MODIFY shipping_fee VARCHAR(10)';

$mysqli->query($sql7);
