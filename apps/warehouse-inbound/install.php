<?php
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

// $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
$sql1 = 'CREATE TABLE sys_warehouse_inbound (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
id_purchase INT(6) NOT NULL,
id_purchase_item INT(6) NOT NULL,
internal_memo VARCHAR(200) NOT NULL,
qty_received INT(6) NOT NULL,
status VARCHAR(200) NOT NULL,
date_time DATETIME NULL DEFAULT NULL,
username VARCHAR(200) NOT NULL,
FOREIGN KEY (id_purchase) REFERENCES sys_purchases(id) on delete cascade,
FOREIGN KEY (id_purchase_item) REFERENCES sys_purchaseitems(id) on delete cascade
)';

    $mysqli->query($sql1);
