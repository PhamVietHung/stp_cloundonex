<?php
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

// $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
$sql1 = 'INSERT INTO
sys_permissions(pname, shortname, available,core)
VALUES("Cost Price","cost_price",0,1)';

    $mysqli->query($sql1);

$sql2 = 'CREATE TABLE sys_manufacturer_product (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
id_item INT(6) NOT NULL,
id_manufacturer INT(6) NOT NULL,
name VARCHAR(200) NOT NULL,
cost_price decimal(18,2)	NOT NULL,
FOREIGN KEY (id_item) REFERENCES sys_items(id) on delete cascade
)';

$mysqli->query($sql2);