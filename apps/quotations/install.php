<?php
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

// $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
$sql1 = 'ALTER TABLE sys_documents
ADD COLUMN `supplier_id` int NULL,
ADD COLUMN `supplier_name` VARCHAR(200) NULL,
ADD COLUMN `document_type` VARCHAR(200) NULL,
ADD COLUMN `status` VARCHAR(200) NULL,
ADD COLUMN `quote_amount` float NULL';

$sql2 = 'ALTER TABLE sys_quotes ADD COLUMN `type` varchar(200) NOT null DEFAULT "quotes"';
$sql3 = 'ALTER TABLE sys_documents ADD COLUMN `quote_received_date` DATETIME NULL DEFAULT NULL ';

    $mysqli->query($sql1);
    $mysqli->query($sql2);
    $mysqli->query($sql3);

$sql4 = 'ALTER TABLE sys_orders
ADD COLUMN `invoice_convert_id` int NULL DEFAULT NULL';
$mysqli->query($sql4);

$sql5 = 'ALTER TABLE sys_quoteitems
ADD COLUMN `sku` VARCHAR(200) NULL DEFAULT NULL';
$mysqli->query($sql5);

$sql6 = 'ALTER TABLE sys_invoiceitems
ADD COLUMN `sku` VARCHAR(200) NULL DEFAULT NULL';
$mysqli->query($sql6);

$sql7 = 'ALTER TABLE order_items
ADD COLUMN `sku` VARCHAR(200) NULL DEFAULT NULL';
$mysqli->query($sql7);

$sql8 = 'ALTER TABLE sys_purchaseitems
ADD COLUMN `sku` VARCHAR(200) NULL DEFAULT NULL';
$mysqli->query($sql8);

$sql9 = 'ALTER TABLE sys_documents
ADD COLUMN `po` INT NULL DEFAULT NULL';
$mysqli->query($sql9);

$sql10 = 'ALTER TABLE sys_documents
ADD COLUMN `invoice` INT NULL DEFAULT NULL';
$mysqli->query($sql10);

$sql11 = 'ALTER TABLE sys_quotes
ADD COLUMN `currency_symbol` VARCHAR(200) NULL DEFAULT NULL,
ADD COLUMN `currency_rate` decimal(11,4) NULL DEFAULT NULL,
ADD COLUMN `currency_iso_code` VARCHAR(200) NULL DEFAULT NULL';
$mysqli->query($sql11);

$sql12 = 'ALTER TABLE sys_orders
ADD COLUMN `currency` INT NULL DEFAULT NULL,
ADD COLUMN `currency_symbol` VARCHAR(200) NULL DEFAULT NULL,
ADD COLUMN `currency_rate` decimal(11,4) NULL DEFAULT NULL,
ADD COLUMN `currency_iso_code` VARCHAR(200) NULL DEFAULT NULL';
$mysqli->query($sql12);

$sql13 = 'ALTER TABLE sys_items
ADD COLUMN `currency` INT NULL DEFAULT NULL,
ADD COLUMN `currency_symbol` VARCHAR(200) NULL DEFAULT NULL,
ADD COLUMN `currency_rate` decimal(11,4) NULL DEFAULT NULL,
ADD COLUMN `currency_iso_code` VARCHAR(200) NULL DEFAULT NULL';
$mysqli->query($sql13);


