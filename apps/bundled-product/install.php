<?php
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

// $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
$sql1 = 'CREATE TABLE sys_bundle_product (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
id_bundle INT(6) NOT NULL,
item_code INT(6) NOT NULL,
description VARCHAR(200) NOT NULL,
qty INT(6) NOT NULL,
price decimal(18,2)	NOT NULL,
total decimal(18,2)	NOT NULL,
taxable INT(1)	NOT NULL,
FOREIGN KEY (id_bundle) REFERENCES sys_items(id) on delete cascade
)';

    $mysqli->query($sql1);

$sql2 = 'ALTER TABLE sys_items
ADD COLUMN `is_bundle` int NULL';

$mysqli->query($sql2);

$sql3 = 'ALTER TABLE sys_items
ADD COLUMN `reorder_level` int NOT null DEFAULT 0';

$mysqli->query($sql3);

$sql4 = 'ALTER TABLE sys_items
ADD COLUMN `package_type` VARCHAR(255) NOT NULL';
$mysqli->query($sql4);

$sql5 = 'ALTER TABLE order_items
ADD COLUMN `weight` decimal(16,4) NULL DEFAULT NULL,
ADD COLUMN `width` decimal(16,4) NULL DEFAULT NULL,
ADD COLUMN `length` decimal(16,4) NULL DEFAULT NULL,
ADD COLUMN `height` decimal(16,4) NULL DEFAULT NULL,
ADD COLUMN `package_type` VARCHAR(255) NULL DEFAULT NULL';

$mysqli->query($sql5);

$sql6 = 'ALTER TABLE sys_bundle_product MODIFY item_code VARCHAR(100)';

$mysqli->query($sql6);