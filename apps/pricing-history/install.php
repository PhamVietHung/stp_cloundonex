<?php
$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

// $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
$sql1 = 'CREATE TABLE sys_pricing_history (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
id_item INT(6) NULL DEFAULT NULL,
user_made_id INT NULL DEFAULT NULL,
user_made_name VARCHAR(255) NULL DEFAULT NULL,
cost_price_old decimal(18,2) NULL DEFAULT NULL,
cost_price_current decimal(18,2) NULL DEFAULT NULL,
selling_rice_old decimal(18,2) NULL DEFAULT NULL,
selling_rice_current decimal(18,2) NULL DEFAULT NULL,
inventory_old INT NULL DEFAULT NULL,
inventory_current INT NULL DEFAULT NULL,
date_change DATETIME NULL DEFAULT NULL,
FOREIGN KEY (id_item) REFERENCES sys_items(id) on delete cascade
)';

$mysqli->query($sql1);

$sql2 = 'ALTER TABLE sys_pricing_history
ADD COLUMN `ordernum` VARCHAR(255) NULL DEFAULT NULL';
$mysqli->query($sql2);