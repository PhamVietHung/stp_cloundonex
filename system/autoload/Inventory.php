<?php

Class Inventory
{
    /**
     * @param $sku
     * @param $qty
     * @param array $user
     * @param null $orderNum
     * @return bool
     */
    public static function decreaseItemMarketPlaceBySku($sku, $qty, $user = [], $orderNum = null)
    {
        $itemSys = ORM::for_table('sys_items')->where('item_number', $sku)->find_one();
        if (!empty($itemSys) && !empty($itemSys->id)) {

            if ($itemSys->is_bundle == 1) {
                $itemBundleds = ORM::for_table('sys_bundle_product')->where('id_bundle', $itemSys->id)->find_many();
                foreach ($itemBundleds as $bundled) {
                    //get by item_number
                    $p = ORM::for_table('sys_items')->where('item_number', $bundled->item_code)->find_one();
                    if (empty($p)) {
                        $p = ORM::for_table('sys_items')->where('name', $bundled->description)->find_one();
                    }
                    if (!empty($p) && !empty($p->id)) {
                        $current_qty_p = $p->inventory;
                        $updated_qty_p = $current_qty_p - $qty * (int)$bundled->qty;
                        $p->inventory = $updated_qty_p;
                        $p->save();

                        //save history
                        $history = ORM::for_table('sys_pricing_history')->create();
                        $history->id_item = $p->id;
                        $history->user_made_id = isset($user['id']) ? $user['id'] : '';
                        $history->user_made_name = isset($user['username']) ? $user['username'] : '';
                        $history->cost_price_old = $p->cost_price;
                        $history->cost_price_current = $p->cost_price;
                        $history->selling_rice_old = $p->sales_price;
                        $history->selling_rice_current = $p->sales_price;
                        $history->inventory_old = $current_qty_p;
                        $history->inventory_current = $updated_qty_p;
                        $history->date_change = date('Y-m-d H:i:s');
                        $history->ordernum = $orderNum;
                        $history->save();
                    }
                }
            } else {
                //save history
                $history = ORM::for_table('sys_pricing_history')->create();
                $history->id_item = $itemSys->id;
                $history->user_made_id = isset($user['id']) ? $user['id'] : '';
                $history->user_made_name = isset($user['username']) ? $user['username'] : '';
                $history->cost_price_old = $itemSys->cost_price;
                $history->cost_price_current = $itemSys->cost_price;
                $history->selling_rice_old = $itemSys->sales_price;
                $history->selling_rice_current = $itemSys->sales_price;
                $history->inventory_old = $itemSys->inventory;
                $history->inventory_current = (int)$itemSys->inventory - $qty;
                $history->date_change = date('Y-m-d H:i:s');
                $history->ordernum = $orderNum;
                $history->save();
                //end save

                $itemSys->inventory = (int)$itemSys->inventory - $qty;
                $itemSys->save();
            }
        }
        return true;
    }

    /**
     * @param $item_number
     * @param $qty
     * @param array $user
     * @param null $orderNum
     * @return bool
     */
    public static function decreaseByItemNumber($item_number, $qty, $user = [], $orderNum = null)
    {
        if ($item_number == '') {
            return false;
        }
        $item = ORM::for_table('sys_items')->where('item_number', $item_number)->find_one();

        if ($item) {
            if ($item->is_bundle == 1) {
                $itemBundleds = ORM::for_table('sys_bundle_product')->where('id_bundle', $item->id)->find_many();
                foreach ($itemBundleds as $bundled) {
                    //get by id
                    $p = ORM::for_table('sys_items')->where('item_number', $bundled->item_code)->find_one();
                    if (empty($p)) {
                        $p = ORM::for_table('sys_items')->where('name', $bundled->description)->find_one();
                    }
                    if (!empty($p) && !empty($p->id)) {
                        $current_qty_p = $p->inventory;
                        $updated_qty_p = $current_qty_p - $qty * (int)$bundled->qty;
                        $p->inventory = $updated_qty_p;
                        $p->save();

                        //save history
                        $history = ORM::for_table('sys_pricing_history')->create();
                        $history->id_item = $p->id;
                        $history->user_made_id = isset($user['id']) ? $user['id'] : '';
                        $history->user_made_name = isset($user['username']) ? $user['username'] : '';
                        $history->cost_price_old = $p->cost_price;
                        $history->cost_price_current = $p->cost_price;
                        $history->selling_rice_old = $p->sales_price;
                        $history->selling_rice_current = $p->sales_price;
                        $history->inventory_old = $current_qty_p;
                        $history->inventory_current = $updated_qty_p;
                        $history->ordernum = $orderNum;
                        $history->date_change = date('Y-m-d H:i:s');
                        $history->save();
                    }
                }
            } else {
                $current_qty = $item->inventory;
                $updated_qty = $current_qty - $qty;
                $item->inventory = $updated_qty;
                $item->save();

                //save history
                $history = ORM::for_table('sys_pricing_history')->create();
                $history->id_item = $item->id;
                $history->user_made_id = isset($user['id']) ? $user['id'] : '';
                $history->user_made_name = isset($user['username']) ? $user['username'] : '';
                $history->cost_price_old = $item->cost_price;
                $history->cost_price_current = $item->cost_price;
                $history->selling_rice_old = $item->sales_price;
                $history->selling_rice_current = $item->sales_price;
                $history->inventory_old = $current_qty;
                $history->inventory_current = $updated_qty;
                $history->ordernum = $orderNum;
                $history->date_change = date('Y-m-d H:i:s');
                $history->save();
            }
            return true;

        }

        return false;

    }

    /**
     * @param $item_number
     * @param $qty
     * @param array $user
     * @param null $orderNum
     * @return bool
     */
    public static function increaseByItemNumber($item_number, $qty, $user = [], $orderNum = null)
    {

        if ($item_number == '') {
            return false;
        }

        $item = ORM::for_table('sys_items')->where('item_number', $item_number)->find_one();

        if ($item) {
            if ($item->is_bundle == 1) {
                $itemBundleds = ORM::for_table('sys_bundle_product')->where('id_bundle', $item->id)->find_many();
                foreach ($itemBundleds as $bundled) {
                    $p = ORM::for_table('sys_items')->find_one($bundled->item_code);
                    if (empty($p)) {
                        $p = ORM::for_table('sys_items')->where('name', $bundled->description)->find_one();
                    }
                    if (!empty($p) && !empty($p->id)) {
                        $current_qty_p = $p->inventory;
                        $updated_qty_p = $current_qty_p + $qty * (int)$bundled->qty;
                        $p->inventory = $updated_qty_p;
                        $p->save();

                        //save history
                        $history = ORM::for_table('sys_pricing_history')->create();
                        $history->id_item = $p->id;
                        $history->user_made_id = isset($user['id']) ? $user['id'] : '';
                        $history->user_made_name = isset($user['username']) ? $user['username'] : '';
                        $history->cost_price_old = $p->cost_price;
                        $history->cost_price_current = $p->cost_price;
                        $history->selling_rice_old = $p->sales_price;
                        $history->selling_rice_current = $p->sales_price;
                        $history->inventory_old = $current_qty_p;
                        $history->inventory_current = $updated_qty_p;
                        $history->ordernum = $orderNum;
                        $history->date_change = date('Y-m-d H:i:s');
                        $history->save();
                    }
                }
            }
            $current_qty = $item->inventory;
            $updated_qty = $current_qty + $qty;
            $item->inventory = $updated_qty;
            $item->save();

            //save history
            $history = ORM::for_table('sys_pricing_history')->create();
            $history->id_item = $item->id;
            $history->user_made_id = isset($user['id']) ? $user['id'] : '';
            $history->user_made_name = isset($user['username']) ? $user['username'] : '';
            $history->cost_price_old = $item->cost_price;
            $history->cost_price_current = $item->cost_price;
            $history->selling_rice_old = $item->sales_price;
            $history->selling_rice_current = $item->sales_price;
            $history->inventory_old = $current_qty;
            $history->inventory_current = $updated_qty;
            $history->ordernum = $orderNum;
            $history->date_change = date('Y-m-d H:i:s');
            $history->save();

            return true;

        }

        return false;

    }

}