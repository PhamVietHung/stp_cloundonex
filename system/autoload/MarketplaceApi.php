<?php
include 'php/lazop/LazopClient.php';
include 'php/lazop/LazopRequest.php';

Class MarketplaceApi
{
    /**
     * @param $status
     * @return array
     * @throws Exception
     */
    public static function autoSyncOrderLaz($status)
    {
        $result = ['status' => true];
        $ms = ORM::for_table('sys_orders_marketplace_settings')->where('options', 'Lazada')->find_many();
        foreach ($ms as $m) {
            if ($m) {
                if ($m->options == 'Lazada') {
                    $r = ORM::for_table('webhook_lazada_token')
                        ->where('marketplace_settings_id', $m->id)
                        ->find_one();

                    if (empty($r)) {
                        $result['status'] = false;
                        $result['msg'] = 'Please get access token first.';
                        return $result;
                    }

                    $accessToken = $r->access_token;
                }
                $username = $m->api_key;
                $password = $m->api_secret;
                $createdAfter = $m->orders_created_after;

                $url = MarketplaceApi::getUrlByCountry($m->country);

                $createdAfter = date('c', strtotime($createdAfter));
                $listOrders = MarketplaceApi::getDataOrderLazada($url, $username, $password, $accessToken, $createdAfter, $status);

                if (empty($listOrders) || isset($listOrders['errors'])) {
                    $result['status'] = false;
                    $result['msg'] =$listOrders['errors'];
                    return $result;
                }

                if ($m->options == 'Lazada') {
                    if (isset($listOrders['data']['orders']) && $listOrders['data']['count'] > 0) {
                        foreach ($listOrders['data']['orders'] as $order) {
                            try {
                                $o = ORM::for_table('sys_orders_marketplace_lazada')
                                    ->where('id_order_marketplace', $order['order_id'])
                                    ->find_one();
                                if (empty($o)) {
                                    $o = ORM::for_table('sys_orders_marketplace_lazada')->create();
                                }
                                $o->id_order_marketplace = $order['order_id'];
                                $o->order_number = $order['order_number'];
                                $o->voucher = $order['voucher'];
                                $o->voucher_platform = $order['voucher_platform'];
                                $o->voucher_seller = $order['voucher_seller'];
                                $o->gift_option = $order['gift_option'];
                                $o->customer_last_name = $order['customer_last_name'];
                                $o->promised_shipping_times = $order['promised_shipping_times'];
                                $o->price = (float)$order['price'];
                                $o->national_registration_number = $order['national_registration_number'];
                                $o->payment_method = $order['payment_method'];
                                $o->customer_first_name = $order['customer_first_name'];
                                $o->shipping_fee = $order['shipping_fee'];
                                $o->items_count = $order['items_count'];
                                $o->delivery_info = $order['delivery_info'];
                                $o->statuses = isset($order['statuses'][0]) ? $order['statuses'][0] : '';
                                $o->address_billing = isset($order['address_billing']) ? json_encode($order['address_billing']) : '';
                                $o->address_shipping = isset($order['address_shipping']) ? json_encode($order['address_shipping']) : '';
                                $o->extra_attributes = $order['extra_attributes'];
                                $o->gift_message = $order['gift_message'];
                                $o->remarks = $order['remarks'];
                                $datetime_created_at = new DateTime($order['created_at']);
                                $o->created_at = $datetime_created_at->format('Y-m-d H:i:s');
                                $datetime_updated_at = new DateTime($order['updated_at']);
                                $o->updated_at = $datetime_updated_at->format('Y-m-d H:i:s');
                                $o->marketplace = 'Lazada';
                                $o->marketplace_settings_id = $m->id;

                                $listItems = MarketplaceApi::getItemDataLazada($url, $username, $password, $accessToken, $order['order_id']);

                                if (isset($listItems['data']) && count($listItems['data']) > 0) {
                                    $items = json_encode($listItems['data']);
                                    $o->items = $items;
                                }
                                $o->save();
                            } catch (\Exception $e) {
                                continue;
                            }
                        }
                    }
                }
            }
        }
        return $result;
    }

    /**
     * @param $type
     * @param $url
     * @param $username
     * @param $password
     * @return array|mixed
     */
    public static function getDataShopify($url, $username, $password)
    {

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
        if (empty(json_decode($response, true))) {
            return ['errors' => 'Errors.'];
        }
        return json_decode($response, true);

    }

    /**
     * @param $url
     * @param $username
     * @param $password
     * @param $accessToken
     * @param $createdAfter
     * @param string $status
     * @return array|mixed
     * @throws Exception
     */
    public static function getDataOrderLazada($url, $username, $password, $accessToken, $createdAfter, $status = 'pending')
    {
        $c = new LazopClient($url, $username, $password);
        $request = new LazopRequest('/orders/get', 'GET');
        $request->addApiParam('created_after', $createdAfter);
        // comment next line if want to test all status, pending don't have any orders
        $request->addApiParam('status', $status);
//        $request->addApiParam('update_before','2019-02-10T16:00:00+08:00');
//        $request->addApiParam('update_after','2017-02-10T09:00:00+08:00');
        $response = $c->execute($request, $accessToken);

        if (empty(json_decode($response, true))) {
            return ['errors' => 'Errors.'];
        }

        return json_decode($response, true);
    }

    /**
     * @param $url
     * @param $username
     * @param $password
     * @param $accessToken
     * @param $orderId
     * @return array|mixed
     * @throws Exception
     */
    public static function getOrderLazada($url, $username, $password, $accessToken, $orderId)
    {
        $c = new LazopClient($url, $username, $password);
        $request = new LazopRequest('/order/get', 'GET');
        $request->addApiParam('order_id', $orderId);
        $response = $c->execute($request, $accessToken);

        if (empty(json_decode($response, true))) {
            return ['errors' => 'Errors.'];
        }

        return json_decode($response, true);
    }

    /**
     * @param $url
     * @param $username
     * @param $password
     * @param $accessToken
     * @return array|mixed
     * @throws Exception
     */
    public static function getShipmentProvidersLazada($url, $username, $password, $accessToken)
    {
        $c = new LazopClient($url, $username, $password);
        $request = new LazopRequest('/shipment/providers/get', 'GET');
        $response = $c->execute($request, $accessToken);

        if (empty(json_decode($response, true))) {
            return ['errors' => 'Errors.'];
        }
        $result = json_decode($response, true);
        if ($result['code'] != '0') {
            return ['errors' => isset($result['message']) ? $result['message'] : 'Errors.'];
        }
        return $result;
    }

    /**
     * @param $country
     * @return string
     */
    public static function getUrlByCountry($country)
    {
        $url = '';
        switch ($country) {
            case 'ph':
                $url = 'https://api.lazada.com.ph/rest';
                break;
            case 'th':
                $url = 'https://api.lazada.co.th/rest';
                break;
            case 'id':
                $url = 'https://api.lazada.co.id/rest';
                break;
            case 'sg':
                $url = 'https://api.lazada.sg/rest';
                break;
            case 'vn':
                $url = 'https://api.lazada.vn/rest';
                break;
            case 'my':
                $url = 'https://api.lazada.com.my/rest';
                break;
        }
        return $url;
    }

    /**
     * @param $url
     * @param $username
     * @param $password
     * @param $accessToken
     * @param $orderId
     * @return array|mixed
     * @throws Exception
     */
    public static function getItemDataLazada($url, $username, $password, $accessToken, $orderId)
    {
        $c = new LazopClient($url, $username, $password);
        $request = new LazopRequest('/order/items/get', 'GET');
        $request->addApiParam('order_id', $orderId);
        $response = $c->execute($request, $accessToken);

        if (empty(json_decode($response, true))) {
            return ['errors' => 'Errors.'];
        }

        return json_decode($response, true);
    }

    /**
     * @param $url
     * @param $username
     * @param $password
     * @param $accessToken
     * @param $listItemIds
     * @param $shipmentProvider
     * @return array|mixed
     * @throws Exception
     */
    public static function setStatusToPackedByMarketplace($url, $username, $password, $accessToken, $listItemIds, $shipmentProvider)
    {
        $c = new LazopClient($url, $username, $password);
        $request = new LazopRequest('/order/pack');
        $request->addApiParam('delivery_type', 'dropship');
        $request->addApiParam('order_item_ids', $listItemIds);
        $request->addApiParam('shipping_provider', $shipmentProvider);
        $response = $c->execute($request, $accessToken);

        if (empty(json_decode($response, true))) {
            return ['errors' => 'Errors.'];
        }
        $result = json_decode($response, true);
        if ($result['code'] != '0') {
            return ['errors' => isset($result['message']) ? $result['message'] : 'Errors.'];
        }
        return $result;
    }

    /**
     * @param $url
     * @param $username
     * @param $password
     * @param $accessToken
     * @param $listItemIds
     * @return array|mixed
     * @throws Exception
     */
    public static function getDocumentLazada($url, $username, $password, $accessToken, $listItemIds, $type)
    {
        $c = new LazopClient($url, $username, $password);
        $request = new LazopRequest('/order/document/get', 'GET');
        $request->addApiParam('doc_type', $type);
        $request->addApiParam('order_item_ids', $listItemIds);
        $response = $c->execute($request, $accessToken);

        if (empty(json_decode($response, true))) {
            return ['errors' => 'Errors.'];
        }
        $result = json_decode($response, true);
        if ($result['code'] != '0') {
            return ['errors' => isset($result['message']) ? $result['message'] : 'Errors.'];
        }
        return $result;
    }

    /**
     * @param $url
     * @param $username
     * @param $password
     * @param $accessToken
     * @throws Exception
     */
    public static function dataMoatLogin($url, $username, $password, $accessToken)
    {
        $url = 'https://api.lazada.com/rest';
        $c = new LazopClient($url, $username, $password);
        $request = new LazopRequest('/datamoat/compute_risk');
        $request->addApiParam('time', '1564560322699');
        $request->addApiParam('appName', 'SnsDev');
        $request->addApiParam('userId', '2OShop');
        $request->addApiParam('tid', 'shobapond@hotmail.com');
        $request->addApiParam('userIp', '212.68.135.22');
        $request->addApiParam('ati', '202cb962ac59075b964b07152d234b70');
        $request->addApiParam('loginResult', 'success');
        $request->addApiParam('loginMessage', 'password success');
        echo '<pre>';
        var_dump($c->execute($request));
        die;
    }

    /**
     * @param $url
     * @param $username
     * @param $password
     * @param $accessToken
     * @param $arrOrderItemIds
     * @param $trackingNumber
     * @return array|mixed
     * @throws Exception
     */
    public static function setStatusToReadyToShipByMarketplace($url, $username, $password, $accessToken, $listItemIds, $shipmentProvider, $trackingNumber)
    {
        $c = new LazopClient($url, $username, $password);
        $request = new LazopRequest('/order/rts');
        $request->addApiParam('delivery_type', 'dropship');
        $request->addApiParam('order_item_ids', $listItemIds);
        $request->addApiParam('shipment_provider', $shipmentProvider);
        $request->addApiParam('tracking_number', $trackingNumber);
        $response = $c->execute($request, $accessToken);

        if (empty(json_decode($response, true))) {
            return ['errors' => 'Errors.'];
        }
        $result = json_decode($response, true);
        if ($result['code'] != '0') {
            return ['errors' => isset($result['message']) ? $result['message'] : 'Errors.'];
        }
        return $result;
    }

    /**
     * @param $url
     * @param $username
     * @param $password
     * @param $accessToken
     * @param $orderItemId
     * @param $invoiceNumber
     * @return array|mixed
     * @throws Exception
     */
    public static function setInvoiceNumberByMarketplace($url, $username, $password, $accessToken, $orderItemId, $invoiceNumber)
    {
        $c = new LazopClient($url, $username, $password);
        $request = new LazopRequest('/order/invoice_number/set');
        $request->addApiParam('order_item_id', $orderItemId);
        $request->addApiParam('invoice_number', $invoiceNumber);
        $response = $c->execute($request, $accessToken);

        if (empty(json_decode($response, true))) {
            return ['errors' => 'Errors.'];
        }
        $result = json_decode($response, true);
        if ($result['code'] != '0') {
            return ['errors' => isset($result['message']) ? $result['message'] : 'Errors.'];
        }
        return $result;
    }

    /**
     * @param $type
     * @param $url
     * @param $username
     * @param $password
     * @param $data
     * @return array|mixed
     */
    public static function createFulfilment($url, $username, $password, $data)
    {

        $headers = ["Content-Type:application/json;charset=UTF-8"];
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

        $response = curl_exec($ch);
        if (empty(json_decode($response, true))) {
            return ['errors' => 'Errors.'];
        }
        return json_decode($response, true);

    }

}