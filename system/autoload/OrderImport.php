<?php

Class OrderImport
{
    /**
     * @param $type
     * @param $url
     * @param $username
     * @param $password
     * @return array|mixed
     */
    public static function add($data)
    {

        $o = ORM::for_table('sys_orders_import')->create();
        $o->name = $data['order']['name'];
        $o->email = $data['order']['email'];
        $o->financial_status = $data['order']['financial_status'];
        $o->paid_at = $data['order']['paid_at'];
        $o->fulfillment_status = $data['order']['fulfillment_status'];
        $o->fulfilled_at = $data['order']['fulfilled_at'];
        $o->accepts_marketing = $data['order']['accepts_marketing'];
        $o->currency = $data['order']['currency'];
        $o->subtotal = $data['order']['subtotal'];
        $o->shipping = $data['order']['shipping'];
        $o->taxes = $data['order']['taxes'];
        $o->total = $data['order']['total'];
        $o->discount_code = $data['order']['discount_code'];
        $o->discount_amount = $data['order']['discount_amount'];
        $o->shipping_method = $data['order']['shipping_method'];
        $o->created_at = $data['order']['created_at'];
        $o->notes = $data['order']['notes'];
        $o->note_attributes = $data['order']['note_attributes'];
        $o->cancelled_at = $data['order']['cancelled_at'];
        $o->payment_method = $data['order']['payment_method'];
        $o->payment_reference = $data['order']['payment_reference'];
        $o->refunded_amount = $data['order']['refunded_amount'];
        $o->vendor = $data['order']['vendor'];
        $o->outstanding_balance = $data['order']['outstanding_balance'];
        $o->employee = $data['order']['employee'];
        $o->location = $data['order']['location'];
        $o->device_id = $data['order']['device_id'];
        $o->id_ref = $data['order']['id_ref'];
        $o->tags = $data['order']['tags'];
        $o->risk_level = $data['order']['risk_level'];
        $o->source = $data['order']['source'];
        $o->tax_1_name = $data['order']['tax_1_name'];
        $o->tax_1_value = $data['order']['tax_1_value'];
        $o->tax_2_name = $data['order']['tax_2_name'];
        $o->tax_2_value = $data['order']['tax_2_value'];
        $o->tax_3_name = $data['order']['tax_3_name'];
        $o->tax_3_value = $data['order']['tax_3_value'];
        $o->tax_4_name = $data['order']['tax_4_name'];
        $o->tax_4_value = $data['order']['tax_4_value'];
        $o->tax_5_name = $data['order']['tax_5_name'];
        $o->tax_5_value = $data['order']['tax_5_value'];
        $o->phone = $data['order']['phone'];
        $o->receipt_number = $data['order']['receipt_number'];
        $o->save();

        $oLine = ORM::for_table('sys_orders_import_lineitem')->create();
        $oLine->orders_import_id = $o->id;
        $oLine->lineitem_quantity = $data['order_lineitem']['lineitem_quantity'];
        $oLine->lineitem_name = $data['order_lineitem']['lineitem_name'];
        $oLine->lineitem_price = $data['order_lineitem']['lineitem_price'];
        $oLine->lineitem_compare_at_price = $data['order_lineitem']['lineitem_compare_at_price'];
        $oLine->lineitem_sku = $data['order_lineitem']['lineitem_sku'];
        $oLine->lineitem_requires_shipping = $data['order_lineitem']['lineitem_requires_shipping'];
        $oLine->lineitem_taxable = $data['order_lineitem']['lineitem_taxable'];
        $oLine->lineitem_fulfillment_status = $data['order_lineitem']['lineitem_fulfillment_status'];
        $oLine->lineitem_discount = $data['order_lineitem']['lineitem_discount'];
        $oLine->save();

        $oAddress = ORM::for_table('sys_orders_import_address')->create();
        $oAddress->orders_import_id = $o->id;
        $oAddress->type = 'billing';
        $oAddress->name = $data['order_address_billing']['name'];
        $oAddress->street = $data['order_address_billing']['street'];
        $oAddress->address1 = $data['order_address_billing']['address1'];
        $oAddress->address2 = $data['order_address_billing']['address2'];
        $oAddress->company = $data['order_address_billing']['company'];
        $oAddress->city = $data['order_address_billing']['city'];
        $oAddress->zip = $data['order_address_billing']['zip'];
        $oAddress->province = $data['order_address_billing']['province'];
        $oAddress->country = $data['order_address_billing']['country'];
        $oAddress->phone = $data['order_address_billing']['phone'];
        $oAddress->save();

        $oAddress = ORM::for_table('sys_orders_import_address')->create();
        $oAddress->orders_import_id = $o->id;
        $oAddress->type = 'shipping';
        $oAddress->name = $data['order_address_shipping']['name'];
        $oAddress->street = $data['order_address_shipping']['street'];
        $oAddress->address1 = $data['order_address_shipping']['address1'];
        $oAddress->address2 = $data['order_address_shipping']['address2'];
        $oAddress->company = $data['order_address_shipping']['company'];
        $oAddress->city = $data['order_address_shipping']['city'];
        $oAddress->zip = $data['order_address_shipping']['zip'];
        $oAddress->province = $data['order_address_shipping']['province'];
        $oAddress->country = $data['order_address_shipping']['country'];
        $oAddress->phone = $data['order_address_shipping']['phone'];
        $oAddress->save();

        return $o->id;
    }

}