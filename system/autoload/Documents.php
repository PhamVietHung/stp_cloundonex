<?php

Class Documents
{

    public static function assign($path, $title = '', $is_global = '0', $rid = '', $rtype = '', $document_type = null, $quote_amount = null, $quote_received_date = null, $supplier_id = null, $supplier_name = null, $status = null, $po = null, $invoice = null, $isCustom = null)
    {

        if ($is_global != '1') {
            $is_global = '0';
        }

        $ext = pathinfo($path, PATHINFO_EXTENSION);


        $token = Ib_Str::random_string(30);

        if ($title == '' || $path == '') {


            return false;

        }


        $d = ORM::for_table('sys_documents')->create();
        $d->title = $title;
        $d->file_path = $path;
        $d->file_dl_token = $token;
        $d->file_mime_type = $ext;
        $d->created_at = date('Y-m-d H:i:s');
        $d->is_global = $is_global;

        if ($document_type != null) {
            if (!$quote_amount || !$supplier_id) {
                return false;
            }
            if (!is_numeric($quote_amount)) {
                return false;
            }
            $d->document_type = $document_type;
        } else {
            $d->document_type = '';
        }
        $d->quote_amount = $quote_amount;
        $d->quote_received_date = $quote_received_date;
        $d->supplier_id = $supplier_id;
        $d->supplier_name = $supplier_name;
        $d->status = $status;
        $d->po = $po;
        $d->invoice = $invoice;
        $d->save();

        $did = $d->id();
        // check relation is posted

        if (!empty($isCustom)) {
            if ($rid != '' && $rtype != '') {

                $r = ORM::for_table('ib_doc_rel')->create();

                $r->rtype = $rtype;
                $r->rid = $rid;
                $r->did = $did;

                $r->save();

            }
        }
        //

        return $did;


    }

}