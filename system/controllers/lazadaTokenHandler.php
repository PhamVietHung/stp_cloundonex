<?php
include 'php/lazop/LazopClient.php';
include 'php/lazop/LazopRequest.php';

_auth();
$action = route(1,'get');

switch ($action) {
    case 'get':
        $code = _get('code');

        if ($code) {
            if (isset($_SESSION['lazada_get_token_id'])) {
                $lazadaId = $_SESSION['lazada_get_token_id'];
                $m = ORM::for_table('sys_orders_marketplace_settings')->find_one($lazadaId);

                if ($m) {
                    try {
                        $appKey = $m->api_key;
                        $secretKey = $m->api_secret;
                        $c = new LazopClient("https://auth.lazada.com/rest", $appKey, $secretKey);
                        $request = new LazopRequest('/auth/token/create');
                        $request->addApiParam('code', $code);

                        $response = $c->execute($request);
                        $resp = json_decode($response, true);

                        if ($resp) {
                            $r = ORM::for_table('webhook_lazada_token')->create();
                            $r->marketplace_settings_id = $lazadaId;
                            $r->access_token = $resp['access_token'];
                            $r->refresh_token = $resp['refresh_token'];
                            $r->expires_in = date('Y-m-d H:i:s', time() + $resp['expires_in']);
                            $r->refresh_expires_in = date('Y-m-d H:i:s', time() + $resp['refresh_expires_in']);
                            $r->save();
                        }
                        unset($_SESSION['lazada_get_token_id']);
                        r2(U . 'marketplace/list', 's', 'Get access token successfully.');
                    } catch (\Exception $e) {
                        unset($_SESSION['lazada_get_token_id']);
                        r2(U . 'marketplace/list', 'e', $e->getMessage());
                    }
                } else {
                    unset($_SESSION['lazada_get_token_id']);
                    r2(U . 'marketplace/list', 'e', 'Please choose a Lazada Account to get Token.');
                }
            } else {
                r2(U . 'marketplace/list', 'e', 'Please choose a Lazada Account to get Token.');
            }
        } else {
            r2(U . 'marketplace/list', 'e', 'Something went wrong. Please try again.');
        }

        break;
    case 'refresh':
        $id = $routes['2'];
        $m = ORM::for_table('webhook_lazada_token')->find_one($id);

        if ($m) {
            try {
                $lazadaId = $m->marketplace_settings_id;
                $r = ORM::for_table('sys_orders_marketplace_settings')->find_one($lazadaId);
                if ($r) {
                    $appKey = $r->api_key;
                    $secretKey = $r->api_secret;
                    $c = new LazopClient('https://auth.lazada.com/rest', $appKey, $secretKey);
                    $request = new LazopRequest('/auth/token/refresh');
                    $request->addApiParam('refresh_token', $m->refresh_token);
                    $response = $c->execute($request);
                    $resp = json_decode($response, true);

                    if ($resp) {
                        $m->access_token = $resp['access_token'];
                        $m->refresh_token = $resp['refresh_token'];
                        $m->expires_in = date('Y-m-d H:i:s', time() + $resp['expires_in']);
                        $m->refresh_expires_in = date('Y-m-d H:i:s', time() + $resp['refresh_expires_in']);
                        $m->save();
                    }

                    r2(U . 'marketplace/list', 's', 'Refresh access token successfully.');
                } else {
                    r2(U . 'marketplace/list', 'e', 'Please choose a Lazada Account to get Token.');
                }
            } catch (\Exception $e) {
                r2(U . 'marketplace/list', 'e', $e->getMessage());
            }
        }else {
            r2(U . 'marketplace/list', 'e', 'Please choose a Lazada Account to get Token.');
        }

        break;
    default:
        r2(U . 'marketplace/list', 'e', 'Wrong action defined.');
        break;
}
