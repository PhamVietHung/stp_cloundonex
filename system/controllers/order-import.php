<?php
/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
|
*/
_auth();
$ui->assign('_application_menu', 'order-import');
$ui->assign('_title', 'Import Orders' . '- ' . $config['CompanyName']);
$ui->assign('_st', 'Import Orders');
$action = $routes['1'];
$user = User::_info();
$ui->assign('user', $user);

Event::trigger('order-import');

switch ($action) {

    case 'list':

        $orders = ORM::for_table('sys_orders_import')->order_by_asc('id')->find_many();

        $ui->assign('jsvar', '
_L[\'are_you_sure\'] = \'' . $_L['are_you_sure'] . '\';
 ');

        $mode_css = '';
        $mode_js = '';

        $mode_css = Asset::css('footable/css/footable.core.min');

        $mode_js = Asset::js(array('footable/js/footable.all.min', 'numeric', 'orders/list-import'));

        $ui->assign('orders', $orders);
        $ui->assign('xheader', $mode_css);
        $ui->assign('xfooter', $mode_js);

        $xjq = '

    $(\'.amount\').autoNumeric(\'init\', {

    aSign: \'' . $config['currency_code'] . ' \',
    dGroup: ' . $config['thousand_separator_placement'] . ',
    aPad: ' . $config['currency_decimal_digits'] . ',
    pSign: \'' . $config['currency_symbol_position'] . '\',
    aDec: \'' . $config['dec_point'] . '\',
    aSep: \'' . $config['thousands_sep'] . '\',
    vMax: \'9999999999999999.00\',
                vMin: \'-9999999999999999.00\'

    });

 ';

        view('order_import_list');

        break;

    case 'import_csv':
        $ui->assign('xheader', Asset::css(array('dropzone/dropzone')));
        $ui->assign('xfooter', Asset::js(array('dropzone/dropzone', 'orders/import')));

        view('order_import');

        break;

    case 'csv_upload':

        $uploader = new Uploader();
        $uploader->setDir('storage/temp/');
        // $uploader->sameName(true);
        $uploader->setExtensions(array('csv'));  //allowed extensions list//
        if ($uploader->uploadFile('file')) {   //txtFile is the filebrowse element name //
            $uploaded = $uploader->getUploadName(); //get uploaded file name, renames on upload//

            $_SESSION['uploaded'] = $uploaded;

        } else {//upload failed
            _msglog('e', $uploader->getMessage()); //get upload error message
        }


        break;

    case 'csv_uploaded':


        if (isset($_SESSION['uploaded'])) {

            $uploaded = $_SESSION['uploaded'];

            $csv = new parseCSV();
            $csv->auto('storage/temp/' . $uploaded);

            $orders = $csv->data;
            $error = false;
            $cn = 0;

            foreach ($orders as $order) {
                try {
                    $o = ORM::for_table('sys_orders_import')->create();
                    $data['order']['name'] = $order['Name'];
                    $data['order']['email'] = $order['Email'];
                    $data['order']['financial_status'] = $order['Financial Status'];
                    $data['order']['paid_at'] = date('Y-m-d H:i:s',strtotime($order['Paid at']));
                    $data['order']['fulfillment_status'] = $order['Fulfillment Status'];
                    $data['order']['fulfilled_at'] = date('Y-m-d H:i:s',strtotime($order['Fulfilled at']));
                    $data['order']['accepts_marketing'] = $order['Accepts Marketing'];
                    $data['order']['currency'] = $order['Currency'];
                    $data['order']['subtotal'] = $order['Subtotal'];
                    $data['order']['shipping'] = $order['Shipping'];
                    $data['order']['taxes'] = $order['Taxes'];
                    $data['order']['total'] = $order['Total'];
                    $data['order']['discount_code'] = $order['Discount Code'];
                    $data['order']['discount_amount'] = $order['Discount Amount'];
                    $data['order']['shipping_method'] = $order['Shipping Method'];
                    $data['order']['created_at'] = date('Y-m-d H:i:s',strtotime($order['Created at']));
                    $data['order']['notes'] = $order['Notes'];
                    $data['order']['note_attributes'] = $order['Note Attributes'];
                    $data['order']['cancelled_at'] = date('Y-m-d H:i:s',strtotime($order['Cancelled at']));
                    $data['order']['payment_method'] = $order['Payment Method'];
                    $data['order']['payment_reference'] = $order['Payment Reference'];
                    $data['order']['refunded_amount'] = $order['Refunded Amount'];
                    $data['order']['vendor'] = $order['Vendor'];
                    $data['order']['outstanding_balance'] = $order['Outstanding Balance'];
                    $data['order']['employee'] = $order['Employee'];
                    $data['order']['location'] = $order['Location'];
                    $data['order']['device_id'] = $order['Device ID'];
                    $data['order']['id_ref'] = $order['Id'];
                    $data['order']['tags'] = $order['Tags'];
                    $data['order']['risk_level'] = $order['Risk Level'];
                    $data['order']['source'] = $order['Source'];
                    $data['order']['tax_1_name'] = $order['Tax 1 Name'];
                    $data['order']['tax_1_value'] = $order['Tax 1 Value'];
                    $data['order']['tax_2_name'] = $order['Tax 2 Name'];
                    $data['order']['tax_2_value'] = $order['Tax 2 Value'];
                    $data['order']['tax_3_name'] = $order['Tax 3 Name'];
                    $data['order']['tax_3_value'] = $order['Tax 3 Value'];
                    $data['order']['tax_4_name'] = $order['Tax 4 Name'];
                    $data['order']['tax_4_value'] = $order['Tax 4 Value'];
                    $data['order']['tax_5_name'] = $order['Tax 5 Name'];
                    $data['order']['tax_5_value'] = $order['Tax 5 Value'];
                    $data['order']['phone'] = $order['Phone'];
                    $data['order']['receipt_number'] = $order['Receipt Number'];

                    $data['order_lineitem']['lineitem_quantity'] = $order['Lineitem quantity'];
                    $data['order_lineitem']['lineitem_name'] = $order['Lineitem name'];
                    $data['order_lineitem']['lineitem_price'] = $order['Lineitem price'];
                    $data['order_lineitem']['lineitem_compare_at_price'] = $order['Lineitem compare at price'];
                    $data['order_lineitem']['lineitem_sku'] = $order['Lineitem sku'];
                    $data['order_lineitem']['lineitem_requires_shipping'] = $order['Lineitem requires shipping'];
                    $data['order_lineitem']['lineitem_taxable'] = $order['Lineitem taxable'];
                    $data['order_lineitem']['lineitem_fulfillment_status'] = $order['Lineitem fulfillment status'];
                    $data['order_lineitem']['lineitem_discount'] = $order['Lineitem discount'];

                    $data['order_address_billing']['name'] = $order['Billing Name'];
                    $data['order_address_billing']['street'] = $order['Billing Street'];
                    $data['order_address_billing']['address1'] = $order['Billing Address1'];
                    $data['order_address_billing']['address2'] = $order['Billing Address2'];
                    $data['order_address_billing']['company'] = $order['Billing Company'];
                    $data['order_address_billing']['city'] = $order['Billing City'];
                    $data['order_address_billing']['zip'] = $order['Billing Zip'];
                    $data['order_address_billing']['province'] = $order['Billing Province'];
                    $data['order_address_billing']['country'] = $order['Billing Country'];
                    $data['order_address_billing']['phone'] = $order['Billing Phone'];

                    $data['order_address_shipping']['name'] = $order['Shipping Name'];
                    $data['order_address_shipping']['street'] = $order['Shipping Street'];
                    $data['order_address_shipping']['address1'] = $order['Shipping Address1'];
                    $data['order_address_shipping']['address2'] = $order['Shipping Address2'];
                    $data['order_address_shipping']['company'] = $order['Shipping Company'];
                    $data['order_address_shipping']['city'] = $order['Shipping City'];
                    $data['order_address_shipping']['zip'] = $order['Shipping Zip'];
                    $data['order_address_shipping']['province'] = $order['Shipping Province'];
                    $data['order_address_shipping']['country'] = $order['Shipping Country'];
                    $data['order_address_shipping']['phone'] = $order['Shipping Phone'];
                    $save = OrderImport::add($data);

                    if (is_numeric($save)) {

                        $cn++;

                    }
                } catch (\Exception $e) {
                    $error = true;
                }
            }
            if($error){
                _msglog('e', 'An Error Occurred.');
            }else {
                _msglog('s', $cn . ' Orders Imported');
            }
        } else {
            _msglog('e', 'An Error Occurred while uploading the files');
        }
        break;

    default:
        echo 'action not defined';
}
