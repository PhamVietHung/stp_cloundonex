<?php

$action = route(1, 'supplier');
$actionType = route(2, 'quotations');
_auth();
$ui->assign('_application_menu', 'suppliers');
$ui->assign('_title', 'Notes ' . '- ' . $config['CompanyName']);
$user = User::_info();
$ui->assign('user', $user);

switch ($action) {


    case 'supplier':
        $ui->assign('jsvar', '
_L[\'are_you_sure\'] = \'' . $_L['are_you_sure'] . '\';
 ');

        $upload_max_size = ini_get('upload_max_filesize');
        $post_max_size = ini_get('post_max_size');

        $ui->assign('upload_max_size', $upload_max_size);
        $ui->assign('post_max_size', $post_max_size);

        $css_arr = array(
            's2/css/select2.min',
            'modal',
            'dp/dist/datepicker.min',
            'redactor/redactor',
            'modal',
            'dropzone/dropzone',
            'footable/css/footable.core.min'
        );
        $js_arr = array(
            'redactor/redactor.min',
            's2/js/select2.min',
            's2/js/i18n/' . lan(),
            'dp/dist/datepicker.min',
            'dp/i18n/' . $config['language'],
            'numeric',
            'filter.min',
            'modal',
            'dropzone/dropzone',
            'footable/js/footable.all.min',
            'js/documents'
        );
        $ui->assign('xheader', Asset::css($css_arr));
        $ui->assign('xfooter', Asset::js($js_arr));

        $xjq = '

var dl_token;

    $(".c_link").click(function (e) {
        e.preventDefault();

dl_token = $(this).attr("data-token")
        bootbox.prompt({
            title: "' . $_L['Secure Download Link'] . '",
            value: "' . U . 'client/dl/" + dl_token,
            buttons: {
        \'cancel\': {
            label: \'' . $_L['Cancel'] . '\'
        },
        \'confirm\': {
            label: \'' . $_L['OK'] . '\'
        }
    },
            callback: function(result) {
                if (result === null) {

                } else {
                   // alert(result);
                     $.post( "' . U . 'settings/networth_goal/", { goal: result })
        .done(function( data ) {
            location.reload();
        });
                }
            }
        });

    });

 ';

        $ui->assign('xjq', $xjq);
        $d = ORM::for_table('sys_documents')->where('document_type', $actionType)->find_array();
        $ui->assign('d', $d);
        $ui->assign('actionType', $actionType);

        // add supplier
        $c = ORM::for_table('crm_accounts')->select('id')->select('account')->select('company')->select('email')->order_by_desc('id')->where_like('type', '%Supplier%')->find_many();
        $po = ORM::for_table('sys_purchases')->where_not_equal('status', 'Paid')->order_by_desc('id')->find_many();
        $invoice = ORM::for_table('sys_invoices')->where_not_equal('status', 'Paid')->order_by_desc('id')->find_many();
        $ui->assign('c', $c);
        $ui->assign('po', $po);
        $ui->assign('invoice', $invoice);
        if (isset($routes['3']) AND ($routes['3'] != '')) {
            $p_cid = $routes['3'];
            $p_d = ORM::for_table('crm_accounts')->find_one($p_cid);
            if ($p_d) {
                $ui->assign('p_cid', $p_cid);
            }
        } else {
            $ui->assign('p_cid', '');
        }

        $ui->assign('idate', date('Y-m-d'));

        view('quotations');

        break;

    case 'post':

//        if (!has_access($user->roleid, 'documents', 'edit')) {
//            permissionDenied();
//        }
        $action_type = _post('action_type');
        $title = _post('title');
        $quote_amount = _post('amount');
        $quote_received_date = _post('quote_received_date');
        $supplierId = _post('supplier');
        $supplierName = '';
        $supplier = ORM::for_table('crm_accounts')->find_one($supplierId);
        if ($supplier) {
            $supplierName = $supplier->account;
        }
        $file_link = _post('file_link');
        $rid = _post('rid');
        $rtype = _post('rtype');
        $status = _post('status');
        $po = isset($_POST['po']) ? $_POST['po'] : null;
        $invoice = isset($_POST['invoice']) ? $_POST['invoice'] : null;

        $did = Documents::assign($file_link, $title, 1, $rid, $rtype, $action_type, $quote_amount, $quote_received_date, $supplierId, $supplierName, $status, $po, $invoice, null);

        if (!empty($invoice) && $status == 'Full') {
            $docForInvoice = ORM::for_table('sys_documents')->where('supplier_id', $supplierId)->where('document_type', 'invoices')->find_many();
            foreach ($docForInvoice as $doc) {
                if (!empty($doc) && !empty($doc['supplier_id']) && !empty($doc['po'])) {
                    $p = ORM::for_table('sys_purchases')->find_one($doc['po']);
                    if (!empty($p) && !empty($p->id)) {
                        $p->stage = 'Accepted';
                        $p->save();
                    }
                }
            }
        }
        if ($did) {
            echo $did;
        } else {
            if (!is_numeric($quote_amount)) {
                ib_die('Please re-check amount.');
            } else {
                ib_die($_L['All Fields are Required']);
            }
        }
        break;

    case 'getpo':
        $type = $routes['2'];
        $supId = $routes['3'];
        $sup = ORM::for_table('crm_accounts')->find_one($supId);
        if ($type == 'invoices') {
            $items = ORM::for_table('sys_purchases')->where('userid', $supId)->where_not_equal('status', 'Paid')->order_by_desc('id')->find_many();
            if (count($items) > 0) {
                foreach ($items as $item) {
                    $title = !empty($item['subject']) ? $item['subject'] : 'Upload purchases';
                    echo "<option value='" . $item['id'] . "'>" . $title . "</option>";
                }
            } else {
                $title = 'Upload purchases ' . $sup->account;
                echo "<option value=''>" . $title . "</option>";
            }
        } else {
            $items = ORM::for_table('sys_documents')->where('supplier_id', $supId)->where('document_type', 'invoices')->order_by_desc('id')->find_many();
            if (count($items) > 0) {
                foreach ($items as $item) {
                    $title = !empty($item['title']) ? $item['title'] : 'Upload invoice';
                    echo "<option value='" . $item['id'] . "'>" . $title . "</option>";
                }
            } else {
                $title = 'Upload invoice ' . $sup->account;
                echo "<option value=''>" . $title . "</option>";
            }
        }
        break;
}