<?php
/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
|
*/
_auth();
$ui->assign('_application_menu', 'orders');
$ui->assign('_title', $_L['Orders'] . '- ' . $config['CompanyName']);
$ui->assign('_st', $_L['Orders']);
$action = $routes['1'];
$user = User::_info();
$ui->assign('user', $user);

Event::trigger('orders');

switch ($action) {

    case 'list':

        $ui->assign('jsvar', '
_L[\'are_you_sure\'] = \'' . $_L['are_you_sure'] . '\';
 ');

        $mode_css = '';
        $mode_js = '';

        $mode_css = Asset::css('footable/css/footable.core.min');

        $mode_js = Asset::js(array('footable/js/footable.all.min', 'numeric', 'orders/list'));

        $d = ORM::for_table('sys_orders')->order_by_desc('id')->find_many();


        $ui->assign('d', $d);
        $ui->assign('xheader', $mode_css);
        $ui->assign('xfooter', $mode_js);

        $ui->assign('xjq', '
         $(\'.amount\').autoNumeric(\'init\', {

   
    dGroup: ' . $config['thousand_separator_placement'] . ',
    aPad: ' . $config['currency_decimal_digits'] . ',
    pSign: \'' . $config['currency_symbol_position'] . '\',
    aDec: \'' . $config['dec_point'] . '\',
    aSep: \'' . $config['thousands_sep'] . '\',
    vMax: \'9999999999999999.00\',
                vMin: \'-9999999999999999.00\'

    });


$(\'[data-toggle="tooltip"]\').tooltip();

 ');

        view('orders_list');

        break;


    case 'add':


        // find all customers


        $c = ORM::for_table('crm_accounts')->select('id')->select('account')->select('company')->select('email')->where('type', 'Customer')->order_by_desc('id')->find_many();
        $ui->assign('c', $c);

        // find all products

        $p = ORM::for_table('sys_items')->select('id')->select('name')->find_array();

        $ui->assign('p', $p);

        if (isset($routes['3']) AND ($routes['3'] != '')) {
            $p_cid = $routes['3'];
            $p_d = ORM::for_table('crm_accounts')->find_one($p_cid);
            if ($p_d) {
                $ui->assign('p_cid', $p_cid);
            }
        } else {
            $ui->assign('p_cid', '');
        }
        $currencies = M::factory('Models_Currency')->find_array();
        $ui->assign('currencies', $currencies);
        $css_arr = array('s2/css/select2.min', 'modal', 'dp/dist/datepicker.min');


        $mode_js = Asset::js(array('s2/js/select2.min', 's2/js/i18n/' . lan(), 'dp/dist/datepicker.min', 'dp/i18n/' . $config['language'], 'numeric', 'modal'));
        $ui->assign('xheader', Asset::css($css_arr));

        $ui->assign('xfooter', $mode_js);

        $ui->assign('xjq', '
        
        function ib_amount() {
    
}
 $(\'.amount\').autoNumeric(\'init\', {

    aSign: \'' . $config['currency_code'] . ' \',
    dGroup: ' . $config['thousand_separator_placement'] . ',
    aPad: ' . $config['currency_decimal_digits'] . ',
    pSign: \'' . $config['currency_symbol_position'] . '\',
    aDec: \'' . $config['dec_point'] . '\',
    aSep: \'' . $config['thousands_sep'] . '\',
    vMax: \'9999999999999999.00\',
                vMin: \'-9999999999999999.00\'

    });

 ');

        view('orders_add');

        break;

    case 'post':


        $pIds = _post('pid');


        $cid = _post('cid');
        $status = _post('status');
        $billing_cycle = _post('billing_cycle');

        $pPrices = _post('price');
        $pPQtys = _post('qty');
        // $items = $_POST['items'];

        if (count($pIds) == 0 || $cid == '') {

            i_close($_L['All Fields are Required']);

        }

        // find the customer

        $c = ORM::for_table('crm_accounts')->find_one($cid);

        if (!$c) {
            i_close($_L['User Not Found']);
        }


        $today = date('Y-m-d');

        // create invoice

        $generate_invoice = _post('generate_invoice');

        if ($generate_invoice == 'Yes') {

            $invoice = Invoice::forMultipleItem($cid, $pIds, $pPrices, $pPQtys);

            $iid = $invoice['id'];

        } else {
            $amount = Finance::amount_fix(end($pPrices));
            $iid = 0;
        }

        $currency_id = _post('currency');

        $currency_find = Currency::where('iso_code', $currency_id)->first();
        if ($currency_find) {
            $currency = $currency_find->id;
            $allCurrency = Currency::getAllCurrencies();
            if (empty($currency_find->symbol)) {
                if (isset($allCurrency[$currency_id])) {
                    $currency_symbol = $allCurrency[$currency_id]['symbol'];
                }
            } else {
                $currency_symbol = $currency_find->symbol;
            }
            $currency_rate = $currency_find->rate;
        } else {
            $currency = 0;
            $currency_symbol = $config['currency_code'];
            $currency_rate = 1.0000;
        }

        $p = ORM::for_table('sys_items')->find_one(end($pIds));

        $order = ORM::for_table('sys_orders')->create();

        $i = 0;
        $total = 0;
        foreach ($pPQtys as $qty) {
            $priceCustom = (float)(trim(str_replace($config['currency_code'], '', $pPrices[$i])));
            $total += (float)((int)$qty * $priceCustom);
            $i++;
        }
        $order->stitle = $p->name;
        $order->pid = end($pIds);
        $order->cid = $cid;
        $order->cname = $c->account;
        $order->date_added = $today;
        $order->amount = $total;
        $order->ordernum = _raid(10);
        $order->status = $status;
        $order->billing_cycle = $billing_cycle;
        $order->iid = $iid;
        $order->currency = $currency;
        $order->currency_symbol = $currency_symbol;
        $order->currency_rate = $currency_rate;
        $order->currency_iso_code = $currency_id;
        $order->save();

        //add order items
        $i = 0;
        foreach ($pIds as $pid) {
            $orderItem = ORM::for_table('order_items')->create();
            $p = ORM::for_table('sys_items')->find_one($pid);
            //decrease inventory
//            Inventory::decreaseByItemNumber($p->item_number,$pPQtys[$i]);

            $priceCustom = (float)(trim(str_replace($config['currency_code'], '', $pPrices[$i])));

            $orderItem->customer_id = $cid;
            $orderItem->order_id = $order->id();
            $orderItem->item_id = $pid;
            $orderItem->item_name = $p->name;
            $orderItem->quantity = $pPQtys[$i];
            $orderItem->unit_price = $priceCustom;
            $orderItem->sku = $p->item_number;

            $orderItem->total = (float)((int)$pPQtys[$i] * $priceCustom);
            $orderItem->created_at = $today;
            $orderItem->updated_at = $today;

            $orderItem->weight = $p->weight;
            $orderItem->width = $p->width;
            $orderItem->length = $p->length;
            $orderItem->height = $p->height;
            $orderItem->package_type = $p->package_type;
            $orderItem->save();
            $i++;
        }

        echo $order->id();


        break;


    case 'view':


        $oid = route(2);

        // find the orders

        $order = ORM::for_table('sys_orders')->find_one($oid);

        if ($order) {

            $ui->assign('jsvar', '
_L[\'data_updated\'] = \'' . $_L['Data Updated'] . '\';
_L[\'email_sent\'] = \'' . $_L['Email Sent'] . '\';
 ');


            $ui->assign('xfooter', Asset::js(array('tinymce/tinymce.min', 'numeric', 'orders/view'), 2));

            $ui->assign('order', $order);

            $xjq = '

    $(\'.amount\').autoNumeric(\'init\', {

    aSign: \'' . $config['currency_code'] . ' \',
    dGroup: ' . $config['thousand_separator_placement'] . ',
    aPad: ' . $config['currency_decimal_digits'] . ',
    pSign: \'' . $config['currency_symbol_position'] . '\',
    aDec: \'' . $config['dec_point'] . '\',
    aSep: \'' . $config['thousands_sep'] . '\',
    vMax: \'9999999999999999.00\',
                vMin: \'-9999999999999999.00\'

    });

 ';

            $ui->assign('xjq', $xjq);
            $ui->assign('xfooter', Asset::js(array('modal', 'dropzone/dropzone', 'redactor/redactor.min', 'numeric', 'jslib/order')));

            view('orders_view');

        } else {
            i_close('Order Not Found');
        }


        break;
    case 'view-items':

        $id = $routes['2'];
        $items = ORM::for_table('order_items')->where('order_id', $id)->order_by_desc('id')->find_array();

        if (count($items) > 0) {
            $content = '<tbody>';
            $i = 1;
            foreach ($items as $item) {

                $d = ORM::for_table('sys_items')->find_one($item['item_id']);
                if ($d && !empty($d->image)) {
                    $img = '<img width="100px" height="100px" src="' . APP_URL . '/storage/items/' . $d->image . '" class="img-responsive">';
                } else {
                    $img = '<img width="100px" height="100px" src="' . APP_URL . '/ui/lib/imgs/item_placeholder.png' . '" class="img-responsive">';
                }
                $content .= '<tr role="row" class="even">
                                <td class="text-center">' . $i . '</td>
                                <td class="text-center">' . $item['sku'] . '</td>
                                <td class="text-center">' . $img . '</td>
                                <td class="text-center">' . $item['item_name'] . '</td>
                                <td class="text-center">' . $item['quantity'] . '</td>
                                <td class="text-center">' . $item['unit_price'] . '</td>
                                <td class="text-center">' . $item['total'] . '</td>
                            </tr>';
                $i++;
            }
            $content .= '</tbody>';
            echo '
    <div class="modal-header" style="margin-top: 150px">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Order Items</h3>
    </div>
    <table class="table table-bordered table-hover display" id="ib_dt" style="background: white;">
                                            <thead>
                                            <tr class="heading">
                                                <th class="text-center">#</th>
                                                <th class="text-center">SKU</th>
                                                <th class="text-center">Image</th>
                                                <th class="text-center">Description</th>
                                                <th class="text-center">Qty</th>
                                                <th class="text-center">Price</th>
                                                <th class="text-center">Total</th>
                                            </tr>
                                            </thead>
                                            ' . $content . '
                                        </table>
	';
        } else {
            $content = '
            <div class="modal-header" style="margin-top: 150px">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Order Items</h3>
            </div>
            ';
            $content .= '<div style="text-align: center;margin-top: 45px;"> Not found </div>';
//            $content .= '<button type="button" data-dismiss="modal" class="btn" style="float: right; margin: 10px;">'.$_L['Close'].'</button>';
            echo $content;
        }

        break;

    case 'set':

        $id = route(2);
        $status = route(3);

        $allowed_status = array('Pending', 'Active', 'Cancelled', 'Fraud', 'Processing', 'Shipped');

        if (in_array($status, $allowed_status)) {

        } else {
            $msg = 'Invalid Status';
        }

        $d = ORM::for_table('sys_orders')->find_one($id);

        if ($d) {
            $orderItems = ORM::for_table('order_items')->where('order_id', $id)->find_many();
            //validate
            foreach ($orderItems as $item) {
                if ($item->item_id != '' || !empty($item->item_name)) {
                    $i = ORM::for_table('sys_items')->find_one($item->item_id);
                    if (!$i) {
                        $i = ORM::for_table('sys_items')->where('name', strip_tags($item->item_name))->find_one();
                    }
                    if ($i && $i->is_bundle == 1 && (int)$i->inventory <= 0) {
                        r2(U . 'orders/view/' . $id, 'e', 'Bundle cannot be zero.');
                        break;
                    }
                }
            }
            if ($d->status != $status) {
                $d->status = $status;
                $d->save();

                if ($status == 'Shipped') {
                    //decreaseByItemNumber
                    foreach ($orderItems as $item) {
                        if ($item->item_id != '' || !empty($item->item_name)) {
                            $i = ORM::for_table('sys_items')->find_one($item->item_id);
                            if (!$i) {
                                $i = ORM::for_table('sys_items')->where('name', strip_tags($item->item_name))->find_one();
                            }
                            if ($i) {
                                Inventory::decreaseByItemNumber($i->item_number, $item->quantity, $user, $d->ordernum);
                            }
                        }
                    }
                }
            } else {
                $msg = 'Can not update, Order have been is ' . $status;
                r2(U . 'orders/view/' . $id, 'e', $msg);
                break;
            }
            $msg = $_L['Data Updated'];


        } else {
            $msg = 'Order not found';
        }


        r2(U . 'orders/list/', 's', $msg);


        break;


    case 'save_activation':

        $oid = _post('oid');

        $activation_subject = $_POST['activation_subject'];

        $activation_message = $_POST['activation_message'];

        $send_email = _post('send_email');

        if ($activation_message == '' || $activation_message == '') {
            i_close($_L['All Fields are Required']);
        }


        $d = ORM::for_table('sys_orders')->find_one($oid);

        if ($d) {

            $cid = $d->cid;

            $d->activation_subject = $activation_subject;
            $d->activation_message = $activation_message;

            $d->save();


            if ($send_email == 'yes') {

                // Send Email

                $client = ORM::for_table('crm_accounts')->find_one($cid);

                if ($client) {

                    if ($client->email != '') {
                        Ib_Email::_send($client->account, $client->email, $activation_subject, $activation_message, $cid);
                    }

                }


            }


            echo $d->id();

        } else {

            echo 'Order not found';

        }


        break;


    case 'module':

        $id = route(2);


        $d = ORM::for_table('sys_orders')->find_one($id);

        if ($d) {

            Event::trigger('orders/modules/');

            r2(U . 'orders/view/' . $id . '/', 's', $_L['Data Updated']);


        } else {
            $msg = 'Order not found';
        }


        break;


    default:
        echo 'action not defined';
}