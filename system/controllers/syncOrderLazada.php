<?php
try {
    $code = $_GET['code'];
    $log = ORM::for_table('webhook_logs')->create();
    $log->date = date('Y-m-d');
    $log->logs = $code;
    $log->save();
} catch (\Exception $e) {
    $log = ORM::for_table('webhook_logs')->create();
    $log->date = date('Y-m-d');
    $log->logs = $e->getMessage();
    $log->save();
}
?>