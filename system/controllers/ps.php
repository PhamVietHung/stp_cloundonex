<?php
/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
|
*/
_auth();
$ui->assign('_application_menu', 'ps');
$ui->assign('_title', $_L['Products n Services'] . '- ' . $config['CompanyName']);
$ui->assign('_st', $_L['Products n Services']);
$action = $routes['1'];
$user = User::_info();
$ui->assign('user', $user);

if (!has_access($user->roleid, 'products_n_services', 'view')) {
    permissionDenied();
}

switch ($action) {
    case 'modal-list-manufacturer':

        $d = ORM::for_table('crm_accounts')->where_like('type', "%Supplier%")->order_by_asc('id')->find_array();
        $mode_js = Asset::js(array('footable/js/footable.all.min', 'numeric', 'orders/list'));
        $mode_css = Asset::css('footable/css/footable.core.min');
        echo '


<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h3>' . $_L['Products n Services'] . '</h3>
</div>
<div class="modal-body">
<input type="text" id="myInput" onkeyup="filterPS()" placeholder="Search for manufacturer Name..">
<table class="table table-striped table-bordered table-hover sys_table footable"  data-filter="#myInput" id="items_table_manufacturer" data-page-size="10">
      <thead>
        <tr>
          <th width="10%">#</th>
          <th width="20%">Name</th>
          <th width="55%">Email</th>
          <th width="15%">Phone</th>
        </tr>
      </thead>
      <tbody>
       ';

        foreach ($d as $ds) {
            echo ' <tr class="manufacturer">
          <td class="manufacturer"><input type="checkbox" class="si"></td>
          <td class="manufacturer">' . $ds['account'] . '</td>
          <td class="manufacturer"><input type="hidden" name="currency" value="' . $config['currency_code'] . '">' . $ds['email'] . '</td>
          <td class="manufacturer"><input type="hidden" value="' . $ds['id'] . '">' . $ds['phone'] . '</td>
        </tr>';
        }

        echo '

      </tbody>
      <tfoot class="pager-custom">
        <tr>
            <td colspan="4">
                <ul class="pagination">
                </ul>
            </td>
        </tr>
        </tfoot>
    </table>

</div>
<div class="modal-footer">

	<button type="button" data-dismiss="modal" class="btn btn-close">' . $_L['Close'] . '</button>
	<button class="btn btn-primary update">' . $_L['Select'] . '</button>
</div>
<style type="text/css">
#myInput {
    width: 100%; /* Full-width */
    font-size: 16px; /* Increase font-size */
    padding: 12px 20px 12px 40px; /* Add some padding */
    border: 1px solid #ddd; /* Add a grey border */
    margin-bottom: 12px; /* Add some space below the input */
}
</style>
<script type="text/javascript">
function filterPS() {
  // Declare variables 
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("items_table_manufacturer");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don\'t match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>
' . $mode_js . $mode_css;

        break;

    case 'modal-list':


        if (!has_access($user->roleid, 'products_n_services', 'view')) {
            permissionDenied();
        }

        $bundled = $routes['2'];
        if (isset($bundled) && $bundled == 'exBundled') {
            $d = ORM::for_table('sys_items')->where_null('is_bundle')->order_by_asc('name')->find_array();
        } else {
            $d = ORM::for_table('sys_items')->order_by_asc('name')->find_array();
        }

        $mode_js = Asset::js(array('footable/js/footable.all.min', 'numeric', 'orders/list'));
        $mode_css = Asset::css('footable/css/footable.core.min');
        echo '


<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h3>' . $_L['Products n Services'] . '</h3>
</div>
<div class="modal-body">
<input type="text" id="myInput" onkeyup="filterPS()" placeholder="Search for Product Name..">
<table class="table table-striped table-bordered table-hover sys_table footable"  data-filter="#myInput" id="items_table" data-page-size="10">
      <thead>
        <tr>
          <th width="10%">#</th>
          <th width="20%">Item Code(SKU)</th>
          <th width="55%">' . $_L['Item Name'] . '</th>

          <th width="15%">' . $_L['Price'] . '</th>
        </tr>
      </thead>
      <tbody>
       ';
        $priceColumn = 'sales_price';
        if (isset($routes['2'])) {
            $isForPO = $routes['2'];
            if ($isForPO == 'forPO') {
                $priceColumn = 'cost_price';
            }
        }

        foreach ($d as $ds) {
            $price = number_format($ds[$priceColumn], 2, $config['dec_point'], $config['thousands_sep']);
            $itemName = htmlentities($ds['item_number']);
            $name = htmlentities($ds['name']);
            echo ' <tr>
          <td class="product"><input type="checkbox" class="si"></td>
          <td class="product">' . $itemName . '</td>
          <td class="product">' . $name . '</td>

          <td class="product price">' . $price . '</td>
        </tr>';
        }

        echo '

      </tbody>
      <tfoot class="pager-custom">
        <tr>
            <td colspan="4">
                <ul class="pagination">
                </ul>
            </td>
        </tr>
        </tfoot>
    </table>

</div>
<div class="modal-footer">

	<button type="button" data-dismiss="modal" class="btn btn-close">' . $_L['Close'] . '</button>
	<button class="btn btn-primary update">' . $_L['Select'] . '</button>
</div>
<style type="text/css">
#myInput {
    width: 100%; /* Full-width */
    font-size: 16px; /* Increase font-size */
    padding: 12px 20px 12px 40px; /* Add some padding */
    border: 1px solid #ddd; /* Add a grey border */
    margin-bottom: 12px; /* Add some space below the input */
}
</style>
<script type="text/javascript">
function filterPS() {
  // Declare variables 
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("items_table");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don\'t match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>
' . $mode_js . $mode_css;

        break;

    case 'modal-list-po':

        if (!has_access($user->roleid, 'products_n_services', 'view')) {
            permissionDenied();
        }
        $viewCostPrice = true;
        if (!has_access($user->roleid, 'cost_price', 'view')) {
            $viewCostPrice = false;
        }
        $d = ORM::for_table('sys_items')->order_by_asc('name')->find_array();
        $mode_js = Asset::js(array('footable/js/footable.all.min', 'numeric', 'orders/list'));
        $mode_css = Asset::css('footable/css/footable.core.min');
        echo '


<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h3>' . $_L['Products n Services'] . '</h3>
</div>
<div class="modal-body">
<input type="text" id="myInput" onkeyup="filterPS()" placeholder="Search for Product Name..">
<table class="table table-striped table-bordered table-hover sys_table footable"  data-filter="#myInput" id="items_table" data-page-size="10">
      <thead>
        <tr>
          <th width="5%">#</th>
          <th width="20%">Item Code(SKU)</th>
          <th width="40%">' . $_L['Item Name'] . '</th>

          <th width="5%">' . $_L['Price'] . '</th>';
        if ($viewCostPrice) {
            echo '
          <th width="30%">Manufacturer</th>';
        }
        echo '
        </tr>
      </thead>
      <tbody>
       ';
        $priceColumn = 'cost_price';
        $manId = $routes[2];
        foreach ($d as $ds) {
            $manInfo = '';
            $man = ORM::for_table('sys_manufacturer_product')->where('id_item', $ds['id'])->find_many();
            if ($man) {
                foreach ($man as $m) {
                    $isChecked = 'disabled';
                    if ($m->id_manufacturer == $manId) {
                        $isChecked = 'checked="checked" readonly="readonly" onclick="return false;"';
                    }
                    $manInfo .= '<p><input type="checkbox" name="manufacturer-price[]" value="' . $m->cost_price . '" ' . $isChecked . ' class="si"> ' . $m->name . ' / ' . $m->cost_price . '</p>';
                }
            }
            $price = number_format(0, 2, $config['dec_point'], $config['thousands_sep']);
            $itemName = htmlentities($ds['item_number']);
            $name = htmlentities($ds['name']);
            echo ' <tr>
          <td class="product">
            <input name="check-selected[]" type="checkbox" class="si">
          </td>
          <td class="product">' . $itemName . '</td>
          <td class="product">' . $name . '</td>
          <td class="product price">' . $price . '</td>';
            if ($viewCostPrice) {
                echo '
                <td class="product">' . $manInfo . '</td>';
            }
            echo '
        </tr>';
        }

        echo '

      </tbody>
      <tfoot class="pager-custom">
        <tr>
            <td colspan="5">
                <ul class="pagination">
                </ul>
            </td>
        </tr>
        </tfoot>
    </table>

</div>
<div class="modal-footer">

	<button type="button" data-dismiss="modal" class="btn btn-close">' . $_L['Close'] . '</button>
	<button class="btn btn-primary update">' . $_L['Select'] . '</button>
</div>
<style type="text/css">
#myInput {
    width: 100%; /* Full-width */
    font-size: 16px; /* Increase font-size */
    padding: 12px 20px 12px 40px; /* Add some padding */
    border: 1px solid #ddd; /* Add a grey border */
    margin-bottom: 12px; /* Add some space below the input */
}
</style>
<script type="text/javascript">
function filterPS() {
  // Declare variables 
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("items_table");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don\'t match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>
' . $mode_js . $mode_css;

        break;

    case 'p-new':

        if (!has_access($user->roleid, 'products_n_services', 'create')) {
            permissionDenied();
        }

        $units = ORM::for_table('sys_units')->order_by_asc('sorder')->find_array();
        $ui->assign('units', $units);

        $ui->assign('type', 'Product');
        $ui->assign('xheader', Asset::css(array('modal', 'dropzone/dropzone', 'redactor/redactor')));
        $ui->assign('xfooter', Asset::js(array('modal', 'dropzone/dropzone', 'redactor/redactor.min', 'numeric', 'jslib/add-ps')));

        $ui->assign('xjq', '
 $(\'.amount\').autoNumeric(\'init\');
 ');

        $max = ORM::for_table('sys_items')->max('id');
        $nxt = $max + 1;
        $ui->assign('nxt', $nxt);

        view('add-ps');


        break;


    case 's-new':

        if (!has_access($user->roleid, 'products_n_services', 'create')) {
            permissionDenied();
        }

        $ui->assign('type', 'Service');
        $ui->assign('xheader', Asset::css(array('modal', 'dropzone/dropzone', 'redactor/redactor')));
        $ui->assign('xfooter', Asset::js(array('modal', 'dropzone/dropzone', 'redactor/redactor.min', 'numeric', 'jslib/add-ps')));

        $ui->assign('xjq', '
 $(\'.amount\').autoNumeric(\'init\');
 ');

        $max = ORM::for_table('sys_items')->max('id');
        $nxt = $max + 1;
        $ui->assign('nxt', $nxt);
        view('add-ps');


        break;


    case 'add-post':

        if (!has_access($user->roleid, 'products_n_services', 'edit')) {
            permissionDenied();
        }

        $msg = '';


        $name = _post('name');
        $sales_price = _post('sales_price', '0.00');
        $sales_price = Finance::amount_fix($sales_price);
        $item_number = _post('item_number');
        $description = _post('description');
        $package_type = _post('package_type');
        $type = _post('type');

        // other variables

        // check item number already exist

        if ($item_number != '') {
            $check = ORM::for_table('sys_items')->where('item_number', $item_number)->find_one();
            if ($check) {
                $msg .= 'Item number already exist <br>';
            }
        }

        $inventory = _post('inventory');

        if (!is_numeric($inventory)) {
            $inventory = '0';
        }

        $unit = _post('unit');

        $weight = _post('weight');

        $width = _post('width');
        $length = _post('length');
        $height = _post('height');

        if (!is_numeric($weight)) {
            $weight = '0.0000';
        }


        if ($name == '') {
            $msg .= 'Item Name is required <br>';
        }

        $is_bundle = _post('is_bundled');
        if ($is_bundle != "") {
            if (isset($_POST['desc'])) {
                $descriptionBundle = $_POST['desc'];
                foreach ($descriptionBundle as $deB) {
                    if ($deB == '') {
                        $msg .= 'Item Name for bunbled product is required <br>';
                    }
                }
            } else {
                $msg .= 'Please select product for bundled product <br>';
            }
        }


        $tax_code = _post('tax_code');
        $sales_price = Finance::amount_fix($sales_price);

        if (!is_numeric($sales_price)) {
            $sales_price = '0.00';
        }

        $cost_price = _post('cost_price', '0.00');

        $cost_price = Finance::amount_fix($cost_price);

        if (!is_numeric($cost_price)) {
            $cost_price = '0.00';
        }


        if ($msg == '') {
            // find currency

            $currency_id = $config['home_currency'];
            $currency_find = Currency::where('iso_code', $currency_id)->first();
            if ($currency_find) {
                $currency = $currency_find->id;
                $allCurrency = Currency::getAllCurrencies();
                if (empty($currency_find->symbol)) {
                    if (isset($allCurrency[$currency_id])) {
                        $currency_symbol = $allCurrency[$currency_id]['symbol'];
                    }
                } else {
                    $currency_symbol = $currency_find->symbol;
                }
                $currency_rate = $currency_find->rate;
            } else {
                $currency = 0;
                $currency_symbol = $config['currency_code'];
                $currency_rate = 1.0000;
            }

            $d = ORM::for_table('sys_items')->create();
            $d->name = $name;
            $d->sales_price = $sales_price;
            $d->item_number = $item_number;
            $d->description = $description;
            $d->package_type = $package_type;
            $d->type = $type;
//others
            $d->reorder_level = _post('reorder_level');
            $d->unit = $unit;
            $d->weight = $weight;
            $d->width = (float)$width;
            $d->length = (float)$length;
            $d->height = (float)$height;
            $d->inventory = $inventory;
            $d->e = '';

            // other variables

            $d->image = _post('file_link');
            $d->cost_price = $cost_price;

            $d->tax_code = $tax_code;
            if ($is_bundle != "") {
                $d->is_bundle = 1;
            } else {
                $d->is_bundle = null;
            }
            $d->currency = $currency;
            $d->currency_symbol = $currency_symbol;
            $d->currency_rate = $currency_rate;
            $d->currency_iso_code = $currency_id;


            $d->save();
            //custom bundle
            if ($is_bundle != "") {
                $i = 0;
                $item_code = $_POST['item_code'];
                $description = $_POST['desc'];
                $qty = $_POST['qty'];
                $price = $_POST['amount'];
                $total = $_POST['total'];
                $taxed = $_POST['taxed'];

                foreach ($item_code as $item) {

                    $bu = ORM::for_table('sys_bundle_product')->create();
                    $bu->id_bundle = $d->id();
                    $bu->item_code = $item;
                    $bu->description = $description[$i];
                    $bu->qty = $qty[$i];
                    $bu->price = (float)$price[$i];
                    $bu->total = (float)$total[$i];
                    $bu->taxable = $total[$i];

                    if (($taxed[$i]) == 'Yes') {
                        $bu->taxable = 1;
                    } else {
                        $bu->taxable = 0;
                    }
                    $bu->save();
                    $i++;
                }
            }
            //custom manufacturer
            if (isset($_POST['idManufacturer'])) {
                $i = 0;
                $id_manufacturer = $_POST['idManufacturer'];
                $name_manufacturer = $_POST['item_name_manufacturer'];
                $cost_price_manufacturer = $_POST['cost_price_manufacturer'];
                if (is_array($id_manufacturer) && count($id_manufacturer) > 0) {
                    foreach ($id_manufacturer as $id) {
                        $ma = ORM::for_table('sys_manufacturer_product')->create();
                        $ma->id_item = $d->id();
                        $ma->id_manufacturer = $id;
                        $ma->name = $name_manufacturer[$i];
                        $ma->cost_price = (float)$cost_price_manufacturer[$i];
                        $ma->save();
                        $i++;
                    }
                }
            }


            _msglog('s', $_L['Item Added Successfully']);

            echo $d->id();
        } else {
            echo $msg;
        }
        break;

    case 'edit-bundle':
        $pId = $routes['2'];
        $items = ORM::for_table('sys_bundle_product')->where('id_bundle', $pId)->order_by_asc('id')->find_many();
        if ($items) {
            $product = ORM::for_table('sys_items')->find_one();
            if (!empty($product->currency_symbol)) {
                $currency = $product->currency_symbol;
            } else {
                $currency = $config['currency_code'];
            }
            $ui->assign('id_bundled_product', $pId);
            $ui->assign('items', $items);
            $ui->assign('xheader', Asset::css(array('modal', 'dropzone/dropzone', 'redactor/redactor')));
            $ui->assign('xfooter', Asset::js(array('modal', 'dropzone/dropzone', 'redactor/redactor.min', 'numeric', 'jslib/bundled-edit')));

            $ui->assign('xjq', '
         $(\'.amount\').autoNumeric(\'init\', {

   
    dGroup: ' . $config['thousand_separator_placement'] . ',
    aPad: ' . $config['currency_decimal_digits'] . ',
    pSign: \'' . $config['currency_symbol_position'] . '\',
    aDec: \'' . $config['dec_point'] . '\',
    aSign: \'' . $currency . '\',
    aSep: \'' . $config['thousands_sep'] . '\',
    vMax: \'9999999999999999.00\',
                vMin: \'-9999999999999999.00\'

    });
 ');
            view('bundled-edit');

        } else {
            echo 'Product Not Found';
        }
        break;


    case 'edit-bundle-post':

        $idBundled = $_POST['id_bundled_product'];

        $i = 0;
        if (isset($_POST['item_code'])) {
            ORM::for_table('sys_bundle_product')->where('id_bundle', $idBundled)->delete_many();

            $item_code = $_POST['item_code'];
            $description = $_POST['desc'];
            $qty = $_POST['qty'];
            $price = $_POST['amount'];
            $total = $_POST['total'];
            $taxed = $_POST['taxed'];

            $totalCostPrice = 0;
            foreach ($item_code as $item) {

                $bu = ORM::for_table('sys_bundle_product')->create();

                $totalCostPrice += (float)$total[$i];
                $bu->id_bundle = $idBundled;
                $bu->item_code = $item;
                $bu->description = $description[$i];
                $bu->qty = $qty[$i];
                $bu->price = $price[$i];
                $bu->total = $total[$i];
                $bu->taxable = $total[$i];

                if (($taxed[$i]) == 'Yes') {
                    $bu->taxable = 1;
                } else {
                    $bu->taxable = 0;
                }
                $bu->save();
                $i++;
            }

            $p = ORM::for_table('sys_items')->where('id', $idBundled)->find_one();
            $p->cost_price = $totalCostPrice;
            $p->save();

            _msglog('s', $_L['Saved Successfully']);
            echo $bu->id();
        } else {
            echo 'Please select product for bundled product.';
        }

        break;

    case 'edit-manufacturer':
        $pId = $routes['2'];
        $items = ORM::for_table('sys_manufacturer_product')->where('id_item', $pId)->order_by_asc('id')->find_many();
        if ($items) {
            $product = ORM::for_table('sys_items')->find_one();
            if (!empty($product->currency_symbol)) {
                $currency = $product->currency_symbol;
            } else {
                $currency = $config['currency_code'];
            }
            $ui->assign('id_manufacturer_product', $pId);
            $ui->assign('currency', $currency);
            $ui->assign('is_bundle', (boolean)$product->is_bundle);
            $ui->assign('items', $items);
            $ui->assign('xheader', Asset::css(array('modal', 'dropzone/dropzone', 'redactor/redactor')));
            $ui->assign('xfooter', Asset::js(array('modal', 'dropzone/dropzone', 'redactor/redactor.min', 'numeric', 'jslib/manufacturer-edit')));

            $ui->assign('xjq', '
         $(\'.amount\').autoNumeric(\'init\', {

   
    dGroup: ' . $config['thousand_separator_placement'] . ',
    aPad: ' . $config['currency_decimal_digits'] . ',
    pSign: \'' . $config['currency_symbol_position'] . '\',
    aDec: \'' . $config['dec_point'] . '\',
    aSign: \'' . $currency . '\',
    aSep: \'' . $config['thousands_sep'] . '\',
    vMax: \'9999999999999999.00\',
                vMin: \'-9999999999999999.00\'

    });
 ');

            view('manufacturer-edit');

        } else {
            echo 'Product Not Found';
        }
        break;


    case 'edit-manufacturer-post':

        $idProduct = $_POST['id_manufacturer_product'];
        $i = 0;
        if (isset($_POST['id_manufacturer'])) {
            ORM::for_table('sys_manufacturer_product')->where('id_item', $idProduct)->delete_many();

            $id_manufacturer = $_POST['id_manufacturer'];
            $name_manufacturer = $_POST['item_name_manufacturer'];
            $cost_price_manufacturer = $_POST['cost_price_manufacturer'];

            foreach ($id_manufacturer as $id) {

                $bu = ORM::for_table('sys_manufacturer_product')->create();

                $bu->id_item = $idProduct;
                $bu->id_manufacturer = $id;
                $bu->name = $name_manufacturer[$i];
                $bu->cost_price = (float)str_replace($_POST['currency'], '', $cost_price_manufacturer[$i]);
                $bu->save();
                $i++;
            }

            _msglog('s', $_L['Saved Successfully']);
            echo $bu->id();
        } else {
            echo 'Please select manufacturer for product.';
        }

        break;

    case 'p-list':

        if (isset($routes[2])) {
            $typeProduct = $routes[2];
        } else {
            $typeProduct = "";
        }
        if (!has_access($user->roleid, 'products_n_services', 'view')) {
            permissionDenied();
        }


        $paginator = Paginator::bootstrap('sys_items', 'type', 'Product');
        $d = ORM::for_table('sys_items')->where('type', 'Product')->offset($paginator['startpoint'])->limit($paginator['limit'])->order_by_desc('id')->find_many();
        $ui->assign('d', $d);
        $ui->assign('type', 'Product');
        $ui->assign('typeProduct', $typeProduct);
        $ui->assign('paginator', $paginator);

        $ui->assign('xheader', Asset::css(array('modal', 'dropzone/dropzone', 'redactor/redactor')));
        $ui->assign('xfooter', Asset::js(array('clipboard.min', 'modal', 'dropzone/dropzone', 'redactor/redactor.min', 'numeric', 'js/ps_list')));

        view('ps-list');
        break;

    case 's-list':

        $paginator = Paginator::bootstrap('sys_items', 'type', 'Service');
        $d = ORM::for_table('sys_items')->where('type', 'Service')->offset($paginator['startpoint'])->limit($paginator['limit'])->order_by_desc('id')->find_many();
        $ui->assign('d', $d);
        $ui->assign('type', 'Service');
        $ui->assign('typeProduct', null);
        $ui->assign('paginator', $paginator);

        $ui->assign('xheader', Asset::css(array('modal', 'dropzone/dropzone', 'redactor/redactor')));
        $ui->assign('xfooter', Asset::js(array('clipboard.min', 'modal', 'dropzone/dropzone', 'redactor/redactor.min', 'numeric', 'js/ps_list')));


        view('ps-list');


        break;


    case 'edit-post':

        if (!has_access($user->roleid, 'products_n_services', 'edit')) {
            permissionDenied();
        }

        $msg = '';
        $id = _post('id');

        $name = _post('name');
        $sales_price = _post('sales_price', '0.00');
        $sales_price = Finance::amount_fix($sales_price);
        $item_number = _post('item_number');
        $description = _post('description');
        $package_type = _post('package_type');
        $type = _post('type');

        // other variables


        $inventory = _post('inventory');

        $inventory = Finance::amount_fix($inventory);

        if (!is_numeric($inventory)) {
            $inventory = '0';
        }

        $unit = _post('unit');

        $weight = _post('weight');

        $width = _post('width');
        $length = _post('length');
        $height = _post('height');

        if (!is_numeric($weight)) {
            $weight = '0.0000';
        }


        $msg = '';

        if ($name == '') {
            $msg .= 'Item Name is required <br>';
        }


        $sales_price = Finance::amount_fix($sales_price);

        if (!is_numeric($sales_price)) {
            $sales_price = '0.00';
        }

        $cost_price = _post('cost_price', '0.00');

        $cost_price = Finance::amount_fix($cost_price);

        if (!is_numeric($cost_price)) {
            $cost_price = '0.00';
        }

        if ($msg == '') {
            $d = ORM::for_table('sys_items')->find_one($id);
            if ($d) {

                if ($item_number != '' && $item_number != $d->item_number) {
                    $check = ORM::for_table('sys_items')->where('item_number', $item_number)->find_one();
                    if ($check) {
                        i_close('Item Number already exist.');
                    }
                }

                //save history
                $history = ORM::for_table('sys_pricing_history')->create();
                $history->id_item = $id;

                $history->user_made_id = $user['id'];
                $history->user_made_name = $user['username'];

                $history->cost_price_old = $d->cost_price;
                $history->cost_price_current = $cost_price;

                $history->selling_rice_old = $d->sales_price;
                $history->selling_rice_current = $sales_price;

                $history->inventory_old = $d->inventory;
                $history->inventory_current = $inventory;
                $history->date_change = date('Y-m-d H:i:s');
                $history->save();
                //end save

                $d->name = $name;
                $d->item_number = $item_number;
                $d->sales_price = $sales_price;
                $d->description = $description;
                $d->package_type = $package_type;
                $d->unit = $unit;
                $d->weight = $weight;
                $d->width = $width;
                $d->length = $length;
                $d->height = $height;
                $d->inventory = $inventory;
                if ($type == 'Product') {
                    $d->reorder_level = _post('reorder_level');
                }


                // other variables

                $d->image = _post('file_link');
                $d->cost_price = $cost_price;

                $d->save();

                echo $d->id();
            } else {
                echo 'Not Found';
            }


        } else {
            echo $msg;
        }


        break;
    case 'delete':

        if (!has_access($user->roleid, 'products_n_services', 'delete')) {
            permissionDenied();
        }

        $id = $routes['2'];
        if (APP_STAGE == 'Demo') {
            r2(U . 'accounts/list', 'e', 'Sorry! Deleting Account is disabled in the demo mode.');
        }
        $d = ORM::for_table('sys_accounts')->find_one($id);
        if ($d) {
            $d->delete();
            r2(U . 'accounts/list', 's', $_L['account_delete_successful']);
        }

        break;

    case 'edit-form':


        if (!has_access($user->roleid, 'products_n_services', 'edit')) {


            exit;

        }

        $id = $routes['2'];
        $d = ORM::for_table('sys_items')->find_one($id);
        $name = htmlentities($d->name);
        $reorder = '';
        if ($d) {
            $reorder = '<div class="form-group">
                        <label for="reorder_level" class="col-sm-2 control-label">Reorder Level</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" name="reorder_level" value="' . $d->reorder_level . '" id="reorder_level">
                          
                        </div>
                      </div>';
            $price = number_format(($d->sales_price), 2, $config['dec_point'], $config['thousands_sep']);
            $has_img = '';
            if ($d->image != '') {
                $has_img = '<hr>
<img src="' . APP_URL . '/storage/items/' . $d->image . '" class="img-responsive">
';
            }

            echo '
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h3>' . $_L['Edit'] . '</h3>
</div>
<div class="modal-body">

<div class="row">
<div class="col-md-8">
<form class="form-horizontal" role="form" id="edit_form" method="post" accept-charset="utf-8">
  <div class="form-group">
    <label for="name" class="col-sm-2 control-label">' . $_L['Name'] . '</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" value="' . $name . '" name="name" id="name">
    </div>
  </div>
  ' . $reorder . '
  <div class="form-group">
    <label for="rate" class="col-sm-2 control-label">Item Code(SKU)</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" name="item_number" value="' . $d->item_number . '" id="item_number">
      
    </div>
  </div>
  <div class="form-group">
    <label for="rate" class="col-sm-2 control-label">' . $_L['Sales Price'] . '</label>
    <div class="col-sm-8">
      <input type="text" id="sales_price" name="sales_price" class="form-control amount" autocomplete="off" data-a-sign="' . $config['currency_code'] . ' "  data-a-dec="' . $config['dec_point'] . '" data-a-sep="' . $config['thousands_sep'] . '" data-d-group="2" value="' . $d->sales_price . '">
    </div>
  </div>
  
    <div class="form-group">
    <label for="cost_price" class="col-sm-2 control-label">' . $_L['Cost Price'] . '</label>
    <div class="col-sm-8">
      <input type="text" id="cost_price" name="cost_price" class="form-control amount" autocomplete="off" data-a-sign="' . $config['currency_code'] . ' "  data-a-dec="' . $config['dec_point'] . '" data-a-sep="' . $config['thousands_sep'] . '" data-d-group="2" value="' . $d->cost_price . '">
    </div>
  </div>
  
  <div class="form-group">
    <label for="description" class="col-sm-2 control-label">' . $_L['Description'] . '</label>
    <div class="col-sm-10">
      <textarea id="description" name="description" class="form-control" rows="3">' . $d->description . '</textarea>
    </div>
  </div>
  
  <div class="form-group">
    <label for="inventory" class="col-sm-2 control-label">' . $_L['Inventory'] . '</label>
    <div class="col-sm-10">
      <input type="text" id="inventory" name="inventory" class="form-control amount" autocomplete="off" data-a-sign=""  data-a-dec="' . $config['dec_point'] . '" data-a-sep="' . $config['thousands_sep'] . '" data-d-group="2" value="' . $d->inventory . '">
    </div>
  </div>
  
  <div class="form-group">
    <label for="weight" class="col-sm-2 control-label">Weight</label>
    <div class="col-sm-10">
      <input type="text" id="weight" name="weight" class="form-control amount" autocomplete="off" value="' . $d->weight . '">
    </div>
  </div>
  
  <div class="form-group">
    <label for="length" class="col-sm-2 control-label">Length</label>
    <div class="col-sm-10">
      <input type="text" id="length" name="length" class="form-control amount" autocomplete="off" value="' . $d->length . '">
    </div>
  </div>
  
  <div class="form-group">
    <label for="height" class="col-sm-2 control-label">Height</label>
    <div class="col-sm-10">
      <input type="text" id="height" name="height" class="form-control amount" autocomplete="off" value="' . $d->height . '">
    </div>
  </div>
  
  <div class="form-group">
    <label for="width" class="col-sm-2 control-label">Width</label>
    <div class="col-sm-10">
      <input type="text" id="width" name="width" class="form-control amount" autocomplete="off" value="' . $d->width . '">
    </div>
  </div>
  
  <div class="form-group">
    <label for="package_type" class="col-sm-2 control-label">Package Type</label>
    <div class="col-sm-10">
      <textarea id="package_type" name="package_type" class="form-control" rows="3">' . $d->package_type . '</textarea>
    </div>
  </div>
  
  
  

  
  <input type="hidden" name="id" value="' . $d->id . '">
  <input type="hidden" name="file_link" id="file_link" value="' . $d->image . '">
</form>
</div>
<div class="col-md-4">
<form action="" class="dropzone" id="upload_container">

                        <div class="dz-message">
                            <h3> <i class="fa fa-cloud-upload"></i>  ' . $_L['Drop File Here'] . '</h3>
                            <br />
                            <span class="note">' . $_L['Click to Upload'] . '</span>
                        </div>
                        
                        <hr>
                        
                        

                    </form>
                 
                    ' . $has_img . '
                    
</div>

</div>
<div class="row">
<div class="col-md-12">
<hr>

  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon">' . $_L['URL'] . '</div>
      <input type="text" class="form-control" value="' . U . 'item/' . $d->id . '/"  onClick="this.setSelectionRange(0, this.value.length)" id="item_url" readonly>
      <div class="input-group-addon"><a href="javascript:void(0)" class="ib_btn_copy" data-clipboard-target="#item_url"><i class="fa fa-clipboard"></i> ' . $_L['Copy'] . '</a></div>
    </div>
  </div>
  
    <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon">' . $_L['URL'] . ' : ' . $_L['Add to Cart'] . '</div>
      <input type="text" class="form-control" value="' . U . 'cart/add/' . $d->id . '/"  onClick="this.setSelectionRange(0, this.value.length)" id="add_to_cart_url" readonly>
      <div class="input-group-addon"><a href="javascript:void(0)" class="ib_btn_copy" data-clipboard-target="#add_to_cart_url"><i class="fa fa-clipboard"></i> ' . $_L['Copy'] . '</a></div>
    </div>
  </div>

</div>
</div>

</div>
<div class="modal-footer">

	<button type="button" data-dismiss="modal" class="btn">' . $_L['Close'] . '</button>
	<button id="update" class="btn btn-primary">' . $_L['Update'] . '</button>
</div>';
        } else {
            echo 'not found';
        }


        break;

    case 'history-form':
        $id = $routes['2'];
        $history = ORM::for_table('sys_pricing_history')->where('id_item', $id)->order_by_desc('id')->find_array();

        if (count($history) > 0) {
            $content = '<tbody>';
            $sort = count($history);
            foreach ($history as $h) {
                $content .= '<tr role="row" class="even">
                                <td class="text-center">' . $sort . '</td>
                                <td class="text-center">' . $h['ordernum'] . '</td>
                                <td class="text-center">' . $config['currency_code'] . ' ' . $h['cost_price_old'] . '</td>
                                <td class="text-center">' . $config['currency_code'] . ' ' . $h['cost_price_current'] . '</td>
                                <td class="text-center">' . $config['currency_code'] . ' ' . $h['selling_rice_old'] . '</td>
                                <td class="text-center">' . $config['currency_code'] . ' ' . $h['selling_rice_current'] . '</td>
                                <td class="text-center">' . $h['inventory_old'] . '</td>
                                <td class="text-center">' . $h['inventory_current'] . '</td>
                                <td class="text-center">' . $h['user_made_name'] . '</td>
                                <td class="text-center" style="width: 140px !important;">' . $h['date_change'] . '</td>
                            </tr>';
                $sort--;
            }
            $content .= '</tbody>';
            echo '
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">
            History
        </h4>
    </div>
    <table class="table table-bordered table-hover display" id="ib_dt">
                                            <thead>
                                            <tr class="heading">
                                                <th class="text-center">#</th>
                                                <th class="text-center">Order #</th>
                                                <th class="text-center">Cost Price Old</th>
                                                <th class="text-center">Cost Price Current</th>
                                                <th class="text-center">Selling Price old</th>
                                                <th class="text-center">Selling Price Current</th>
                                                <th class="text-center">Inventory Old</th>
                                                <th class="text-center">Inventory Current</th>
                                                <th class="text-center">User Changed</th>
                                                <th class="text-center">Date Changed</th>
                                            </tr>
                                            </thead>
                                            ' . $content . '
                                        </table>
	<button type="button" data-dismiss="modal" class="btn" style="float: right; margin: 10px;">' . $_L['Close'] . '</button>
	';
        } else {
            $content = '
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">
                    History
                </h4>
            </div>
            ';
            $content .= '<div style="text-align: center;margin-top: 45px;"> Not found </div>';
            $content .= '<button type="button" data-dismiss="modal" class="btn" style="float: right; margin: 10px;">' . $_L['Close'] . '</button>';
            echo $content;
        }

        break;

    case 'update-inventory-form':
        $id = $routes['2'];
        $d = ORM::for_table('sys_items')->find_one($id);

        echo '
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h3>Update Inventory</h3>
</div>
<div class="modal-body">

<div class="row">
<div class="col-md-12">
<form class="form-horizontal" role="form" id="edit_form_inventory" method="post">

  <div class="form-group">
      <div class="col-sm-8">
        <label for="inventory" class="col-sm-2 control-label" style="width: 30%;">Current Inventory</label>
        <div class="col-sm-4">
          <input type="text" id="current_inventory" name="current_inventory" readonly="true" class="form-control amount" autocomplete="off" data-a-sign=""  data-a-dec="' . $config['dec_point'] . '" data-a-sep="' . $config['thousands_sep'] . '" data-d-group="2" value="' . $d->inventory . '">
        </div>
       </div>
  </div>
  
  <div class="form-group" style="margin-left: 19.5%;">
      <div class="col-sm-8">
          <button type="button" id="btn-increase" class="btn btn-update-inventory"><i class="fa fa-plus"></i></button>
          <input type="number" min="1" max="99" id="update-inventory" name="update-inventory" value="1" style="text-align: center; margin: 10px;height: 32px;">
          <button type="button" id="btn-decrease" class="btn btn-update-inventory"><i class="fa fa-minus"></i></button>
      </div>
  </div>
   
   <div class="form-group">
      <div class="col-sm-8">
        <label for="inventory" class="col-sm-2 control-label" style="width: 30%;">New Inventory</label>
        <div class="col-sm-4">
          <input type="text" id="new_inventory" name="new_inventory" class="form-control amount" autocomplete="off" data-a-sign=""  data-a-dec="' . $config['dec_point'] . '" data-a-sep="' . $config['thousands_sep'] . '" data-d-group="2" value="">
        </div>
       </div>
  </div>
  
  <input type="hidden" name="id-update-inventory" value="' . $d->id . '">
</form>
	<button type="button" data-dismiss="modal" class="btn" style="float: right; margin: 10px;">Cancel</button>
	<button type="button" id="save-inventory" class="btn" style="float: right; margin: 10px;">Save</button>
';

        break;

    case 'edit-post-inventory':

        $type = $routes[2];
        $id = _post('id-update-inventory');
        $d = ORM::for_table('sys_items')->find_one($id);

        $inventory = _post('new_inventory');
        $inventoryUpdate = Finance::amount_fix($inventory);

        //save history
        $history = ORM::for_table('sys_pricing_history')->create();
        $history->id_item = $id;

        $history->user_made_id = $user['id'];
        $history->user_made_name = $user['username'];

        $history->cost_price_old = $d->cost_price;
        $history->cost_price_current = $d->cost_price;

        $history->selling_rice_old = $d->sales_price;
        $history->selling_rice_current = $d->sales_price;

        $history->inventory_old = $d->inventory;
        $history->inventory_current = $inventoryUpdate;
        $history->date_change = date('Y-m-d H:i:s');
        $history->save();
        //end save

        $d->inventory = $inventoryUpdate;
        $d->save();

        echo $d->id;

        break;

    case 'json_get':

        if (!has_access($user->roleid, 'products_n_services', 'view')) {
            permissionDenied();
        }

        header('Content-Type: application/json');

        $pid = route(2);

        $d = ORM::for_table('sys_items')->find_one($pid);

        if ($d) {

            $i = array();
            $i['sales_price'] = $d->sales_price;

            echo json_encode($i);

        }


        break;

    case 'cats':


        break;


    case 'upload':

        if (!has_access($user->roleid, 'products_n_services', 'create')) {
            permissionDenied();
        }

        if (APP_STAGE == 'Demo') {
            exit;
        }

        $uploader = new Uploader();
        $uploader->setDir('storage/items/');
        $uploader->sameName(false);
        $uploader->setExtensions(array('jpg', 'jpeg', 'png', 'gif'));  //allowed extensions list//
//        $uploader->allowAllFormats();  //allowed extensions list//
        if ($uploader->uploadFile('file')) {   //txtFile is the filebrowse element name //
            $uploaded = $uploader->getUploadName(); //get uploaded file name, renames on upload//

            $file = $uploaded;
            $msg = $_L['Uploaded Successfully'];
            $success = 'Yes';

            // create thumb

            $image = new Img();

            // indicate a source image (a GIF, PNG or JPEG file)
            $image->source_path = 'storage/items/' . $file;

            // indicate a target image
            // note that there's no extra property to set in order to specify the target
            // image's type -simply by writing '.jpg' as extension will instruct the script
            // to create a 'jpg' file
            $image->target_path = 'storage/items/thumb' . $file;

            // since in this example we're going to have a jpeg file, let's set the output
            // image's quality
            $image->jpeg_quality = 100;

            // some additional properties that can be set
            // read about them in the documentation
            $image->preserve_aspect_ratio = true;
            $image->enlarge_smaller_images = true;
            $image->preserve_time = true;

            // resize the image to exactly 100x100 pixels by using the "crop from center" method
            // (read more in the overview section or in the documentation)
            //  and if there is an error, check what the error is about
            if (!$image->resize(100, 100, ZEBRA_IMAGE_CROP_CENTER)) {


                // if no errors
            } else {

                // echo 'Success!';

            }

            //


        } else {//upload failed
            $file = '';
            $msg = $uploader->getMessage();
            $success = 'No';
        }

        $a = array(
            'success' => $success,
            'msg' => $msg,
            'file' => $file
        );

        header('Content-Type: application/json');

        echo json_encode($a);


        break;


    default:
        echo 'action not defined';
}