<?php
$ms = ORM::for_table('sys_orders_marketplace_settings')->where('options','Shopify')->find_many();

try {
    foreach ($ms as $m) {
        if ($m) {
            $username = $m->api_key;
            $password = $m->api_secret;
            $url = $m->url;
//            $url = $url.'/admin/api/'.date('Y-m').'/orders.json';
            $url = $url . '/admin/api/2019-04/orders.json';
            $listOrders = MarketplaceApi::getDataShopify($url, $username, $password);

            if (empty($listOrders) || isset($listOrders['errors'])) {
                $log = ORM::for_table('sys_schedulelogs')->create();
                $log->date = date('Y-m-d');
                $log->logs = $listOrders['errors'];
                $log->save();
                continue;
            }
            if (isset($listOrders['orders']) && count($listOrders['orders']) > 0) {
                foreach ($listOrders['orders'] as $order) {
                    try {
                        $o = ORM::for_table('sys_orders_marketplace')->where('id_order_marketplace', $order['id'])->find_one();
                        if (empty($o)) {
                            $o = ORM::for_table('sys_orders_marketplace')->create();
                        }
                        $o->id_order_marketplace = $order['id'];
                        $o->email = $order['email'];
                        $o->number = $order['number'];
                        $o->note = $order['note'];
                        $datetime_created_at = new DateTime($order['created_at']);
                        $o->created_at = $datetime_created_at->format('Y-m-d H:i:s');
                        $datetime_updated_at = new DateTime($order['updated_at']);
                        $o->updated_at = $datetime_updated_at->format('Y-m-d H:i:s');
                        $o->total_price = $order['total_price'];
                        $o->subtotal_price = $order['subtotal_price'];
                        $o->total_weight = $order['total_weight'];
                        $o->total_tax = $order['total_tax'];
                        $o->currency = $order['currency'];
                        $o->financial_status = $order['financial_status'];
                        $o->confirmed = $order['confirmed'];
                        $o->total_discounts = $order['total_discounts'];
                        $o->order_number = $order['order_number'];
                        $o->fulfillment_status = $order['fulfillment_status'];
                        $o->line_items = json_encode($order['line_items']);
                        $o->billing_address = isset($order['billing_address']) ? json_encode($order['billing_address']) : '';
                        $o->shipping_address = isset($order['shipping_address']) ? json_encode($order['shipping_address']) : '';
                        $o->customer = isset($order['customer']) ? json_encode($order['customer']) : '';
                        $o->marketplace = 'Shopify';
                        $o->marketplace_settings_id = $m->id;
                        $o->save();
                    } catch (\Exception $e) {
                        $log = ORM::for_table('sys_schedulelogs')->create();
                        $log->date = date('Y-m-d');
                        $log->logs = $e->getMessage();
                        $log->save();
                        continue;
                    }
                }
            }
        }
    }
    $log = ORM::for_table('sys_schedulelogs')->create();
    $log->date = date('Y-m-d');
    $log->logs = 'Sync order Shopify successful.';
    $log->save();
}catch (\Exception $e){
    $log = ORM::for_table('sys_schedulelogs')->create();
    $log->date = date('Y-m-d');
    $log->logs = $e->getMessage();
    $log->save();
}