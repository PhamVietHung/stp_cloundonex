<?php
/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
|
*/
_auth();
$ui->assign('_application_menu', 'warehouse');
$ui->assign('_title', 'Warehouse Inbound'.'- '. $config['CompanyName']);
$ui->assign('_st', 'Warehouse Inbound');
$action = $routes['1'];
$user = User::_info();
$ui->assign('user', $user);

Event::trigger('warehouse');

switch ($action) {

    case 'list':

        $paginator = array();
        $mode_css = '';
        $mode_js = '';
        $view_type = 'filter';
        $mode_css = Asset::css('footable/css/footable.core.min');
        $mode_js = Asset::js(array(
            'numeric',
            'footable/js/footable.all.min',
            'contacts/mode_search'
        ));

        $f = ORM::for_table('sys_purchases');

        $d = $f->order_by_desc('id')->find_many();
        $paginator['contents'] = '';

        $ui->assign('xheader', $mode_css);
        $ui->assign('xfooter', $mode_js);
        $ui->assign('view_type', $view_type);
        $ui->assign('d', $d);
        $ui->assign('paginator', $paginator);


        view('warehouse_list');


        break;

    case 'view':

        $id = $routes['2'];
        $d = ORM::for_table('sys_purchases')->find_one($id);
        if ($d) {

            // find all activity for this user

            $items = ORM::for_table('sys_purchaseitems')->where('invoiceid', $id)->left_outer_join('sys_warehouse_inbound','sys_purchaseitems.id = sys_warehouse_inbound.id_purchase_item')
                ->order_by_asc('sys_purchaseitems.id')
                ->select_many('sys_purchaseitems.id','sys_purchaseitems.itemcode','sys_purchaseitems.description','sys_purchaseitems.qty','sys_warehouse_inbound.qty_received','sys_warehouse_inbound.internal_memo','sys_warehouse_inbound.status','sys_warehouse_inbound.date_time','sys_warehouse_inbound.username')
                ->find_many();
            $ui->assign('items', $items);
            $ui->assign('U', U);


            $a = ORM::for_table('crm_accounts')->find_one($d['userid']);
            $ui->assign('a', $a);
            $ui->assign('d', $d);
            $i_credit = $d['credit'];
            $i_due = '0.00';
            $i_total = $d['total'];
            if ($d['credit'] != '0.00') {
                $i_due = $i_total - $i_credit;
            }
            else {
                $i_due = $d['total'];
            }

            $i_due = number_format($i_due, 2, $config['dec_point'], $config['thousands_sep']);
            $ui->assign('i_due', $i_due);
            $cf = ORM::for_table('crm_customfields')->where('showinvoice', 'Yes')->order_by_asc('id')->find_many();
            $ui->assign('cf', $cf);
            $ui->assign('xheader', Asset::css(array(
                's2/css/select2.min',
                'modal','css/ribbon'
            )));
            $ui->assign('xfooter', Asset::js(array(
                's2/js/select2.min',
                's2/js/i18n/' . lan() ,
                'dp/dist/datepicker.min',
                'dp/i18n/' . $config['language'],
                'numeric',
                'modal',
                'sn/summernote.min',
                'dropzone/dropzone'
            )));
            $x_html = '';
            Event::trigger('view_invoice');
            $ui->assign('x_html', $x_html);
            $ui->assign('xjq', ' $(\'.amount\').autoNumeric(\'init\', {

   
    dGroup: ' . $config['thousand_separator_placement'] . ',
    aPad: ' . $config['currency_decimal_digits'] . ',
    pSign: \'' . $config['currency_symbol_position'] . '\',
    aDec: \'' . $config['dec_point'] . '\',
    aSep: \'' . $config['thousands_sep'] . '\',
    vMax: \'9999999999999999.00\',
                vMin: \'-9999999999999999.00\'

    });');

            $currency_rate = 1;
            if($a->cid != '' || $a->cid != 0){
                $company = Company::find($a->cid);
            }
            else{
                $company = false;
            }

            view('warehouse_view', ['currencies' => Currency::all() , 'currency_rate' => $currency_rate, 'company' => $company]);
        }
        else {
            r2(U . 'customers/list', 'e', $_L['Account_Not_Found']);
        }


        break;

    case 'save':

        $purchaseId = _post('iid');
        $purchaseItemIds = _post('item_ids');

        $item_code = _post('item_code');
        $item_description = _post('item_description');

        $qty_received = _post('qty_received');
        $internal_memo = _post('internal_memo');
        $status = _post('status');
        $date_time = date('Y-m-d H:i:s');
        $username = $user['username'];

        foreach ($purchaseItemIds as $key => $itemId) {
            $w = ORM::for_table('sys_warehouse_inbound')->where('id_purchase_item', $itemId)->find_one();
            if(!$w){
                $w = ORM::for_table('sys_warehouse_inbound')->create();

            }
            $w->id_purchase = $purchaseId;
            $w->id_purchase_item = $itemId;
            $w->internal_memo = $internal_memo[$key];
            $w->qty_received = (int)$qty_received[$key];
            $w->status = $status[$key];
            $w->date_time = $date_time;
            $w->username = $username;
            $w->save();

            //update inventory
            $item = ORM::for_table('sys_items')->where('item_number', $item_code[$key])->find_one();
            if(!$item){
                $item = ORM::for_table('sys_items')->where('name', $item_description[$key])->find_one();
            }
            if($item){

                //save history
                $history = ORM::for_table('sys_pricing_history')->create();
                $history->id_item = $item->id;

                $history->user_made_id = $user['id'];
                $history->user_made_name = $user['username'];

                $history->cost_price_old = $item->cost_price;
                $history->cost_price_current = $item->cost_price;

                $history->selling_rice_old = $item->sales_price;
                $history->selling_rice_current = $item->sales_price;

                $history->inventory_old = $item->inventory;
                $history->inventory_current = (int)$item->inventory + (int)$qty_received[$key];
                $history->date_change = date('Y-m-d H:i:s');
                $history->save();
                //end save

                $item->inventory = (int)$item->inventory + (int)$qty_received[$key];
                $item->save();
            }
        }
        echo $purchaseId;


        break;


    case 'barcode':


        $item_code = route(2);
        $item_name = route(3);
        $item = ORM::for_table('sys_items')->where('item_number', $item_code)->find_one();
        if(!$item){
            $item = ORM::for_table('sys_items')->where('name', base64_decode($item_name))->find_one();
        }
        if($item){

            $name = substr($item->name, 0, 10);
            $sales_price = ib_money_format($item->sales_price,$config);
            $item_number = $item->item_number;

            $html = '
<table style="width:100%">
  <tr>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
  </tr>
  <tr>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
  </tr>
  <tr>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
  </tr>
  <tr>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
  </tr>
  <tr>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
  </tr>
  <tr>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
  </tr>
  <tr>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
  </tr>
  <tr>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
    <td align="center"><div style="text-align: center; font-family: ocrb;">'.$item_number.'<br><barcode code="'.$item_number.'" type="C39" height="0.5" seize="0.8" /><br> &nbsp;</div></td>
  </tr>
  
</table>
';

//            $mpdf=new mPDF('','A4','','',20,15,25,25,10,10);

            $mpdf = new \Mpdf\Mpdf();

            $mpdf->WriteHTML($html);

            $mpdf->Output();

        }







        break;

    default:
        echo 'action not defined';
}