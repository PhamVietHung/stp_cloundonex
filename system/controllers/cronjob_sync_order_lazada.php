<?php
$ms = ORM::for_table('sys_orders_marketplace_settings')->where('options', 'Lazada')->find_many();
try {
    foreach ($ms as $m) {
        if ($m) {
            $r = ORM::for_table('webhook_lazada_token')
                ->where('marketplace_settings_id', $m->id)
                ->find_one();

            if (empty($r)) {
                $log = ORM::for_table('sys_schedulelogs')->create();
                $log->date = date('Y-m-d H:i:s');
                $log->logs = 'Please get access token first.';
                $log->save();
                continue;
            }

            $accessToken = $r->access_token;

            $username = $m->api_key;
            $password = $m->api_secret;
            $url = $m->url;
            $createdAfter = $m->orders_created_after;
            $url = MarketplaceApi::getUrlByCountry($m->country);
            $createdAfter = date('c', strtotime($createdAfter));
            $statuses = ['pending', 'ready_to_ship', 'shipped', 'delivered'];
            foreach ($statuses as $status) {
                $listOrders = MarketplaceApi::getDataOrderLazada($url, $username, $password, $accessToken, $createdAfter, $status);
                if (empty($listOrders) || isset($listOrders['errors'])) {
                    $log = ORM::for_table('sys_schedulelogs')->create();
                    $log->date = date('Y-m-d H:i:s');
                    $log->logs = $listOrders['errors'];
                    $log->save();
                    continue;
                }
                if (isset($listOrders['data']['orders']) && $listOrders['data']['count'] > 0) {
                    foreach ($listOrders['data']['orders'] as $order) {
                        try {
                            $o = ORM::for_table('sys_orders_marketplace_lazada')
                                ->where('id_order_marketplace', $order['order_id'])
                                ->find_one();
                            if (empty($o)) {
                                $o = ORM::for_table('sys_orders_marketplace_lazada')->create();
                            }
                            $o->id_order_marketplace = $order['order_id'];
                            $o->order_number = $order['order_number'];
                            $o->voucher = $order['voucher'];
                            $o->voucher_platform = $order['voucher_platform'];
                            $o->voucher_seller = $order['voucher_seller'];
                            $o->gift_option = $order['gift_option'];
                            $o->customer_last_name = $order['customer_last_name'];
                            $o->promised_shipping_times = $order['promised_shipping_times'];
                            $o->price = (float)$order['price'];
                            $o->national_registration_number = $order['national_registration_number'];
                            $o->payment_method = $order['payment_method'];
                            $o->customer_first_name = $order['customer_first_name'];
                            $o->shipping_fee = $order['shipping_fee'];
                            $o->items_count = $order['items_count'];
                            $o->delivery_info = $order['delivery_info'];
                            $o->statuses = isset($order['statuses'][0]) ? $order['statuses'][0] : '';
                            $o->address_billing = isset($order['address_billing']) ? json_encode($order['address_billing']) : '';
                            $o->address_shipping = isset($order['address_shipping']) ? json_encode($order['address_shipping']) : '';
                            $o->extra_attributes = $order['extra_attributes'];
                            $o->gift_message = $order['gift_message'];
                            $o->remarks = $order['remarks'];
                            $datetime_created_at = new DateTime($order['created_at']);
                            $o->created_at = $datetime_created_at->format('Y-m-d H:i:s');
                            $datetime_updated_at = new DateTime($order['updated_at']);
                            $o->updated_at = $datetime_updated_at->format('Y-m-d H:i:s');
                            $o->marketplace = 'Lazada';
                            $o->marketplace_settings_id = $m->id;

                            $listItems = MarketplaceApi::getItemDataLazada($url, $username, $password, $accessToken, $order['order_id']);

                            if (isset($listItems['data']) && count($listItems['data']) > 0) {
                                $items = json_encode($listItems['data']);
                                $o->items = $items;
                            }
                            $o->save();
                        } catch (\Exception $e) {
                            $log = ORM::for_table('sys_schedulelogs')->create();
                            $log->date = date('Y-m-d H:i:s');
                            $log->logs = $e->getMessage();
                            $log->save();
                            continue;
                        }
                    }
                }
            }
        }
    }
    $log = ORM::for_table('sys_schedulelogs')->create();
    $log->date = date('Y-m-d H:i:s');
    $log->logs = 'Sync order Lazada successful.';
    $log->save();
} catch (\Exception $e) {
    $log = ORM::for_table('sys_schedulelogs')->create();
    $log->date = date('Y-m-d H:i:s');
    $log->logs = $e->getMessage();
    $log->save();
}