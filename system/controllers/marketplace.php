<?php
/*
|--------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------
|
*/
_auth();
$ui->assign('_application_menu', 'orders');
$ui->assign('_title', 'Marketplace Orders' . '- ' . $config['CompanyName']);
$ui->assign('_st', 'Marketplace Orders');
$action = $routes['1'];
$user = User::_info();
$ui->assign('user', $user);

Event::trigger('marketplace');

switch ($action) {

    case 'list':
        $type = isset($routes['2']) ? $routes['2'] : null;
        $ui->assign('type', $type);
        $mar = ORM::for_table('sys_orders_marketplace_settings')->order_by_asc('id')->find_many();

        $ui->assign('jsvar', '
_L[\'are_you_sure\'] = \'' . $_L['are_you_sure'] . '\';
 ');

        $mode_css = '';
        $mode_js = '';

        $mode_css = Asset::css('footable/css/footable.core.min');

        $mode_js = Asset::js(array('footable/js/footable.all.min', 'numeric', 'orders/marketplace'));

        $ui->assign('mar', $mar);
        $ui->assign('xheader', $mode_css);
        $ui->assign('xfooter', $mode_js);

        $xjq = '

    $(\'.amount\').autoNumeric(\'init\', {

    aSign: \'' . $config['currency_code'] . ' \',
    dGroup: ' . $config['thousand_separator_placement'] . ',
    aPad: ' . $config['currency_decimal_digits'] . ',
    pSign: \'' . $config['currency_symbol_position'] . '\',
    aDec: \'' . $config['dec_point'] . '\',
    aSep: \'' . $config['thousands_sep'] . '\',
    vMax: \'9999999999999999.00\',
                vMin: \'-9999999999999999.00\'

    });

 ';
        if ($type == 'sync') {
            view('marketplace_list_sync');
        } else {
            view('marketplace_list');
        }
        break;

    case 'add-new':
        $ui->assign('idate', date('Y-m-d'));
        $ui->assign('xheader', Asset::css(array('s2/css/select2.min', 'modal', 'dp/dist/datepicker.min', 'redactor/redactor')));
        $ui->assign('xfooter', Asset::js(array('redactor/redactor.min', 's2/js/select2.min', 's2/js/i18n/' . lan(), 'dp/dist/datepicker.min', 'dp/i18n/' . $config['language'], 'numeric', 'modal', 'quotes')));
        view('marketplace_add');


        break;

    case 'add-post':

        $url = _post('url');
        $api_key = _post('api_key');
        $api_secret = _post('api_secret');
        $options = _post('options');
        $title = _post('title');
        if ($options == "Lazada") {
            $country = _post('country');
            $ordersCreatedAfter = _post('orders_created_after');
        } else {
            $country = '';
            $ordersCreatedAfter = null;
        }

        $m = ORM::for_table('sys_orders_marketplace_settings')->create();

        $m->url = $url;
        $m->api_key = $api_key;
        $m->api_secret = $api_secret;
        $m->options = $options;
        $m->country = $country;
        $m->title = $title;
        $m->date_added = date('Y-m-d H:i:s');
        $m->orders_created_after = $ordersCreatedAfter;
        $m->save();
        r2(U . 'marketplace/list', 's', 'Created Successfully');

        break;


    case 'edit':

        $id = $routes['2'];
        $mar = ORM::for_table('sys_orders_marketplace_settings')->find_one($id);

        if ($mar) {
            $ui->assign('idate', date('Y-m-d'));
            $ui->assign('xheader', Asset::css(array('s2/css/select2.min', 'modal', 'dp/dist/datepicker.min', 'redactor/redactor')));
            $ui->assign('xfooter', Asset::js(array('redactor/redactor.min', 's2/js/select2.min', 's2/js/i18n/' . lan(), 'dp/dist/datepicker.min', 'dp/i18n/' . $config['language'], 'numeric', 'modal', 'quotes')));
            $ui->assign('mar', $mar);
            view('marketplace_edit');
        }


        break;

    case 'edit-post':

        $id = _post('id');
        $url = _post('url');
        $api_key = _post('api_key');
        $api_secret = _post('api_secret');
        $options = _post('options');
        $title = _post('title');

        if ($options == "Lazada") {
            $country = _post('country');
            $ordersCreatedAfter = _post('orders_created_after');
        } else {
            $country = '';
            $ordersCreatedAfter = null;
        }

        $m = ORM::for_table('sys_orders_marketplace_settings')->find_one($id);
        if ($m) {
            if ($m->api_key != $api_key || $m->api_secret != $api_secret) {
                $r = ORM::for_table('webhook_lazada_token')
                    ->where('marketplace_settings_id', $id)
                    ->find_one();

                if ($r) {
                    $r->delete();
                }
            }

            $m->url = $url;
            $m->api_key = $api_key;
            $m->api_secret = $api_secret;
            $m->options = $options;
            $m->country = $country;
            $m->title = $title;
            $m->date_added = date('Y-m-d H:i:s');
            $m->orders_created_after = $ordersCreatedAfter;
            $m->save();
        }
        r2(U . 'marketplace/list', 's', 'Updated Successfully');

        break;

    case 'sync':
        $id = $routes['2'];
        $type = $routes['3'];
        $option = isset($routes['4']) ? $routes['4'] : '';
        if ($option == 'sync') {
            $redirect = U . 'marketplace/list/sync';
        } else {
            $redirect = U . 'marketplace/list';
        }
        $m = ORM::for_table('sys_orders_marketplace_settings')->find_one($id);
        if ($m) {
            if ($m->options == 'Lazada') {
                $r = ORM::for_table('webhook_lazada_token')
                    ->where('marketplace_settings_id', $id)
                    ->find_one();

                if (empty($r)) {
                    r2($redirect, 'e', 'Please get access token first.');
                }

                $accessToken = $r->access_token;
            }
            $username = $m->api_key;
            $password = $m->api_secret;
            $url = $m->url;
            $createdAfter = $m->orders_created_after;
//            $url = $url.'/admin/api/'.date('Y-m').'/orders.json';
            if ($m->options == 'Lazada') {
                $url = MarketplaceApi::getUrlByCountry($m->country);

                $createdAfter = date('c', strtotime($createdAfter));
                $listOrders = MarketplaceApi::getDataOrderLazada($url, $username, $password, $accessToken, $createdAfter);
            } else {
                $url = $url . '/admin/api/2020-01/orders.json';
                $listOrders = MarketplaceApi::getDataShopify($url, $username, $password);
            }

            if (empty($listOrders) || isset($listOrders['errors'])) {
                r2($redirect, 'e', $listOrders['errors']);
                return false;
            }

            if ($m->options == 'Lazada') {
                if (isset($listOrders['data']['orders']) && $listOrders['data']['count'] > 0) {
                    foreach ($listOrders['data']['orders'] as $order) {
                        try {
                            $o = ORM::for_table('sys_orders_marketplace_lazada')
                                ->where('id_order_marketplace', $order['order_id'])
                                ->find_one();
                            if (empty($o)) {
                                $o = ORM::for_table('sys_orders_marketplace_lazada')->create();
                            }
                            $o->id_order_marketplace = $order['order_id'];
                            $o->order_number = $order['order_number'];
                            $o->voucher = $order['voucher'];
                            $o->voucher_platform = $order['voucher_platform'];
                            $o->voucher_seller = $order['voucher_seller'];
                            $o->gift_option = $order['gift_option'];
                            $o->customer_last_name = $order['customer_last_name'];
                            $o->promised_shipping_times = $order['promised_shipping_times'];
                            $o->price = (float)$order['price'];
                            $o->national_registration_number = $order['national_registration_number'];
                            $o->payment_method = $order['payment_method'];
                            $o->customer_first_name = $order['customer_first_name'];
                            $o->shipping_fee = $order['shipping_fee'];
                            $o->items_count = $order['items_count'];
                            $o->delivery_info = $order['delivery_info'];
                            $o->statuses = isset($order['statuses'][0]) ? $order['statuses'][0] : '';
                            $o->address_billing = isset($order['address_billing']) ? json_encode($order['address_billing']) : '';
                            $o->address_shipping = isset($order['address_shipping']) ? json_encode($order['address_shipping']) : '';
                            $o->extra_attributes = $order['extra_attributes'];
                            $o->gift_message = $order['gift_message'];
                            $o->remarks = $order['remarks'];
                            $datetime_created_at = new DateTime($order['created_at']);
                            $o->created_at = $datetime_created_at->format('Y-m-d H:i:s');
                            $datetime_updated_at = new DateTime($order['updated_at']);
                            $o->updated_at = $datetime_updated_at->format('Y-m-d H:i:s');
                            $o->marketplace = $type;
                            $o->marketplace_settings_id = $id;

                            $listItems = MarketplaceApi::getItemDataLazada($url, $username, $password, $accessToken, $order['order_id']);

                            if (isset($listItems['data']) && count($listItems['data']) > 0) {
                                $items = json_encode($listItems['data']);
                                $o->items = $items;
                            }

                            $o->save();
                        } catch (\Exception $e) {
                            r2($redirect, 'e', $e->getMessage());
                            return false;
                        }
                    }
                }
            } else {
                if (isset($listOrders['orders']) && count($listOrders['orders']) > 0) {
                    foreach ($listOrders['orders'] as $order) {
                        try {
                            $o = ORM::for_table('sys_orders_marketplace')->where('id_order_marketplace', $order['id'])->find_one();
                            if (empty($o)) {
                                $o = ORM::for_table('sys_orders_marketplace')->create();
                            }
                            $o->id_order_marketplace = $order['id'];
                            $o->email = $order['email'];
                            $o->number = $order['number'];
                            $o->note = $order['note'];
                            $datetime_created_at = new DateTime($order['created_at']);
                            $o->created_at = $datetime_created_at->format('Y-m-d H:i:s');
                            $datetime_updated_at = new DateTime($order['updated_at']);
                            $o->updated_at = $datetime_updated_at->format('Y-m-d H:i:s');
                            $o->total_price = $order['total_price'];
                            $o->subtotal_price = $order['subtotal_price'];
                            $o->total_weight = $order['total_weight'];
                            $o->total_tax = $order['total_tax'];
                            $o->currency = $order['currency'];
                            $o->financial_status = $order['financial_status'];
                            $o->confirmed = $order['confirmed'];
                            $o->total_discounts = $order['total_discounts'];
                            $o->order_number = $order['order_number'];
                            $o->fulfillment_status = $order['fulfillment_status'];
                            $o->line_items = json_encode($order['line_items']);
                            $o->billing_address = isset($order['billing_address']) ? json_encode($order['billing_address']) : '';
                            $o->shipping_address = isset($order['shipping_address']) ? json_encode($order['shipping_address']) : '';
                            $o->customer = isset($order['customer']) ? json_encode($order['customer']) : '';
                            $o->shipping_lines = isset($order['shipping_lines']) ? json_encode($order['shipping_lines']) : '';
                            $o->marketplace = $type;
                            $o->marketplace_settings_id = $id;
                            $o->save();
                        } catch (\Exception $e) {
                            r2($redirect, 'e', $e->getMessage());
                            return false;
                        }
                    }
                }
            }
            r2($redirect, 's', 'Sync order ' . $type . ' successfully');
            return true;

        }

        break;

    case 'get-token':
        $id = $routes['2'];
        $option = isset($routes['3']) ? $routes['3'] : '';
        if ($option == 'sync') {
            $redirect = U . 'marketplace/list/sync';
        } else {
            $redirect = U . 'marketplace/list';
        }
        $m = ORM::for_table('sys_orders_marketplace_settings')->find_one($id);

        if ($m) {
            $r = ORM::for_table('webhook_lazada_token')
                ->where('marketplace_settings_id', $id)
                ->find_one();

            if ($r) {
                $tokenExpiresTime = strtotime($r->expires_in);
                $refreshTokenExpiresTime = strtotime($r->refresh_expires_in);
                $nowTime = time();

                if ($nowTime < $tokenExpiresTime) {
                    r2($redirect, 's', 'Current token isn\'t expired yet.');
                } else if ($nowTime >= $tokenExpiresTime && $nowTime < $refreshTokenExpiresTime) {
                    r2(U . 'lazadaTokenHandler/refresh/' . $r->id);
                }
            }
            $clientId = $m->api_key;
            $redirectUri = $m->url;
            $_SESSION['lazada_get_token_id'] = $id;
            $url = 'https://auth.lazada.com/oauth/authorize?response_type=code&redirect_uri=' . $redirectUri . '&client_id=' . $clientId;
            header('location:' . $url);
        }

        break;

    case 'shopify-order-list':
        $status = isset($routes['2']) ? $routes['2'] : '';
        $orders = ORM::for_table('sys_orders_marketplace')
            ->order_by_asc('id')
            ->find_many();

        $ui->assign('jsvar', '
_L[\'are_you_sure\'] = \'' . $_L['are_you_sure'] . '\';
 ');

        $mode_css = '';
        $mode_js = '';

        $mode_js = Asset::js(array('footable/js/footable.all.min', 'numeric', 'orders/marketplace', 'modal'));

        $ui->assign('status', $status);
        $ui->assign('list_type', 'Shopify');
        $ui->assign('orders', $orders);
        $ui->assign('xheader', Asset::css(array('modal', 'dropzone/dropzone', 'redactor/redactor')));
        $ui->assign('xfooter', $mode_js);

        $xjq = '

    $(\'.amount\').autoNumeric(\'init\', {

    aSign: \'' . $config['currency_code'] . ' \',
    dGroup: ' . $config['thousand_separator_placement'] . ',
    aPad: ' . $config['currency_decimal_digits'] . ',
    pSign: \'' . $config['currency_symbol_position'] . '\',
    aDec: \'' . $config['dec_point'] . '\',
    aSep: \'' . $config['thousands_sep'] . '\',
    vMax: \'9999999999999999.00\',
                vMin: \'-9999999999999999.00\'

    });

 ';
        view('marketplace_order_list');

        break;

    case 'lazada-order-list':

        $status = $routes['2'];
        $orders = ORM::for_table('sys_orders_marketplace_lazada')->where('statuses', $status)
            ->order_by_asc('id')
            ->find_many();

        $ui->assign('jsvar', '
_L[\'are_you_sure\'] = \'' . $_L['are_you_sure'] . '\';
 ');

        $mode_css = '';
        $mode_js = '';

        $mode_js = Asset::js(array('footable/js/footable.all.min', 'numeric', 'orders/marketplace', 'modal'));

        $ui->assign('list_type', 'Lazada');
        $ui->assign('status', $status);
        $ui->assign('orders', $orders);
        $ui->assign('xheader', Asset::css(array('modal', 'dropzone/dropzone', 'redactor/redactor')));
        $ui->assign('xfooter', $mode_js);

        $xjq = '

    $(\'.amount\').autoNumeric(\'init\', {

    aSign: \'' . $config['currency_code'] . ' \',
    dGroup: ' . $config['thousand_separator_placement'] . ',
    aPad: ' . $config['currency_decimal_digits'] . ',
    pSign: \'' . $config['currency_symbol_position'] . '\',
    aDec: \'' . $config['dec_point'] . '\',
    aSep: \'' . $config['thousands_sep'] . '\',
    vMax: \'9999999999999999.00\',
                vMin: \'-9999999999999999.00\'

    });

 ';
        view('marketplace_order_list');

        break;

    case 'view-invoice':

        $id = $routes['2'];
        $type = $routes['3'];
        $action = isset($routes['4']) ? $routes['4'] : '';

        if ($type == 'Shopify') {
            $m = ORM::for_table('sys_orders_marketplace')->find_one($id);
            if ($m) {

                $ui->assign('order_id', $id);
                $ui->assign('type', $type);
                $ui->assign('order_number', $m->order_number);
                $ui->assign('order_number', $m->order_number);
                $ui->assign('currency', $m->currency);
                $ui->assign('date', $m->created_at);
                $ui->assign('payment_method', $m->payment_method);
                $ui->assign('statusOrder', $m->financial_status);
                $ui->assign('shipping_fee', (float)$m->shipping_fee);
                $ui->assign('total', (float)$m->total_price);
                $ui->assign('subtotal_price', (float)$m->subtotal_price);
                $ui->assign('total_discounts', (float)$m->total_discounts);
                $ui->assign('total_tax', (float)$m->total_tax);

                $ui->assign('shipping_address', json_decode($m->shipping_address, true));
                $ui->assign('address_billing', json_decode($m->billing_address, true));
                $ui->assign('customer', json_decode($m->customer, true));
                $ui->assign('shipping_lines', json_decode($m->shipping_lines, true));
                $ui->assign('note', $m->note);
                $ui->assign('items', json_decode($m->line_items, true));

            }
        } else {
            $m = ORM::for_table('sys_orders_marketplace_lazada')->find_one($id);
            if ($m) {
                $items = json_decode($m->items, true);
                $subTotal = 0;
                $totalDiscount = 0;
                $totalTax = 0;

                foreach ($items as $item) {
                    $subTotal += $item['paid_price'];
                    $totalDiscount += $item['voucher_amount'];
                    $totalTax += $item['tax_amount'];
                    $currency = $item['currency'];
                    $item['quantity'] = 1;
                    $item['id'] = $item['order_item_id'];
                    $item['price'] = $item['item_price'];
                    $item['title'] = $item['name'];
                }

                $total = $subTotal + $totalTax + $m->shipping_fee;

                $ui->assign('order_id', $id);
                $ui->assign('type', $type);
                $ui->assign('order_number', $m->order_number);
                $ui->assign('currency', $currency);
                $ui->assign('date', $m->created_at);
                $ui->assign('payment_method', $m->payment_method);
                $ui->assign('statusOrder', $m->statuses);
                $ui->assign('shipping_fee', (float)$m->shipping_fee);
                $ui->assign('total', (float)$total);
                $ui->assign('subtotal_price', (float)$subTotal);
                $ui->assign('total_discounts', (float)$totalDiscount);
                $ui->assign('total_tax', (float)$totalTax);

                $ui->assign('shipping_address', json_decode($m->address_shipping, true));
                $ui->assign('address_billing', json_decode($m->address_billing, true));
                $customerData = [
                    'customer_first_name' => $m->customer_first_name,
                    'customer_last_name' => $m->customer_last_name,
                ];
                $ui->assign('customer', $customerData);
                $ui->assign('shipping_lines', json_decode($m->shipping_lines, true));
                $ui->assign('note', $m->remarks);
                $ui->assign('items', $items);
            }
        }
        $ui->assign('status', $action);
        $mode_js = '';

        $ui->assign('xheader', Asset::css(array(
            's2/css/select2.min',
            'dropzone/dropzone',
            'dp/dist/datepicker.min',
            'sn/summernote',
            'sn/summernote-bs3',
            'modal', 'css/ribbon'
        )));
        $ui->assign('xfooter', Asset::js(array(
            's2/js/select2.min',
            's2/js/i18n/' . lan(),
            'dp/dist/datepicker.min',
            'dp/i18n/' . $config['language'],
            'numeric',
            'modal',
            'sn/summernote.min',
            'dropzone/dropzone'
        )));
        if ($action == 'print') {
            view('marketplace_print_invoice');
        } else {
            view('marketplace_view_invoice');
        }

        break;

    case 'tracking-number-form':
        $type = $routes['2'];
        $orderId = $routes['3'];
        $marketplace_setting_id = $routes['4'];
        $s = ORM::for_table('sys_orders_marketplace_settings')->find_one($marketplace_setting_id);
        $title = '';
        if ($type == 'Lazada') {
            $title = 'Get Tracking number';
            $o = ORM::for_table('sys_orders_marketplace_lazada')->find_one($orderId);

            $s = ORM::for_table('sys_orders_marketplace_settings')->find_one($marketplace_setting_id);

            $r = ORM::for_table('webhook_lazada_token')
                ->where('marketplace_settings_id', $marketplace_setting_id)
                ->find_one();

            if (empty($r)) {
                echo 'Please get access token first.';
                break;
            }

            $accessToken = $r->access_token;
            $username = $s->api_key;
            $password = $s->api_secret;

            $url = MarketplaceApi::getUrlByCountry($s->country);

            $shipmentProviders = MarketplaceApi::getShipmentProvidersLazada($url, $username, $password, $accessToken);
            if (isset($shipmentProviders['errors'])) {
                echo $shipmentProviders['errors'];
                break;
            }
            $shipmentProvidersHtml = '<div class="form-group">
                                          <div class="col-sm-8">
                                            <label for="shipment_providers" class="col-sm-6 control-label">Shipment Providers</label>
                                                <div class="col-sm-4">
                                                    <select id="shipment_providers">
                                                        <option value="">Please select shipment</option>';
            if (isset($shipmentProviders['data']['shipment_providers'])) {
                foreach ($shipmentProviders['data']['shipment_providers'] as $shipment) {
                    $shipmentProvidersHtml .= '<option value="' . $shipment['name'] . '">' . $shipment['name'] . '</option>';
                }
            }
            $shipmentProvidersHtml .= '</select>
                    </div>
               </div>
          </div>';
            $trackingNumberHtml = '
            <div class="form-group" id="tracking_number_content" style="display: none">
              <div class="col-sm-8">
                <label for="tracking_number" class="col-sm-6 control-label">Tracking number</label>
                <div class="col-sm-4">
                  <input type="text" id="tracking_number" name="tracking_number" class="form-control" autocomplete="off" value="">
                </div>
               </div>
            </div>
            ';
        } elseif ($type == 'Shopify') {
            $title = 'Tracking number';
            $o = ORM::for_table('sys_orders_marketplace')->find_one($orderId);
            $shipmentProvidersHtml = '';
            $trackingNumberHtml = '
            <div class="form-group">
              <div class="col-sm-8">
                <label for="tracking_number" class="col-sm-6 control-label">Tracking number</label>
                <div class="col-sm-4">
                  <input type="text" id="tracking_number" name="tracking_number" class="form-control" autocomplete="off" value="">
                </div>
               </div>
            </div>
            ';
        }
        if ($s && $o) {
            echo '
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h3>' . $title . '</h3>
</div>
<div class="modal-body">

<div class="row">
<div class="col-md-12">
<form class="form-horizontal" role="form" id="edit_form_inventory" method="post">
    ' . $shipmentProvidersHtml . $trackingNumberHtml . '
  <input type="hidden" id="order_id" name="order_id" value="' . $orderId . '">
  <input type="hidden" id="marketplace" name="marketplace" value="' . $type . '">
  <input type="hidden" id="marketplace_setting_id" name="marketplace_setting_id" value="' . $marketplace_setting_id . '">
</form>
    <div class="custom-error" style="display: none;text-align: center; color: #e02b27; background: #fae5e5; font-size: 1.3rem;  margin-top: 10px; border-radius: 15px; padding: 10px 10px;"></div>
	<button type="button" data-dismiss="modal" class="btn" style="float: right; margin: 10px;">Cancel</button>
	<button type="button" id="confirm-create-waybill" class="btn" style="float: right; margin: 10px;">Ok</button>
';
        }

        break;

    case 'create-waybill':
        try {
            $id = $routes['2'];
            $marketplace_setting_id = $routes['3'];
            $trackingNumber = $routes['4'];
            $s = ORM::for_table('sys_orders_marketplace_settings')->find_one($marketplace_setting_id);
            $o = ORM::for_table('sys_orders_marketplace')->find_one($id);

            if ($s && $o) {
                $username = $s->api_key;
                $password = $s->api_secret;
                $url = $s->url;

                //get location
                $locationUrl = $url . '/admin/api/2020-01/locations.json';
                $location = MarketplaceApi::getDataShopify($locationUrl, $username, $password);

                if (!isset($location['errors']) && isset($location['locations'][0]['id'])) {

                    //create fulfillment
                    $urlFulfillment = $url . '/admin/api/2020-01/orders/' . $o->id_order_marketplace . '/fulfillments.json';
                    $data = [];
                    $data['fulfillment']['location_id'] = $location['locations'][0]['id'];
                    $data['fulfillment']['tracking_numbers'] = [$trackingNumber];

                    $line_items = json_decode($o->line_items, true);
                    foreach ($line_items as $item) {
                        $data['fulfillment']['line_items'][] = ['id' => $item['id']];
                    }
                    $fulfillment = MarketplaceApi::createFulfilment($urlFulfillment, $username, $password, $data);
                    if (isset($fulfillment['errors'])) {
                        r2(U . 'marketplace/shopify-order-list', 'e', json_encode($fulfillment));
                    } else {
                        foreach ($line_items as $item) {
                            Inventory::decreaseItemMarketPlaceBySku($item['sku'], $item['quantity'], $user, $o->order_number);
                        }
                        r2(U . 'marketplace/shopify-order-list', 's', 'Created fulfilment successful.');
                    }
                } else {
                    r2(U . 'marketplace/shopify-order-list', 'e', json_encode($location));
                }
            }
        } catch (\Exception $e) {
            r2(U . 'marketplace/shopify-order-list', 'e', $e->getMessage());
        }


        break;

    case 'set-status-packed-marketplace-lazada':
        try {
            $orderId = $routes['2'];
            $marketplace_setting_id = $routes['3'];
            $shipmentProvider = $routes['4'];
            $trackingNumberProvider = $routes['5'];
            $s = ORM::for_table('sys_orders_marketplace_settings')->find_one($marketplace_setting_id);
            $o = ORM::for_table('sys_orders_marketplace_lazada')->find_one($orderId);
            $arrItemIds = [];
            $items = json_decode($o->items, true);
            if (!empty($items) && count($items) > 0) {
                foreach ($items as $item) {
                    if (isset($item['order_item_id'])) {
                        $arrItemIds[] = $item['order_item_id'];
                    }
                }
            }
            if (count($arrItemIds) == 0) {
                r2(U . 'marketplace/lazada-order-list/' . $o->statuses, 'e', 'Have no Item.');
            }
            $listItemIds = '[' . implode(",", $arrItemIds) . ']';
            $r = ORM::for_table('webhook_lazada_token')
                ->where('marketplace_settings_id', $marketplace_setting_id)
                ->find_one();

            if (empty($r)) {
                r2(U . 'marketplace/lazada-order-list/' . $o->statuses, 'e', 'Please get access token first.');
            }

            $accessToken = $r->access_token;
            $username = $s->api_key;
            $password = $s->api_secret;

            $url = MarketplaceApi::getUrlByCountry($s->country);

            $responsePacked = MarketplaceApi::setStatusToPackedByMarketplace($url, $username, $password, $accessToken, $listItemIds, $shipmentProvider);
            if (isset($responsePacked['errors'])) {
                r2(U . 'marketplace/lazada-order-list/' . $o->statuses, 'e', $responsePacked['errors']);
            }
            if (empty($trackingNumberProvider)) {
                foreach ($responsePacked['data']['order_items'] as $item) {
                    $trackingNumberProvider = $item['tracking_number'];
                    break;
                }
            }

            $responseReadyToShip = MarketplaceApi::setStatusToReadyToShipByMarketplace($url, $username, $password, $accessToken, $listItemIds, $shipmentProvider, $trackingNumberProvider);
            if (isset($responseReadyToShip['errors'])) {
                r2(U . 'marketplace/lazada-order-list/' . $o->statuses, 'e', $responseReadyToShip['errors']);
            }

            $responseSetInvoiceNumber = MarketplaceApi::setInvoiceNumberByMarketplace($url, $username, $password, $accessToken, $arrItemIds[0], $o->order_number);
            if (isset($responseSetInvoiceNumber['errors'])) {
                r2(U . 'marketplace/lazada-order-list/' . $o->statuses, 'e', $responseSetInvoiceNumber['errors']);
            }
//        $document = MarketplaceApi::getDocumentLazada($url, $username, $password, $accessToken, $listItemIds, 'shippingLabel');
//        $input = base64_decode($document['data']['document']['file']);
//
//        if (!is_dir('storage/documentlazada/')) {
//            mkdir('storage/documentlazada/', 0777, true);
//        }
//        $myfile = file_put_contents(str_replace('system/controllers/marketplace.php', '', __FILE__) . 'storage/documentlazada/' . $document['request_id'] . '.txt', $document['data']['document']['file'] . PHP_EOL);

            $o = ORM::for_table('sys_orders_marketplace_lazada')->find_one($orderId);
            $o->statuses = 'shipped';
            $o->updated_at = date('Y-m-d H:i:s');
            $o->save();

            $items = json_decode($o->items, true);
            foreach ($items as $item) {
                Inventory::decreaseItemMarketPlaceBySku($item['sku'], 1, $user, $o->order_number);
            }
            if ($shipmentProvider == 'NinjaVan') {
                r2(U . 'marketplace/print-waybill/' . $orderId . '/' . $marketplace_setting_id);
            } else {
                r2(U . 'marketplace/lazada-order-list/' . $o->statuses, 's', 'Set Order to shipped successful.');
            }
        } catch (\Exception $e) {
            r2(U . 'marketplace/lazada-order-list/' . $o->statuses, 'e', $e->getMessage());
        }
        break;

    case 'print-waybill':
        try {
            $orderId = $routes['2'];
            $marketplace_setting_id = $routes['3'];

            $s = ORM::for_table('sys_orders_marketplace_settings')->find_one($marketplace_setting_id);
            $o = ORM::for_table('sys_orders_marketplace_lazada')->find_one($orderId);
            $arrItemIds = [];
            $items = json_decode($o->items, true);
            if (!empty($items) && count($items) > 0) {
                foreach ($items as $item) {
                    if (isset($item['order_item_id'])) {
                        $arrItemIds[] = $item['order_item_id'];
                    }
                }
            }
            if (count($arrItemIds) == 0) {
                r2(U . 'marketplace/lazada-order-list/' . $o->statuses, 'e', 'Have no Item.');
            }
            $listItemIds = '[' . implode(",", $arrItemIds) . ']';
            $r = ORM::for_table('webhook_lazada_token')
                ->where('marketplace_settings_id', $marketplace_setting_id)
                ->find_one();

            if (empty($r)) {
                r2(U . 'marketplace/lazada-order-list/' . $o->statuses, 'e', 'Please get access token first.');
            }

            $accessToken = $r->access_token;
            $username = $s->api_key;
            $password = $s->api_secret;

            $url = MarketplaceApi::getUrlByCountry($s->country);

            $document = MarketplaceApi::getDocumentLazada($url, $username, $password, $accessToken, $listItemIds, 'shippingLabel');
            if (!isset($document['data']['document']['file']) || strlen($document['data']['document']['file']) < 30) {
                r2(U . 'marketplace/lazada-order-list/' . $o->statuses, 'e', 'Do not have document for Shipping Label.');
            }

            view('marketplace_print_waybill', ['document' => $document['data']['document']['file']]);
        } catch (\Exception $e) {
            r2(U . 'marketplace/lazada-order-list/' . $o->statuses, 'e', $e->getMessage());
        }
        break;

    case 'view-fulfilment':
        try {
            $orderId = $routes['2'];
            $marketplace_setting_id = $routes['3'];
            $s = ORM::for_table('sys_orders_marketplace_settings')->find_one($marketplace_setting_id);
            $o = ORM::for_table('sys_orders_marketplace')->find_one($orderId);
            if ($s && $o) {
                $username = $s->api_key;
                $password = $s->api_secret;
                $url = $s->url;

                $fulfilmentUrl = $url . '/admin/api/2020-01/orders/' . $o->id_order_marketplace . '/fulfillments.json';
                $fulfillment = MarketplaceApi::getDataShopify($fulfilmentUrl, $username, $password);
                if (isset($fulfillment['errors'])) {
                    echo json_encode($fulfillment);
                } else {
                    $content = '<tbody>';
                    foreach ($fulfillment['fulfillments'] as $ful) {
                        $content .= '
                        <div style="padding: 20px;">
                            <address>
                                <strong>Name : ' . $ful['name'] . '</strong>
                                <br>
                                <strong>Id : ' . $ful['id'] . '</strong>
                                <br>
                                <strong>Order Id : ' . $ful['order_id'] . '</strong>
                                <br>
                                <strong>Status : ' . $ful['status'] . '</strong>
                                <br>
                                <strong>Created At : ' . date($config['df'], strtotime($ful['created_at'])) . '</strong>
                                <br>
                                <strong>Tracking Number : ' . $ful['tracking_number'] . '</strong>
                            </address>
                        </div>
                        ';
                        foreach ($ful['line_items'] as $item) {
                            $content .= '<tr role="row" class="even">
                                <td class="text-center">' . $item['id'] . '</td>
                                <td class="text-center">' . $item['title'] . '</td>
                                <td class="text-center">' . $item['name'] . '</td>
                                <td class="text-center">' . $item['quantity'] . '</td>
                                <td class="text-center">' . $item['sku'] . '</td>
                                <td class="text-center">' . $item['price'] . '</td>
                                <td class="text-center">' . $item['fulfillment_status'] . '</td>
                            </tr>';
                        }
                    }
                    $content .= '</tbody>';
                    echo '
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h4 class="modal-title">
                                Fulfilment
                            </h4>
                        </div>
                            <table class="table table-bordered table-hover display">
                                                                    <thead>
                                                                    <tr class="heading">
                                                                        <th class="text-center">Id</th>
                                                                        <th class="text-center">Title</th>
                                                                        <th class="text-center">Name</th>
                                                                        <th class="text-center">Quantity</th>
                                                                        <th class="text-center">Sku</th>
                                                                        <th class="text-center">Price</th>
                                                                        <th class="text-center">Fulfillment Status</th>
                                                                    </tr>
                                                                    </thead>
                                                                    ' . $content . '
                                                                </table>
                            <button type="button" data-dismiss="modal" class="btn" style="float: right;">' . $_L['Close'] . '</button>
                        ';
                }
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        break;

    default:
        echo 'action not defined';
}