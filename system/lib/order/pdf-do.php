<html>

<head>

    <style>

        /*

        PDF library using PHP have some limitations and all CSS properties may not support. Before Editing this file, Please create a backup, so that You can restore this.

        The location of this file is here- system/lib/invoices/pdf-x2.php

        */

        * { margin: 0; padding: 0; }
        body {
            /*

            Important: Do not Edit Font Name, Unless you are sure. It's required for PDF Rendering Properly

            */


            font: 14px/1.4  dejavusanscondensed;


            /*

            Font Name End

            */
        }

        #page-wrap { width: 800px; margin: 0 auto; }

        table { border-collapse: collapse; }
        table td, table th { border: 1px solid black; padding: 5px; }


        #customer { overflow: hidden; }

        #logo { text-align: right; float: right; position: relative; margin-top: 25px; border: 1px solid #fff; max-width: 540px; overflow: hidden; }

        #meta { margin-top: 1px; width: 100%; float: right; }
        #meta td { text-align: right;  }
        #meta td.meta-head { text-align: left; background: #eee; }
        #meta td textarea { width: 100%; height: 20px; text-align: right; }

        #items { clear: both; width: 100%; margin: 30px 0 0 0; border: 1px solid black; }
        #items th { background: #eee; }
        #items textarea { width: 80px; height: 50px; }
        #items tr.item-row td {  vertical-align: top; }
        #items td.description { width: 300px; }
        #items td.item-name { width: 175px; }
        #items td.description textarea, #items td.item-name textarea { width: 100%; }
        #items td.total-line { border-right: 0; text-align: right; }
        #items td.total-value { border-left: 0; padding: 10px; }
        #items td.total-value textarea { height: 20px; background: none; }
        #items td.balance { background: #eee; }
        #items td.blank { border: 0; }

        #terms { text-align: left; margin: 20px 0 0 0; }
        #terms h5 { text-transform: uppercase; font: 13px <?php echo $config['pdf_font']; ?>; letter-spacing: 10px; border-bottom: 1px solid black; padding: 0 0 8px 0; margin: 0 0 8px 0; }
        #terms textarea { width: 100%; text-align: center;}

    </style>

</head>

<body style="font-family:dejavusanscondensed">

<div id="page-wrap">

    <table width="100%">
        <tr>
            <td style="border: 0;  text-align: left" width="62%">
                <span style="font-size: 18px; color: #2f4f4f"><strong><?php echo 'ORDER'; ?> # <?php

                        echo $order['ordernum'].'-'.$order['id'];
                        ?></strong></span>
            </td>
            <td style="border: 0;  text-align: right" width="62%"><div id="logo" style="font-size:18px">
                    <img id="image" src="<?php echo APP_URL; ?>/storage/system/<?php echo $config['logo_default']; ?>" alt="logo" /> <br> <br>
                    <?php echo $config['CompanyName']; ?> <br>
                    <?php echo $config['caddress']; ?>
                </div></td>
        </tr>
        <tr>
            <td style="border: 0;text-align: left" width="62%">
                <h1>Delivery Order</h1>
            </td>
        </tr>


    </table>

    <hr>
    <div style="clear:both"></div>

    <div id="customer">

        <table id="meta">
            <tr>
                <td rowspan="5" style="border: 1px solid white; border-right: 1px solid black; text-align: left" width="62%">

                    <!--                    --><?php //if(isset($d['title']) && $d['title'] != '') {
                    //
                    //                        echo '<h4>'.$d['title'].'</h4>
                    //                    <br>';
                    //                    }
                    //                    ?>

                    <!--                    --><?php //if($config['invoice_receipt_number'] == '1' && $d['receipt_number'] != '') {
                    //
                    //                        echo '<h4>'.$_L['Receipt Number'].': '.$d['receipt_number'].'</h4>
                    //                    <br>';
                    //                    }
                    //                    ?>

                    <strong><?php echo 'Order To'; ?></strong> <br>

                    <?php if($a['company'] != '') {
                        ?>
                        <?php echo $a['company']; ?> <br>

                        <?php
                        if($company && $config['show_business_number'] == '1'){

                            if($company->business_number != ''){
                                echo $config['label_business_number'].': '.$company->business_number.' <br>';
                            }
                        }
                        ?>
                        <?php echo $_L['ATTN']; ?>: <?php echo $a['account']; ?> <br>
                        <?php
                    }
                    else{
                        ?>
                        <?php echo $a['account']; ?> <br>
                        <?php
                    }
                    ?>
                    <?php echo $a['address']; ?> <br>
                    <?php echo $a['city']; ?> <?php echo $a['state']; ?> <?php echo $a['zip']; ?> <br>
                    <?php echo $a['country']; ?> <br>
                    <?php
                    if(($a['phone']) != ''){
                        echo $_L['Phone'].': '. $a['phone']. ' <br>';
                    }

                    if(($a['fax']) != '' && $config['fax_field'] != '0'){
                        echo $_L['Fax'].': '. $a['fax']. ' <br>';
                    }

                    if(($a['email']) != ''){
                        echo 'Email: '. $a['email']. ' <br>';
                    }
                    //                    foreach ($cf as $cfs){
                    //                        echo $cfs['fieldname'].': '. get_custom_field_value($cfs['id'],$a['id']). ' <br>';
                    //                    }
                    ?></td>
                <td class="meta-head"><?php echo 'ORDER'; ?> #</td>
                <td><?php echo $order['ordernum'].'-'.$order['id']; ?></td>
            </tr>
            <tr>

                <td class="meta-head"><?php echo $_L['Status']; ?></td>
                <td><?php
                    echo ib_lan_get_line($order['status'])
                    ?></td>
            </tr>
            <tr>

                <td class="meta-head"><?php echo 'Order Date'; ?></td>
                <td><?php echo date($config['df'], strtotime($order['date_added'])); ?></td>
            </tr>



        </table>

    </div>

    <?php
    //    if(isset($extraHtml) && $extraHtml != ''){
    //
    //        echo $extraHtml;
    //    }
    //    ?>

    <table id="items">

        <tr>
            <th align="center">#</th>

            <th align="center">Sku</th>

            <th align="center"><?php echo $_L['Item']; ?></th>

            <th align="center"><?php echo 'Length'; ?></th>

            <th align="center"><?php echo 'Height'; ?></th>

            <th align="center"><?php echo 'Width'; ?></th>

            <th align="center"><?php echo $_L['Qty']; ?></th>

            <th align="center"><?php echo 'Package Type'; ?></th>

        </tr>



        <?php
        $i = 1;
        foreach ($items as $item){
            if(!empty($item['sku'])){
                $sku = $item['sku'];
            }else{
                $sku = '9999';
            }
            $cols = '';

            $item_total = $item['total'];

            echo '  <tr class="item-row">
            <td align="center">'.$i.'</td>
            <td align="center">'.$sku.'</td>
            <td align="center">'.$item['item_name'].'</td>
            <td align="center">'.$item['length'].'</td>
            <td align="center">'.$item['height'].'</td>
            <td align="center">'.$item['width'].'</td>
            <td align="center">'.$item['quantity'].'</td>
            <td align="center"><span class="price">'.$item['package_type'].'</span></td>
        </tr>';

        }
        $i++;
        ?>

    </table>
    <h4>By signing below, well here by confirm that all the goods were received in good condition</h4>
    <table width="100%">
        <tr>
            <td style="border: 0">Name : </td>
        </tr>
        <tr>
            <td style="border: 0">Date : </td>
        </tr>
        <tr>
            <td style="border: 0">Signature : </td>
        </tr>
        <tr>
            <td style="border: 0">Company Chop / NRIC : </td>
        </tr>
        <tr>
            <td style="border: 0; text-align:center">Thank you for your business</td>
        </tr>
    </table>

</div>

</body>

</html>